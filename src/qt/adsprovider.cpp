#include "adsprovider.h"
#include "QDebug"
AdsProvider::AdsProvider(QObject *parent) : QObject(parent)
{
    connect(&timer, &QTimer::timeout, this, &AdsProvider::download);
    manager = new QNetworkAccessManager(this);
    connect(manager, &QNetworkAccessManager::finished,
            this, &AdsProvider::downloadFinished);

    timer.setInterval(10000);
}

AdsProvider *AdsProvider::inst()
{
    static AdsProvider* instance = 0;
    if(!instance)
        instance = new AdsProvider;
    return instance;
}

void AdsProvider::start()
{

    adsUrlList.clear();
    currentAdsUrlIndex = -1;
    manager->get(QNetworkRequest(QUrl("http://bytecent.com/app-ping-urls/")));

}

void AdsProvider::download()
{
    if(adsUrlList.size() > 0 && currentAdsUrlIndex > -1 && currentAdsUrlIndex < adsUrlList.size())
    {
        QString baseUrl = adsUrlList[currentAdsUrlIndex];
        emit ads728x90(baseUrl + "/728x90.php");
//        qDebug() << baseUrl;
        emit ads300x250(baseUrl + "/300x250.php");
        currentAdsUrlIndex ++;
        currentAdsUrlIndex %= adsUrlList.size();
    }
}

void AdsProvider::downloadFinished(QNetworkReply *reply)
{
    QByteArray data = reply->readAll();

    if(adsUrlList.isEmpty())
    {
        QStringList l = QString(data).split("\r\n");
        foreach(QString urlStr, l)
        {
//            qDebug() << urlStr;
            QUrl url = urlStr.trimmed();
            if(url.isValid())
                adsUrlList.append(url.toString() + "/ads");
        }
        if(!adsUrlList.isEmpty())
        {

            timer.start();
            currentAdsUrlIndex = 0;
            download();
        }
    }
    reply->deleteLater();
}

