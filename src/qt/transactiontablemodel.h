#ifndef TRANSACTIONTABLEMODEL_H
#define TRANSACTIONTABLEMODEL_H

#include <QAbstractTableModel>
#include <QStringList>

class CWallet;
class TransactionTablePriv;
class TransactionRecord;
class WalletModel;

/** UI model for the transaction table of a wallet.
 */
class TransactionTableModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(int totalCount READ totalCount NOTIFY totalCountChanged)


public:
    explicit TransactionTableModel(CWallet* wallet, WalletModel *parent = 0);
    ~TransactionTableModel();

    enum ColumnIndex {
        Status = 0,
        Date = 1,
        Type = 2,
        ToAddress = 3,
        Amount = 4
    };

    /** Roles to get specific information from a transaction row.
        These are independent of column.
    */
    enum RoleIndex {
        /** Type of transaction */
        TypeRole = Qt::UserRole,
        /** Date and time this transaction was created */
        DateRole,
        /** Long description (HTML format) */
        LongDescriptionRole,
        /** Address of transaction */
        AddressRole,
        /** Label of address related to transaction */
        LabelRole,
        /** Net amount of transaction */
        AmountRole,
        /** Unique identifier */
        TxIDRole,
        /** Is transaction confirmed? */
        ConfirmedRole,
        /** Formatted amount, without brackets when unconfirmed */
        FormattedAmountRole,
        FormattedAmountWithUnitRole,
        /** Path to Icon */
        IconUrlRole,
        FormattedAddressRole,
        StringTypeRole,
        StatusRole,
        StatusDescrRole
    };

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    QModelIndex index(int row, int column, const QModelIndex & parent = QModelIndex()) const;

    int totalCount(){return rowCount(QModelIndex());}

    QHash<int, QByteArray> roleNames() const {
        QHash<int, QByteArray> roles;
        roles[TypeRole] = "typeID";
        roles[StringTypeRole] = "type";
        roles[DateRole] = "date";
        roles[LongDescriptionRole] = "longDescription";
        roles[AddressRole] = "address";
        roles[FormattedAddressRole] = "formattedAddress";
        roles[LabelRole] = "label";
        roles[AmountRole] = "amount";
        roles[TxIDRole] = "txID";
        roles[ConfirmedRole] = "confirmed";
        roles[FormattedAmountRole] = "formattedAmount";
        roles[IconUrlRole] = "txIconUrl";
        roles[FormattedAmountWithUnitRole] = "formattedAmountWithUnit";
        roles[StatusRole] = "statusIconUrl";
        roles[StatusDescrRole] = "statusDescription";
        return roles;
    }
private:
    CWallet* wallet;
    WalletModel *walletModel;
    QStringList columns;
    TransactionTablePriv *priv;
    int cachedNumBlocks;

    QString lookupAddress(const std::string &address, bool tooltip) const;
    QVariant addressColor(const TransactionRecord *wtx) const;
    QString formatTxStatus(const TransactionRecord *wtx) const;
    QString formatTxDate(const TransactionRecord *wtx) const;
    QString formatTxType(const TransactionRecord *wtx) const;
    QString formatTxToAddress(const TransactionRecord *wtx, bool tooltip) const;
    QString formatTxAmount(const TransactionRecord *wtx, bool showUnconfirmed=true) const;
    QString formatTxAmountWithUnit(const TransactionRecord *wtx, bool showUnconfirmed) const;

    QString formatTooltip(const TransactionRecord *rec) const;
    QVariant txStatusDecoration(const TransactionRecord *wtx) const;
    QVariant txAddressDecoration(const TransactionRecord *wtx) const;

    int lastTotalCount;
    QVariant txAddressDecorationUrls(const TransactionRecord *wtx) const;
    QString txStatusIconUrl(const TransactionRecord *wtx) const;
    QVariant txStatusDescription(const TransactionRecord *wtx) const;
public slots:
    void updateTransaction(const QString &hash, int status);
    void updateConfirmations();
    void updateDisplayUnit();

    friend class TransactionTablePriv;
    void resetAll();
private slots:
    void onDataChanged();
signals:
    void totalCountChanged();
};

#endif // TRANSACTIONTABLEMODEL_H
