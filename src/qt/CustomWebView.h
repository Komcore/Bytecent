#ifndef CUSTOMWEBVIEW_H
#define CUSTOMWEBVIEW_H

#include <QWebView>
class CustomWebView : public QWebView
{
public:
    CustomWebView(QWidget * parent = 0);
};

#endif // CUSTOMWEBVIEW_H
