#include "trans_addressfilterproxy.h"

#include "transactiontablemodel.h"

#include <QDateTime>

#include <cstdlib>
// Earliest date that can be represented (far in the past)
const QDateTime TransactionAdressFilterProxy::MIN_DATE = QDateTime::fromTime_t(0);
// Last date that can be represented (far in the future)
const QDateTime TransactionAdressFilterProxy::MAX_DATE = QDateTime::fromTime_t(0xFFFFFFFF);

TransactionAdressFilterProxy::TransactionAdressFilterProxy(QObject *parent) :
    QSortFilterProxyModel(parent),
    dateFrom(MIN_DATE),
    dateTo(MAX_DATE),
    addrPrefix(),
    typeFilter(ALL_TYPES),
    minAmount(0),
    limitRows(-1)
{
    connect(this, SIGNAL(sourceModelChanged()), SLOT(sourceChanged()));
}

bool TransactionAdressFilterProxy::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);

    int type = index.data(TransactionTableModel::TypeRole).toInt();
    QDateTime datetime = index.data(TransactionTableModel::DateRole).toDateTime();
    QString address = index.data(TransactionTableModel::AddressRole).toString();
    QString label = index.data(TransactionTableModel::LabelRole).toString();
    qint64 amount = llabs(index.data(TransactionTableModel::AmountRole).toLongLong());


    if(!(TYPE(type) & typeFilter))
        return false;
    if(datetime < dateFrom || datetime > dateTo)
        return false;
    if (!address.contains(addrPrefix, Qt::CaseInsensitive) && !label.contains(addrPrefix, Qt::CaseInsensitive))
        return false;
    if(amount < minAmount)
        return false;

    if(addressSet.contains(address))
       return false;
    const_cast<TransactionAdressFilterProxy*>(this)->addressSet.insert(address);
    return true;
}

void TransactionAdressFilterProxy::sourceChanged()
{
    addressSet.clear();
    invalidateFilter();
}

void TransactionAdressFilterProxy::setDateRange(const QDateTime &from, const QDateTime &to)
{
    this->dateFrom = from;
    this->dateTo = to;
    addressSet.clear();
    invalidateFilter();
}

void TransactionAdressFilterProxy::setAddressPrefix(const QString &addrPrefix)
{
    this->addrPrefix = addrPrefix;
    addressSet.clear();
    invalidateFilter();
}

void TransactionAdressFilterProxy::setTypeFilter(quint32 modes)
{
    this->typeFilter = modes;
    addressSet.clear();
    invalidateFilter();
}

void TransactionAdressFilterProxy::setMinAmount(qint64 minimum)
{
    this->minAmount = minimum;
    addressSet.clear();
    invalidateFilter();
}

void TransactionAdressFilterProxy::setLimit(int limit)
{
    this->limitRows = limit;
}

int TransactionAdressFilterProxy::rowCount(const QModelIndex &parent) const
{
    if(limitRows != -1)
    {
        return std::min(QSortFilterProxyModel::rowCount(parent), limitRows);
    }
    else
    {
        return QSortFilterProxyModel::rowCount(parent);
    }
}
