#include "overviewpage.h"
#include "ui_overviewpage.h"

#include "clientmodel.h"
#include "walletmodel.h"
#include "bitcoinunits.h"
#include "optionsmodel.h"
#include "transactiontablemodel.h"
#include "transactionfilterproxy.h"
#include "trans_addressfilterproxy.h"
#include "guiutil.h"
#include "guiconstants.h"
#include <QAbstractItemDelegate>
#include <QPainter>
#include <QDebug>
#include <QDesktopServices>
#include "adsprovider.h"
//#include "CustomWebView.h"
#define DECORATION_SIZE 32
#define NUM_ITEMS 5
#define ADDRESS_WIDTH 280
#define DATE_WIDTH 100
class TxViewDelegate : public QAbstractItemDelegate
{
    Q_OBJECT
public:
    TxViewDelegate(): QAbstractItemDelegate(), unit(BitcoinUnits::BTC)
    {

    }

    inline void paint(QPainter *painter, const QStyleOptionViewItem &option,
                      const QModelIndex &index ) const
    {
        painter->save();
        painter->fillRect(option.rect, QColor("#363637"));
        QIcon icon = qvariant_cast<QIcon>(index.data(Qt::DecorationRole));
        QRect mainRect = option.rect;
        QRect decorationRect(mainRect.topLeft(), QSize(DECORATION_SIZE, DECORATION_SIZE));
        int xspace = DECORATION_SIZE + 8;
        int ypad = 6;
        int height = (mainRect.height() - 2*ypad);
        QRect addressRect(mainRect.left() + xspace, mainRect.top()+ypad, ADDRESS_WIDTH, height);
        QRect dateRect(addressRect.right() + xspace, mainRect.top()+ypad, DATE_WIDTH, height);
        QRect amountRect(dateRect.right() + xspace, mainRect.top()+ypad, mainRect.width() - xspace - ADDRESS_WIDTH - DATE_WIDTH, height);
        icon.paint(painter, decorationRect);

        QDateTime date = index.data(TransactionTableModel::DateRole).toDateTime();
        QString address = index.data(Qt::DisplayRole).toString();
        qint64 amount = index.data(TransactionTableModel::AmountRole).toLongLong();
        bool confirmed = index.data(TransactionTableModel::ConfirmedRole).toBool();
        QVariant value = index.data(Qt::ForegroundRole);
        QColor foreground = (255,255,255);
        if(value.canConvert<QBrush>())
        {
            QBrush brush = qvariant_cast<QBrush>(value);
            foreground = brush.color();
        }

        painter->setPen(foreground);
        painter->drawText(addressRect, Qt::AlignLeft|Qt::AlignVCenter, address);

        if(amount < 0)
        {
            foreground = COLOR_NEGATIVE;
        }
        else if(!confirmed)
        {
            foreground = COLOR_UNCONFIRMED;
        }
        else
        {
            foreground = option.palette.color(QPalette::Text);
        }
        painter->setPen(foreground);
        QString amountText = BitcoinUnits::formatWithUnit(unit, amount, true);
        if(!confirmed)
        {
            amountText = QString("[") + amountText + QString("]");
        }
        painter->drawText(amountRect, Qt::AlignLeft|Qt::AlignVCenter, amountText);

        painter->setPen(option.palette.color(QPalette::Text));
        painter->drawText(dateRect, Qt::AlignLeft|Qt::AlignVCenter, GUIUtil::dateTimeStr(date));

        painter->restore();
    }

    inline QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        return QSize(DECORATION_SIZE, DECORATION_SIZE);
    }

    int unit;

};

class AddrViewDelegate : public QAbstractItemDelegate
{
    Q_OBJECT
public:
    AddrViewDelegate(): QAbstractItemDelegate()
    {

    }

    inline void paint(QPainter *painter, const QStyleOptionViewItem &option,
                      const QModelIndex &index ) const
    {
        painter->save();

        QIcon icon(":/icons/bytecent_ico");

        QRect mainRect = option.rect;
        QRect decorationRect(mainRect.topLeft(), QSize(DECORATION_SIZE, DECORATION_SIZE));
        int xspace = DECORATION_SIZE + 8;
        int ypad = 6;
        QRect addressRect(mainRect.left() + xspace, mainRect.top()+ypad, mainRect.width() - xspace, mainRect.height() - 2*ypad);
        icon.paint(painter, decorationRect);

        QString address = index.data(Qt::DisplayRole).toString();

        painter->setPen(option.palette.color(QPalette::Text));
        painter->drawText(addressRect, Qt::AlignLeft|Qt::AlignVCenter, address);

        painter->restore();
    }

    inline QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        return QSize(DECORATION_SIZE, DECORATION_SIZE);
    }
};


#include "overviewpage.moc"

OverviewPage::OverviewPage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OverviewPage),
    clientModel(0),
    walletModel(0),
    currentBalance(-1),
    currentUnconfirmedBalance(-1),
    currentImmatureBalance(-1),
    txdelegate(new TxViewDelegate()),
    addrDelegate(new AddrViewDelegate()),
    filter(0),
    addrFilter(0)
{
    ui->setupUi(this);
    ui->transactionsHeaderFrame->setStyleSheet("QFrame { background-color: #363637; color: #808182;}");

    // Recent transactions
    ui->listTransactions->setItemDelegate(txdelegate);
    ui->listTransactions->setIconSize(QSize(DECORATION_SIZE, DECORATION_SIZE));
//    ui->listTransactions->setMinimumHeight(NUM_ITEMS * (DECORATION_SIZE + 2));
    ui->listTransactions->setAttribute(Qt::WA_MacShowFocusRect, false);

    ui->listAddresses->setItemDelegate(addrDelegate);
    ui->listAddresses->setIconSize(QSize(DECORATION_SIZE, DECORATION_SIZE));
//    ui->listAddresses->setMinimumHeight(NUM_ITEMS * (DECORATION_SIZE + 2));
    ui->listAddresses->setAttribute(Qt::WA_MacShowFocusRect, false);

    connect(ui->listTransactions, SIGNAL(clicked(QModelIndex)), this, SLOT(handleTransactionClicked(QModelIndex)));
    connect(ui->pushButtonViewMoreTransactions, SIGNAL(clicked(bool)), this, SIGNAL(transactionsPageRequested()));
    connect(ui->pushButtonViewMoreAddresses, SIGNAL(clicked(bool)), this, SIGNAL(addressesPageRequested()));



    // init "out of sync" warning labels
//    ui->labelWalletStatus->setText("(" + tr("out of sync") + ")");
//    ui->labelTransactionsStatus->setText("(" + tr("out of sync") + ")");

    // start with displaying the "out of sync" warnings
    showOutOfSyncWarning(true);

    networkManager = new QNetworkAccessManager(this);
    connect(networkManager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(httpReplyFinished(QNetworkReply*)));
    bytecentStatsTimer.setInterval(2000);
    connect(&bytecentStatsTimer, SIGNAL(timeout()), SLOT(requestBytecentStats()));
    bytecentStatsTimer.start();
    requestBytecentStats();

    connect(ui->pushButtonTwitter, &QPushButton::clicked, [=] () {  QDesktopServices::openUrl(QUrl("https://twitter.com/bytecent_byc?lang=en"));  });
    connect(ui->pushButtonYouTube, &QPushButton::clicked, [=] () {  QDesktopServices::openUrl(QUrl("https://www.youtube.com/channel/UCTVJkXJ5LxkWj2lWsHrN4cA"));  });
    connect(ui->pushButtonFacebook, &QPushButton::clicked, [=] () {  QDesktopServices::openUrl(QUrl("https://www.facebook.com/bytecent"));  });

//    adsWebView = new CustomWebView(this);
//    adsWebView->setMinimumHeight(90);
//    adsWebView->setMaximumHeight(90);
//    adsWebView->setMinimumWidth(728);
//    adsWebView->setMaximumWidth(728);

//    ui->adsHorLayout->insertWidget(0, adsWebView);
//    connect(AdsProvider::inst(), &AdsProvider::ads728x90, [=] (QUrl url) {  adsWebView->load(url);  });
}

void OverviewPage::handleTransactionClicked(const QModelIndex &index)
{
    if(filter)
        emit transactionClicked(filter->mapToSource(index));
}

OverviewPage::~OverviewPage()
{
    delete ui;
}

void OverviewPage::setBalance(qint64 balance, qint64 unconfirmedBalance, qint64 immatureBalance)
{
    int unit = walletModel->getOptionsModel()->getDisplayUnit();
    currentBalance = balance;
    currentUnconfirmedBalance = unconfirmedBalance;
    currentImmatureBalance = immatureBalance;
    ui->labelBalance->setText(BitcoinUnits::formatWithUnit(unit, balance));
    ui->labelUnconfirmed->setText(BitcoinUnits::formatWithUnit(unit, unconfirmedBalance));
    ui->labelImmature->setText(BitcoinUnits::formatWithUnit(unit, immatureBalance));

    // only show immature (newly mined) balance if it's non-zero, so as not to complicate things
    // for the non-mining users
    bool showImmature = immatureBalance != 0;
    ui->labelImmature->setVisible(showImmature);
    ui->labelImmatureText->setVisible(showImmature);
}

void OverviewPage::setClientModel(ClientModel *model)
{
    this->clientModel = model;
    if(model)
    {
        // Show warning if this is a prerelease version
        connect(model, SIGNAL(alertsChanged(QString)), this, SLOT(updateAlerts(QString)));
        updateAlerts(model->getStatusBarWarnings());
        connect(model, SIGNAL(numConnectionsChanged(int)), this, SLOT(updatePeersCount(int)));
    }
}

void OverviewPage::setWalletModel(WalletModel *model)
{
    this->walletModel = model;
    if(model && model->getOptionsModel())
    {
        // Set up transaction list
        filter = new TransactionFilterProxy();
        filter->setSourceModel(model->getTransactionTableModel());
        filter->setLimit(NUM_ITEMS);
        filter->setDynamicSortFilter(true);
        filter->setSortRole(Qt::EditRole);
        filter->sort(TransactionTableModel::Status, Qt::DescendingOrder);

        ui->listTransactions->setModel(filter);
        ui->listTransactions->setModelColumn(TransactionTableModel::ToAddress);
        ui->listTransactions->setSpacing(1);

        addrFilter = new TransactionAdressFilterProxy();
        addrFilter->setSourceModel(model->getTransactionTableModel());
        addrFilter->setLimit(NUM_ITEMS);
        addrFilter->setDynamicSortFilter(true);
        addrFilter->setSortRole(Qt::EditRole);
        addrFilter->sort(TransactionTableModel::Status, Qt::DescendingOrder);
        connect(model->getTransactionTableModel(), SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)), addrFilter, SLOT(sourceChanged()));

        ui->listAddresses->setModel(addrFilter);
        ui->listAddresses->setModelColumn(TransactionTableModel::ToAddress);

        // Keep up to date with wallet
        setBalance(model->getBalance(), model->getUnconfirmedBalance(), model->getImmatureBalance());
        connect(model, SIGNAL(balanceChanged(qint64, qint64, qint64)), this, SLOT(setBalance(qint64, qint64, qint64)));

        connect(model->getOptionsModel(), SIGNAL(displayUnitChanged(int)), this, SLOT(updateDisplayUnit()));


        connect(model->getTransactionTableModel(), SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)), this, SLOT(updateTransactionsTotalCount()));
        connect(model, SIGNAL(encryptionStatusChanged(int)), this, SLOT(updateWalletEncryptionStatus()));

    }

    // update the display unit, to not use the default ("BTC")
    updateDisplayUnit();
    updateTransactionsTotalCount();
    updateWalletEncryptionStatus();
}

void OverviewPage::updateDisplayUnit()
{
    if(walletModel && walletModel->getOptionsModel())
    {
        if(currentBalance != -1)
            setBalance(currentBalance, currentUnconfirmedBalance, currentImmatureBalance);

        // Update txdelegate->unit with the current unit
        txdelegate->unit = walletModel->getOptionsModel()->getDisplayUnit();

        ui->listTransactions->update();
    }
}

void OverviewPage::updateAlerts(const QString &warnings)
{
    this->ui->labelAlerts->setVisible(!warnings.isEmpty());
    this->ui->labelAlerts->setText(warnings);
}

void OverviewPage::updateTransactionsTotalCount()
{
    this->ui->labelActivity->setText(tr("%n Transaction(s)", "", walletModel->getTransactionTableModel()->rowCount(QModelIndex())));
}

void OverviewPage::updateWalletEncryptionStatus()
{
    switch(walletModel->getEncryptionStatus())
    {
    case WalletModel::Locked:
        ui->labelWalletLockedStatus->setText(tr("Locked"));
        break;
    case WalletModel::Unlocked:
        ui->labelWalletLockedStatus->setText(tr("Unlocked"));
        break;
    case WalletModel::Unencrypted:
        ui->labelWalletLockedStatus->setText(tr("Unencrypted"));
        break;
    }
}

void OverviewPage::updatePeersCount(int connectionCount)
{
    ui->labelPeersCount->setText(QString("%1").arg(connectionCount));
    if(connectionCount > 0)
    {
        ui->labelPeersCount->show();
        ui->label_10->show();
        ui->labelConnectedStatus->setText("Connected");
    }
    else
    {
        ui->labelPeersCount->hide();
        ui->label_10->hide();
        ui->labelConnectedStatus->setText("Disconnected");
    }
}

void OverviewPage::httpReplyFinished(QNetworkReply *reply)
{
    QString data = reply->readAll();
    QList<QString> strVals = data.split(";");
    if(strVals.size() == 3)
    {
        ui->label_BYCValue->setText(strVals[0]);
        ui->labelCirculationValue->setText(strVals[1]);
        ui->labelMarketCap->setText(strVals[2]);
    }
    reply->deleteLater();
}

void OverviewPage::requestBytecentStats()
{
    networkManager->get(QNetworkRequest(QUrl("https://bytecent.com/app-stats/")));
}

void OverviewPage::showOutOfSyncWarning(bool fShow)
{
//    ui->labelWalletStatus->setVisible(fShow);
    ui->labelTransactionsStatus->setVisible(fShow);
}
