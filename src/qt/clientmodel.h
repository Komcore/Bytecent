#ifndef CLIENTMODEL_H
#define CLIENTMODEL_H

#include <QObject>

class OptionsModel;
class AddressTableModel;
class TransactionTableModel;
class CWallet;

QT_BEGIN_NAMESPACE
class QDateTime;
class QTimer;
QT_END_NAMESPACE

#include <QDateTime>

/** Model for Bitcoin network client. */
class ClientModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int connectionsNumber READ getNumConnections NOTIFY connectionsNumberChanged)
    Q_PROPERTY(int blocksNumber READ getNumBlocks NOTIFY blocksNumberChanged)
    Q_PROPERTY(int blocksTotalNumber READ getBlocksTotalNumber NOTIFY blocksTotalNumberChanged)
    Q_PROPERTY(BlockSource blockSource READ getBlockSource NOTIFY blockSourceChanged)
    Q_PROPERTY(QDateTime lastBlockDate READ lastBlockDate NOTIFY lastBlockDateChanged)
public:
    explicit ClientModel(OptionsModel *optionsModel, QObject *parent = 0);
    explicit ClientModel(QObject *parent = 0);
    ~ClientModel();


    OptionsModel *getOptionsModel();

    int getNumConnections() const;
    int getNumBlocks() const;
    int getNumBlocksAtStartup();

    double getVerificationProgress() const;
    Q_INVOKABLE QDateTime getLastBlockDate() const;

    //! Return true if client connected to testnet
    Q_INVOKABLE bool isTestNet() const;
    //! Return true if core is doing initial block download
    bool inInitialBlockDownload() const;
    //! Return true if core is importing blocks
    enum BlockSource {
        BLOCK_SOURCE_INVALID,
        BLOCK_SOURCE_NONE,
        BLOCK_SOURCE_REINDEX,
        BLOCK_SOURCE_DISK,
        BLOCK_SOURCE_NETWORK
    };
    Q_ENUM(BlockSource)
    enum BlockSource getBlockSource() const;
    //! Return conservative estimate of total number of blocks, or 0 if unknown
    int getNumBlocksOfPeers() const;
    //! Return warnings to be displayed in status bar
    QString getStatusBarWarnings() const;

    Q_INVOKABLE QString formatFullVersion() const;
    Q_INVOKABLE QString formatBuildDate() const;
    bool isReleaseVersion() const;
    Q_INVOKABLE QString clientName() const;
    Q_INVOKABLE QString formatClientStartupTime() const;

    int getBlocksTotalNumber();
    QDateTime lastBlockDate() const
    {
        return getLastBlockDate();
    }

private:
    OptionsModel *optionsModel;

    int cachedNumBlocks;
    int cachedNumBlocksOfPeers;
	bool cachedReindexing;
	bool cachedImporting;
    BlockSource cachedBlockSource;

    int numBlocksAtStartup;

    QTimer *pollTimer;

    void subscribeToCoreSignals();
    void unsubscribeFromCoreSignals();

    int lastConnNumber;
    QDateTime m_lastBlockDate;

signals:
    void numConnectionsChanged(int count);
    void numBlocksChanged(int count, int countOfPeers);
    void alertsChanged(const QString &warnings);

    //! Asynchronous message notification
    void message(const QString &title, const QString &message, unsigned int style);
    void blocksNumberChanged();
    void connectionsNumberChanged();
    void blockSourceChanged(BlockSource blockSource);
    void blocksTotalNumberChanged(int blocksTotalNumber);
    void lastBlockDateChanged(QDateTime lastBlockDate);

public slots:
    void updateTimer();
    void updateNumConnections(int numConnections);
    void updateAlert(const QString &hash, int status);
};

#endif // CLIENTMODEL_H
