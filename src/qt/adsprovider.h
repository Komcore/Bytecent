#ifndef ADSPROVIDER_H
#define ADSPROVIDER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>
class AdsProvider : public QObject
{
    Q_OBJECT
    QTimer timer;
    QNetworkAccessManager * manager;
    QStringList adsUrlList;
    int currentAdsUrlIndex;
public:
    explicit AdsProvider(QObject *parent = 0);
    static AdsProvider* inst();
    void start();
signals:
    void ads728x90(QUrl url);
    void ads300x250(QUrl url);
public slots:
private slots:
    void download();
    void downloadFinished(QNetworkReply* reply);
};

#endif // ADSPROVIDER_H
