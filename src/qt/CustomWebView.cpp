#include "CustomWebView.h"
#include <QNetworkReply>
#include <QDesktopServices>
#include <QDebug>
CustomWebView::CustomWebView(QWidget *parent):
    QWebView(parent)
{
    page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
    connect(page()->networkAccessManager(), &QNetworkAccessManager::sslErrors,
            [=] (QNetworkReply * reply, const QList<QSslError> & errors)
    {
        reply->ignoreSslErrors();
    }
    );

    connect(this, &CustomWebView::linkClicked, [=] (const QUrl & url) {qDebug() << url; QDesktopServices::openUrl(url);});
}

