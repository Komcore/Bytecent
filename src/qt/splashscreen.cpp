#include "splashscreen.h"
#include "clientversion.h"
#include "util.h"

#include <QPainter>

SplashScreen::SplashScreen(const QPixmap &pixmap, Qt::WindowFlags f) :
    QSplashScreen(pixmap, f)
{
    float fontFactor            = 1.0;

    QString font            = "Arial";

    // load the bitmap for writing some text over it
    QPixmap newPixmap;
    if(GetBoolArg("-testnet")) {
        newPixmap     = QPixmap(":/images/res/images/bytecent-splash.png");
    }
    else {
        newPixmap     = QPixmap(":/images/res/images/bytecent-splash.png");
    }

    QPainter pixPaint(&newPixmap);
    pixPaint.setPen(QColor(100,100,100));

    // check font size and drawing with
    pixPaint.setFont(QFont(font, 33*fontFactor));
    QFontMetrics fm = pixPaint.fontMetrics();
    //int titleTextWidth  = fm.width(titleText);
  //  if(titleTextWidth > 160) {
        // strange font rendering, Arial probably not found
  //      fontFactor = 0.75;
   // }

    /*pixPaint.setFont(QFont(font, 33*fontFactor));
    fm = pixPaint.fontMetrics();
  //  titleTextWidth  = fm.width(titleText);
    pixPaint.drawText(newPixmap.width()-titleTextWidth-paddingRight,paddingTop,titleText);

    pixPaint.setFont(QFont(font, 15*fontFactor));

    // if the version string is to long, reduce size
    fm = pixPaint.fontMetrics();
    //int versionTextWidth  = fm.width(versionText);
  //  if(versionTextWidth > titleTextWidth+paddingRight-10) {
        //pixPaint.setFont(QFont(font, 10*fontFactor));
       // titleVersionVSpace -= 5;
    }
    //pixPaint.drawText(newPixmap.width()-titleTextWidth-paddingRight+2,paddingTop+titleVersionVSpace,versionText);

    // draw testnet string if -testnet is on
    if(QApplication::applicationName().contains(QString("-testnet"))) {
        // draw copyright stuff
        QFont boldFont = QFont(font, 10*fontFactor);
        boldFont.setWeight(QFont::Bold);
        pixPaint.setFont(boldFont);
        fm = pixPaint.fontMetrics();
        //int testnetAddTextWidth  = fm.width(testnetAddText);
       // pixPaint.drawText(newPixmap.width()-testnetAddTextWidth-10,15,testnetAddText);
    }*/

    pixPaint.end();

    this->setPixmap(newPixmap);
}
