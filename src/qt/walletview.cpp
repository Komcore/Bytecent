/*
 * Qt4 bitcoin GUI.
 *
 * W.J. van der Laan 2011-2012
 * The Bitcoin Developers 2011-2013
 */
#include "walletview.h"
#include "bitcoingui.h"
#include "transactiontablemodel.h"
#include "addressbookpage.h"
#include "sendcoinsdialog.h"
#include "signverifymessagedialog.h"
#include "clientmodel.h"
#include "walletmodel.h"
#include "optionsmodel.h"
#include "transactionview.h"
#include "overviewpage.h"
#include "askpassphrasedialog.h"
#include "ui_interface.h"
#include "ui_minerpage.h"
#include "init.h"
#include "main.h"
#include "bitcoinrpc.h"
#include "key.h"

#include <stdlib.h>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QAction>
#include <QDesktopServices>
#include <QFileDialog>
#include <QPushButton>
#include <QNetworkAccessManager>
#include <QtNetwork/QHostInfo>
#include <QNetworkRequest>
#include <QByteArray>
#include <QTimer>
#include <QTime>
#include <QUrl>
#include <QPainter>
#include <QGraphicsTextItem>
#include "captcha.h"

extern Captcha gCaptcha;

WalletView::WalletView(QWidget *parent, BitcoinGUI *_gui):
    QStackedWidget(parent),
    gui(_gui),
    clientModel(0),
    walletModel(0),
    mineInit(false),
    balanceInit(false),
    timeLeft(0),
    checked(false),
    POM(25),
    version(2111),
    ID(2),
    speedCheck(false),
    clientVersion(2111),
    depth(6),
    blankName("")

{
    // Create tabs
    getPOMValue();
    overviewPage = new OverviewPage();
    transactionsPage = new QWidget(this);
    QVBoxLayout *vbox = new QVBoxLayout();
    QHBoxLayout *hbox_buttons = new QHBoxLayout();
    transactionView = new TransactionView(this);
    vbox->addWidget(transactionView);
    QPushButton *exportButton = new QPushButton(tr("&Export"), this);
    exportButton->setToolTip(tr("Export the data in the current tab to a file"));
#ifndef Q_OS_MAC // Icons on push buttons are very uncommon on Mac
    exportButton->setIcon(QIcon(":/icons/export"));
#endif
    hbox_buttons->addStretch();
    hbox_buttons->addWidget(exportButton);
    vbox->addLayout(hbox_buttons);
    transactionsPage->setLayout(vbox);

    addressBookPage = new AddressBookPage(AddressBookPage::ForEditing, AddressBookPage::SendingTab);

    receiveCoinsPage = new AddressBookPage(AddressBookPage::ForEditing, AddressBookPage::ReceivingTab);

    sendCoinsPage = new SendCoinsDialog(gui);

    signVerifyMessageDialog = new SignVerifyMessageDialog(gui);

    //build minerpage
    minerpage = new QWidget(this);
    Ui::minerpage miner;
    miner.setupUi(minerpage);
    QPushButton * reloadButton = minerpage->findChild<QPushButton *>("reloadButton");
    QPushButton * tryButton = minerpage->findChild<QPushButton *>("tryButton");
    QValidatedLineEdit * captchaEnter = minerpage->findChild<QValidatedLineEdit *>("captchaEnter");
    captchaEnter->setPlaceholderText(tr("Enter captcha to begin earning BYC!"));
    QValidatedLineEdit * minerAddress = minerpage->findChild<QValidatedLineEdit *>("minerAddress");
    minerAddress->setPlaceholderText(tr("Enter your BYC address to increase your hours earning Bytecent."));
    generateCaptcha();
    connect(reloadButton, SIGNAL(clicked()), this, SLOT(generateCaptcha()));
    connect(tryButton, SIGNAL(clicked()), this, SLOT(mineButtonPushed()));

    //initialize labels
    QString blocksStr = QString::number(nBestHeight);
    minerpage->findChild<QLabel *>("blocks")->setText(blocksStr);
    QString POMStr = QString::number(POM);
    minerpage->findChild<QLabel *>("POMlabel")->setText(POMStr);
    QString difficultyStr = QString::number(GetDifficulty(pindexBest));
    minerpage->findChild<QLabel *>("difficulty")->setText(difficultyStr);
    QString primesPerSecStr = QString::number(dPrimesPerSec);
    minerpage->findChild<QLabel *>("primesPerSec")->setText(primesPerSecStr);
    QString chainsPerDayStr = QString::number(dChainsPerDay);
    minerpage->findChild<QLabel *>("chainsPerDay")->setText(chainsPerDayStr);
    minerpage->findChild<QLabel *>("timer")->setVisible(false);

    addWidget(overviewPage);
    addWidget(minerpage);
    addWidget(transactionsPage);
    addWidget(addressBookPage);
    addWidget(receiveCoinsPage);
    addWidget(sendCoinsPage);

    // Clicking on a transaction on the overview page simply sends you to transaction history page
    connect(overviewPage, SIGNAL(transactionClicked(QModelIndex)), this, SLOT(gotoHistoryPage()));
    connect(overviewPage, SIGNAL(transactionClicked(QModelIndex)), transactionView, SLOT(focusTransaction(QModelIndex)));
    connect(overviewPage, SIGNAL(transactionsPageRequested()), this, SLOT(gotoHistoryPage()));
    connect(overviewPage, SIGNAL(addressesPageRequested()), this, SLOT(gotoReceiveCoinsPage()));




    // Double-clicking on a transaction on the transaction history page shows details
    connect(transactionView, SIGNAL(doubleClicked(QModelIndex)), transactionView, SLOT(showDetails()));

    // Clicking on "Send Coins" in the address book sends you to the send coins tab
    connect(addressBookPage, SIGNAL(sendCoins(QString)), this, SLOT(gotoSendCoinsPage(QString)));
    // Clicking on "Verify Message" in the address book opens the verify message tab in the Sign/Verify Message dialog
    connect(addressBookPage, SIGNAL(verifyMessage(QString)), this, SLOT(gotoVerifyMessageTab(QString)));
    // Clicking on "Sign Message" in the receive coins page opens the sign message tab in the Sign/Verify Message dialog
    connect(receiveCoinsPage, SIGNAL(signMessage(QString)), this, SLOT(gotoSignMessageTab(QString)));
    // Clicking on "Export" allows to export the transaction list
    connect(exportButton, SIGNAL(clicked()), transactionView, SLOT(exportClicked()));
    captchaTimer = new QTimer(this);
    connect(captchaTimer, SIGNAL(timeout()), this, SLOT(updateCaptchaTimer()));
    captchaTimer->start(60000);
    gotoOverviewPage();
}

WalletView::~WalletView()
{
}

void WalletView::setBitcoinGUI(BitcoinGUI *gui)
{
    this->gui = gui;
}

void WalletView::setClientModel(ClientModel *clientModel)
{
    this->clientModel = clientModel;
    if (clientModel)
    {
        overviewPage->setClientModel(clientModel);
        addressBookPage->setOptionsModel(clientModel->getOptionsModel());
        receiveCoinsPage->setOptionsModel(clientModel->getOptionsModel());
    }
}

void WalletView::setWalletModel(WalletModel *walletModel)
{
    this->walletModel = walletModel;
    if (walletModel)
    {
        // Receive and report messages from wallet thread
        connect(walletModel, SIGNAL(message(QString,QString,unsigned int)), gui, SLOT(message(QString,QString,unsigned int)));

        // Put transaction list in tabs
        transactionView->setModel(walletModel);
        overviewPage->setWalletModel(walletModel);
        addressBookPage->setModel(walletModel->getAddressTableModel());
        receiveCoinsPage->setModel(walletModel->getAddressTableModel());
        sendCoinsPage->setModel(walletModel);
        signVerifyMessageDialog->setModel(walletModel);

        setEncryptionStatus();
        connect(walletModel, SIGNAL(encryptionStatusChanged(int)), gui, SLOT(setEncryptionStatus(int)));

        // Balloon pop-up for new transaction
        connect(walletModel->getTransactionTableModel(), SIGNAL(rowsInserted(QModelIndex,int,int)),
                this, SLOT(incomingTransaction(QModelIndex,int,int)));

        // Ask for passphrase if needed
//        connect(walletModel, SIGNAL(requireUnlock()), this, SLOT(unlockWallet()));
    }
}

void WalletView::incomingTransaction(const QModelIndex& parent, int start, int /*end*/)
{
    // Prevent balloon-spam when initial block download is in progress
    if(!walletModel || !clientModel || clientModel->inInitialBlockDownload())
        return;

    TransactionTableModel *ttm = walletModel->getTransactionTableModel();

    QString date = ttm->index(start, TransactionTableModel::Date, parent).data().toString();
    qint64 amount = ttm->index(start, TransactionTableModel::Amount, parent).data(Qt::EditRole).toULongLong();
    QString type = ttm->index(start, TransactionTableModel::Type, parent).data().toString();
    QString address = ttm->index(start, TransactionTableModel::ToAddress, parent).data().toString();

    gui->incomingTransaction(date, walletModel->getOptionsModel()->getDisplayUnit(), amount, type, address);
}

void WalletView::gotoOverviewPage()
{
    gui->getOverviewAction()->setChecked(true);
    setCurrentWidget(overviewPage);
}

void WalletView::gotominerpage()
{
    gui->getminerAction()->setChecked(true);
    setCurrentWidget(minerpage);
}

void WalletView::gotoHistoryPage()
{
    gui->getHistoryAction()->setChecked(true);
    setCurrentWidget(transactionsPage);
}

void WalletView::gotoAddressBookPage()
{
    gui->getAddressBookAction()->setChecked(true);
    setCurrentWidget(addressBookPage);
}

void WalletView::gotoReceiveCoinsPage()
{
    gui->getReceiveCoinsAction()->setChecked(true);
    setCurrentWidget(receiveCoinsPage);
}

void WalletView::gotoSendCoinsPage(QString addr)
{
    gui->getSendCoinsAction()->setChecked(true);
    setCurrentWidget(sendCoinsPage);

    if (!addr.isEmpty())
        sendCoinsPage->setAddress(addr);
}

void WalletView::gotoSignMessageTab(QString addr)
{
    // call show() in showTab_SM()
    signVerifyMessageDialog->showTab_SM(true);

    if (!addr.isEmpty())
        signVerifyMessageDialog->setAddress_SM(addr);
}

void WalletView::gotoVerifyMessageTab(QString addr)
{
    // call show() in showTab_VM()
    signVerifyMessageDialog->showTab_VM(true);

    if (!addr.isEmpty())
        signVerifyMessageDialog->setAddress_VM(addr);
}

bool WalletView::handleURI(const QString& strURI)
{
    // URI has to be valid
    if (sendCoinsPage->handleURI(strURI))
    {
        gotoSendCoinsPage();
        emit showNormalIfMinimized();
        return true;
    }
    else
        return false;
}

void WalletView::showOutOfSyncWarning(bool fShow)
{
    overviewPage->showOutOfSyncWarning(fShow);
}

void WalletView::setEncryptionStatus()
{
    gui->setEncryptionStatus(walletModel->getEncryptionStatus());
}

void WalletView::encryptWallet(bool status)
{
    if(!walletModel)
        return;
    AskPassphraseDialog dlg(status ? AskPassphraseDialog::Encrypt : AskPassphraseDialog::Decrypt, this);
    dlg.setModel(walletModel);
    dlg.exec();

    setEncryptionStatus();
}

void WalletView::backupWallet()
{
    QString saveDir = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString filename = QFileDialog::getSaveFileName(this, tr("Backup Wallet"), saveDir, tr("Wallet Data (*.dat)"));
    if (!filename.isEmpty()) {
        if (!walletModel->backupWallet(filename)) {
            gui->message(tr("Backup Failed"), tr("There was an error trying to save the wallet data to the new location."),
                         CClientUIInterface::MSG_ERROR);
        }
        else
            gui->message(tr("Backup Successful"), tr("The wallet data was successfully saved to the new location."),
                         CClientUIInterface::MSG_INFORMATION);
    }
}

void WalletView::changePassphrase()
{
    AskPassphraseDialog dlg(AskPassphraseDialog::ChangePass, this);
    dlg.setModel(walletModel);
    dlg.exec();
}


void WalletView::sendMachineID()
{
    //get mac address
    QString mac;
    foreach(QNetworkInterface netInterface, QNetworkInterface::allInterfaces())
    {
        if (!(netInterface.flags() & QNetworkInterface::IsLoopBack))
            mac = netInterface.hardwareAddress();
    }

    //get host name
    QHostInfo hostInfo;
    hostInfo = QHostInfo::fromName(QHostInfo::localHostName());
    QString hostName = QHostInfo::localHostName();

    //get username
#ifndef WIN32
    char* user = getenv("USERNAME");
#else
    char* user = getenv("USER");
#endif

    //get address and balance
    QValidatedLineEdit * minerAddress = minerpage->findChild<QValidatedLineEdit *>("minerAddress");
    qaddress = minerpage->findChild<QValidatedLineEdit *>("minerAddress")->text();
    address = qaddress.toStdString();
    CTxDestination myaddress = CBitcoinAddress(address).Get();
    bool mine = IsMine(*pwalletMain, myaddress);
    if (mine)
    {
        currentBalance = GetOneAccountBalance(address)*(qreal)COIN;
    }
    else if (qaddress.isEmpty())
    {
        currentBalance = 0;
        startMining();
        timeLeft = 0;
        getBalanceFactor();
        QValidatedLineEdit * captchaEnter = minerpage->findChild<QValidatedLineEdit *>("captchaEnter");
        captchaEnter->setPlaceholderText(tr("That's correct. Earning BYC!"));
        minerpage->findChild<QGraphicsView *>("captcha")->setVisible(false);
        minerpage->findChild<QLabel *>("timer")->setVisible(true);
        return;
    }
    else
    {
        minerAddress->clear();
        minerAddress->clearFocus();
        minerAddress->setPlaceholderText(tr("Enter your BYC address to increase your hours earning Bytecent."));
        generateCaptcha();
        checked=false;
        QMessageBox::warning(this, tr("Not your address"),
                             tr("You do not own this address or it is invalid, please try again."),
                             QMessageBox::Ok, QMessageBox::Ok);
        return;
    }

    //creat machine ID
    QByteArray IDdata;
    IDdata.append(mac).append(hostName).append(user);

    //encrypt ID
    QByteArray md5IDhash = QCryptographicHash::hash(IDdata,QCryptographicHash::Md5);
    QByteArray md5ID = md5IDhash.toHex();
    QByteArray SHA1IDhash = QCryptographicHash::hash(md5ID,QCryptographicHash::Sha1);
    QByteArray SHA1ID = SHA1IDhash.toHex();

    //create message
    QString url = ("http://bytecent.com/reg/?walletid=");
    QString id = ("&hwid=");
    QString key = ("&key=");
    QByteArray message;
    message.append(url).append(qaddress).append(id).append(md5ID).append(key).append(SHA1ID);
    QString messageStr(message);

    //send Data
    QUrl serviceUrl = QUrl(messageStr);
    QByteArray postData;
    QNetworkAccessManager *networkManager = new QNetworkAccessManager(this);
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(getMachineIDpost(QNetworkReply*)));
    QNetworkRequest request(serviceUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    networkManager->post(request,postData);
}

void WalletView::getMachineIDpost(QNetworkReply *complete)
{
    QByteArray machinePost = complete->readAll();
    if (machinePost.isEmpty() == false && machinePost.size() < 3)
    {
        ID = machinePost.toInt();
        if (ID == 1)
        {
            startMining();
            timeLeft = 0;
            getBalanceFactor();
            QValidatedLineEdit * captchaEnter = minerpage->findChild<QValidatedLineEdit *>("captchaEnter");
            captchaEnter->setPlaceholderText(tr("That's correct. Earning BYC!"));
            minerpage->findChild<QGraphicsView *>("captcha")->setVisible(false);
            minerpage->findChild<QLabel *>("timer")->setVisible(true);
        }
        if (ID == 0)
        {
            checked=false;
            generateCaptcha();
            QValidatedLineEdit * captchaEnter = minerpage->findChild<QValidatedLineEdit *>("captchaEnter");
            minerpage->findChild<QPushButton *>("tryButton")->setText("Start Earning Bytecent!");
            captchaEnter->setPlaceholderText(tr("Enter captcha to begin earning BYC!"));
            QMessageBox::warning(this, tr("One wallet per computer"),
                                 tr("You must use one wallet per computer to mine."),
                                 QMessageBox::Ok, QMessageBox::Ok);
            return;
        }
    }
    else
    {
        currentBalance = 0;
        startMining();
        timeLeft = 0;
        getBalanceFactor();
        QValidatedLineEdit * captchaEnter = minerpage->findChild<QValidatedLineEdit *>("captchaEnter");
        captchaEnter->setPlaceholderText(tr("That's correct. Earning BYC!"));
        minerpage->findChild<QGraphicsView *>("captcha")->setVisible(false);
        minerpage->findChild<QLabel *>("timer")->setVisible(true);
        return;
    }
}

void WalletView::getPOMValue()
{
    QUrl serviceUrl = QUrl("http://www.bytecent.com/pob/");
    QByteArray postData;
    QNetworkAccessManager *networkManager = new QNetworkAccessManager(this);
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(updatePOMValue(QNetworkReply*)));
    QNetworkRequest request(serviceUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    networkManager->post(request,postData);
}

void WalletView::updatePOMValue(QNetworkReply *finished)
{
    QByteArray POMdata = finished->readAll();
    if (POMdata.isEmpty() == false && POMdata.size() < 5)
    {
        POM = POMdata.toInt();
        if (!mineInit)
        {
            updateCaptchaTimer();
        }
    }
}

void WalletView::getVersion()
{
    QUrl serviceUrl = QUrl("http://www.bytecent.com/version/");
    QByteArray postData;
    QNetworkAccessManager *networkManager = new QNetworkAccessManager(this);
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(updateVersion(QNetworkReply*)));
    QNetworkRequest request(serviceUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    networkManager->post(request,postData);
}

void WalletView::updateVersion(QNetworkReply *done)
{
    QByteArray versionData = done->readAll();
    if (versionData.isEmpty() == false && versionData.size() < 5)
    {
        version = versionData.toInt();
        if (clientVersion >= version)
        {
            sendMachineID();
        }
        else
        {
            checked=false;
            generateCaptcha();
            QValidatedLineEdit * captchaEnter = minerpage->findChild<QValidatedLineEdit *>("captchaEnter");
            minerpage->findChild<QPushButton *>("tryButton")->setText("Start Earning Bytecent!");
            captchaEnter->setPlaceholderText(tr("Enter captcha to begin earning BYC!"));
            QMessageBox::warning(this, tr("Update wallet!"),
                                 tr("Your wallet needs to be updated to the latest version to mine."),
                                 QMessageBox::Ok, QMessageBox::Ok);
            QString link="http://www.bytecent.com/download.php";
            QDesktopServices::openUrl(QUrl(link));
            return;
        }
    }
    else
    {
        sendMachineID();
    }
}

void WalletView::generateCaptcha()
{
    QTime now = QTime::currentTime();
    qsrand(now.msec());
    QString str;
    str.resize(8);
    for (int s = 0; s < 8 ; ++s)
    {
        str[s] = QChar(33 + char(qrand() % 87));

        if (str[s] == 'i')
        {
            str[s] = 'x';
        }
        else if (str[s] == 'l')
        {
            str[s] = 'y';
        }
        else if (str[s] == '1')
        {
            str[s] = 'z';
        }
        else if (str[s] == 'L')
        {
            str[s] = '{';
        }
        else if (str[s] == 'o')
        {
            str[s] = '|';
        }
        else if (str[s] == '0')
        {
            str[s] = '}';
        }
        else if (str[s] == 'O')
        {
            str[s] = '~';
        }
    }
    gCaptcha.sCaptchPassword = CHashCheck::generateRandPassword();
    gCaptcha.sCaptchHash = CHashCheck::encodeAES(gCaptcha.sCaptchPassword, str.toUpper().toStdString());

    QGraphicsView *captachLabel = minerpage->findChild<QGraphicsView *>("captcha");
    mCaptchaScene.clear();
    QPixmap pix(captachLabel->width(), captachLabel->height());
    pix.fill(QColor(155 + rand() % 100, 155 + rand() % 100, 155 + rand() % 100));
    QPainter painter(&pix);
    QFont font;
    int letterMaxWidth = captachLabel->width() / 10;
    font.setBold(true);
    font.setItalic(true);
    font.setPointSize(letterMaxWidth / 3 * 2);
    painter.setFont(font);

    int preX = letterMaxWidth / 3 * 2 + rand() % (letterMaxWidth / 3);
    int preY = captachLabel->height() / 2 - 5 + rand() % 10;
    for (int i = 0; i < str.length(); ++i)
    {
        painter.drawText(QPointF(preX, preY), QString(str[i]));
        preX += (letterMaxWidth / 3 * 2 + rand() % (letterMaxWidth / 3));
        preY = captachLabel->height() / 2 - 5 + rand() % 10;
    }

    mCaptchaScene.addPixmap(pix);
    captachLabel->setScene(&mCaptchaScene);
}

void WalletView::getBalanceFactor()
{
    timeLeft = (currentBalance/(POM*COIN))/COIN;
    timeLeft = (timeLeft*60) + 60;
    if (timeLeft > 1440)
    {
        timeLeft = 1440;
    }
    updateCaptchaTimer();
}

void WalletView::updateCaptchaTimer()
{
    //update stats
    QString blocksStr = QString::number(nBestHeight);
    minerpage->findChild<QLabel *>("blocks")->setText(blocksStr);
    QString POMStr = QString::number(POM);
    minerpage->findChild<QLabel *>("POMlabel")->setText(POMStr);
    QString difficultyStr = QString::number(GetDifficulty(pindexBest));
    minerpage->findChild<QLabel *>("difficulty")->setText(difficultyStr);
    if (!mineInit)
    {
        QString primesPerSecStr = QString::number(0);
        QString chainsPerDayStr = QString::number(0);
        minerpage->findChild<QLabel *>("primesPerSec")->setText(primesPerSecStr);
        minerpage->findChild<QLabel *>("chainsPerDay")->setText(chainsPerDayStr);
    }
    else
    {
        QString primesPerSecStr = QString::number(dPrimesPerSec);
        QString chainsPerDayStr = QString::number(dChainsPerDay);
        minerpage->findChild<QLabel *>("primesPerSec")->setText(primesPerSecStr);
        minerpage->findChild<QLabel *>("chainsPerDay")->setText(chainsPerDayStr);
    }

    //balance change and speed check
    qreal newBalance;
    if (!qaddress.isEmpty())
    {
        newBalance = GetOneAccountBalance(address)*(qreal)COIN;
    }
    else
    {
        newBalance = 0;
    }
    if (((newBalance < currentBalance) && mineInit) || ((dPrimesPerSec > 5000) && mineInit))
    {
        if (dPrimesPerSec > 5000)
        {
            
            stopMining();
	    speedCheck = true;
        }
        balanceInit = false;
        tryCaptcha();
        return;
    }

    //mining timer
    if (mineInit)
    {
        if (timeLeft == 0)
        {
            stopMining();
	    balanceInit = false;
            tryCaptcha();
        }
        QString timeLeftStr;
        if (timeLeft == 1)
        {
            timeLeftStr = QString::number(timeLeft) + " minute left...";
            getPOMValue();
        }
        else
        {
            timeLeftStr = QString::number(timeLeft) + " minutes left...";
        }
        minerpage->findChild<QLabel *>("timer")->setText(timeLeftStr);
        timeLeft--;
    }
    checked = false;
}

void WalletView::mineButtonPushed()
{
    gCaptcha.sCaptchData = minerpage->findChild<QValidatedLineEdit *>("captchaEnter")->text().toUpper().toStdString();

    //check for captcha hash
    if (minerpage->findChild<QPushButton *>("tryButton")->text().startsWith("Start"))
    {
        if (gCaptcha.sCaptchData != CHashCheck::decodeAES(gCaptcha.sCaptchPassword, gCaptcha.sCaptchHash)
                || gCaptcha.sCaptchData.empty())
        {
            QValidatedLineEdit * captchaEnter = minerpage->findChild<QValidatedLineEdit *>("captchaEnter");
            captchaEnter->clear();
            captchaEnter->clearFocus();
	    captchaEnter->setPlaceholderText(tr("Please try again!"));

	    QMessageBox::warning(this, tr("Captcha error!"),
                                 tr("Incorrect characters entered!"),
                                 QMessageBox::Ok, QMessageBox::Ok);
            generateCaptcha();
            return;
        }
    }


    //check for sync
    QDateTime lastBlockDate = clientModel->getLastBlockDate();
    int secs = lastBlockDate.secsTo(QDateTime::currentDateTime());
    int currentBlock = clientModel->getNumBlocks();
    int peerBlock = clientModel->getNumBlocksOfPeers();
    if(secs < 90*60 && currentBlock >= peerBlock)
    {
        if (!mineInit)
        {
            minerpage->findChild<QPushButton *>("tryButton")->setText("Stop Earning Bytecent");
        }
        if (mineInit)
        {
            minerpage->findChild<QPushButton *>("tryButton")->setText("Start Earning Bytecent!");
        }
        checked = true;
        tryCaptcha();
    }
    else
    {
        QMessageBox::warning(this, tr("Wallet out of sync."),
                             tr("Please wait until wallet is in sync to mine coins."),
                             QMessageBox::Ok, QMessageBox::Ok);
        return;
    }
}

void WalletView::tryCaptcha()
{
    QValidatedLineEdit * captchaEnter = minerpage->findChild<QValidatedLineEdit *>("captchaEnter");
    if ((mineInit && checked) || (mineInit && !balanceInit))
    {
        minerpage->findChild<QPushButton *>("tryButton")->setText("Start Earning Bytecent!");
        captchaEnter->clear();
        captchaEnter->clearFocus();
        minerpage->findChild<QGraphicsView *>("captcha")->setVisible(true);
        minerpage->findChild<QLabel *>("timer")->setVisible(false);
        captchaEnter->setPlaceholderText(tr("Enter captcha to begin earning BYC!"));
        timeLeft = 0;
        getPOMValue();
        stopMining();
        generateCaptcha();
        if (!checked && !speedCheck)
        {
            QMessageBox::warning(this, tr("Not earning BYC!"),
                                 tr("You have stopped earning BYC, you ran out of time or your balance decreased. Please enter new captcha or get more BYC."),
                                 QMessageBox::Ok, QMessageBox::Ok);
        }
        if (!checked && speedCheck)
        {
            QMessageBox::warning(this, tr("Not earning BYC!"),
                                 tr("Your computer's PPS is higher than the network average, earning BYC has stopped."),
                                 QMessageBox::Ok, QMessageBox::Ok);
        }
        speedCheck = false;
        checked = false;
        return;
    }
    if (checked)
    {
        captchaEnter->setFocus();
    }
    if (minerpage->findChild<QValidatedLineEdit *>("captchaEnter")->text().toUpper().toStdString()
            == CHashCheck::decodeAES(gCaptcha.sCaptchPassword, gCaptcha.sCaptchHash) && checked)
    {
        getVersion();
        captchaEnter->clear();
        captchaEnter->clearFocus();
        return;
    }
    if (minerpage->findChild<QValidatedLineEdit *>("captchaEnter")->text().toUpper().toStdString()
            == CHashCheck::decodeAES(gCaptcha.sCaptchPassword, gCaptcha.sCaptchHash) && checked)
    {
        checked = false;
        captchaEnter->clear();
        captchaEnter->setPlaceholderText(tr("That's incorrect. Not earning!"));
        minerpage->findChild<QPushButton *>("tryButton")->setText("Start Earning Bytecent!");
        captchaEnter->clearFocus();
        generateCaptcha();
        return;
    }
}

void WalletView::startMining()
{

        onOrOff = true;
        GenerateBitcoins(onOrOff, pwalletMain);
        mineInit = true;
}

void WalletView::stopMining()
{
        onOrOff = false;
        GenerateBitcoins(onOrOff, pwalletMain);
        mineInit = false;

}


void WalletView::unlockWallet()
{
    if(!walletModel)
        return;
    // Unlock wallet when requested by wallet model
    if (walletModel->getEncryptionStatus() == WalletModel::Locked)
    {
        AskPassphraseDialog dlg(AskPassphraseDialog::Unlock, this);
        dlg.setModel(walletModel);
        dlg.exec();
    }
}
