#ifndef CAPTCHA
#define CAPTCHA

#include "net.h"

struct Captcha {
    std::string sCaptchData;
    std::string sCaptchHash;
    std::string sCaptchPassword;
};

#endif // CAPTCHA

