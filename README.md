Bytecent integration/staging tree
==================================

http://www.Bytecent.com

Copyright (c) 2019 Bytecent Developers


What is Bytecent?
------------------

The Bytecent social rewards network is powered by blockchain
technology, and computers across 150+ countries keep our network
up and running through mining. Mining Bytecent is the process of
automatically verifying transactions while assisting with securing
the Bytecent network. People who mine Bytecent are called �miners.�

Miners running the free Bytecent software can earn Bytecent by using
the power of their CPU to help process transactions. Mining Bytecent
is the digital equivalent of panning for gold, and the network rewards
miners when their PC processes a transaction/block. Bytecent requires
no special hardware or complex configurations to begin mining Bytecent.

There are approximately 1440 � 2050 Bytecent generated per day, so
mining new Bytecent can take a few hours or even a few days depending
on the speed of your computer. Simply download the software, install
it, and begin mining Bytecent today!


