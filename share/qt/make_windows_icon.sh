#!/bin/bash
# create multiresolution windows icon
ICON_SRC=../../src/qt/res/icons/bytecent.png
ICON_DST=../../src/qt/res/icons/bytecent.ico
convert ${ICON_SRC} -resize 16x16 bytecent-16.png
convert ${ICON_SRC} -resize 32x32 bytecent-32.png
convert ${ICON_SRC} -resize 48x48 bytecent-48.png
convert ${ICON_SRC} -resize 64x64 bytecent-64.png
convert bytecent-32.png ${ICON_SRC} bytecent-64.png bytecent-48.png bytecent-32.png bytecent-16.png ${ICON_DST}

