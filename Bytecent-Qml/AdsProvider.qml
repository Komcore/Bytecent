import QtQuick 2.5

Item {

    property string ads728x90Url
    property string ads300x250Url
    property string siteUrl

    property var adsUrls: []  
    property int currentsAdsIndex: -1   


    Component.onCompleted: {  
        requestAdsList();
    }

    function requestAdsList() {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function processRequest()   {
            if ( xmlHttp.readyState == 4 && xmlHttp.status == 200 )    {
                var l = xmlHttp.responseText.split("\r\n")
                adsUrls = [];
                for(var i = 0; i < l.length; i ++) {
                    adsUrls.push(l[i])
                    currentsAdsIndex = 0;

                }
                if(currentsAdsIndex == 0) {
                    adsTimer.start();
                    nextAds();
                }
            }
            if(currentsAdsIndex == -1 && xmlHttp.readyState == 4) {
                adsListTimer.start()
            }

        }

        xmlHttp.open( "GET",  "https://bytecent.com/app-ad-urls/index.php", true );
        xmlHttp.send( null );
    }


    function nextAds() {
        ads728x90Url = adsUrls[currentsAdsIndex] + "728x90.php";
        ads300x250Url = adsUrls[currentsAdsIndex] + "300x250.php";
        currentsAdsIndex ++;
        currentsAdsIndex = currentsAdsIndex % adsUrls.length;
    }

    Timer {
        id: adsTimer
        repeat: true
        interval: constants.adsShowInterval
        onTriggered: nextAds()
    }
    Timer {
        id: adsListTimer
        interval: 300
        onTriggered: requestAdsList()
    }

}
