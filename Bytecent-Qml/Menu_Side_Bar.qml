import QtQuick 2.5
import WalletModel 1.0
Rectangle {
    id: root
    signal entryClicked(string name)
    ListModel {
        id: sideBarModel

        ListElement {
            name: "dashboard"
            caption: qsTr("Dashboard")
            iconUrl: "res/dashboard.png"
            selectable: true
        }
        ListElement {
            name: "earn"
            caption: qsTr("Earn")
            iconUrl: "res/earn.png"
            selectable: true
        }
        ListElement {
            name: "receive"
            caption: qsTr("Receive BYC")
            iconUrl: "res/receive.png"
            selectable: true
        }
        ListElement {
            name: "send"
            caption: qsTr("Send BYC")
            iconUrl: "res/send.png"
            selectable: true
        }
        ListElement {
            name: "transactions"
            caption: qsTr("Transactions")
            iconUrl: "res/transactions.png"
            selectable: true
        }
        ListElement {
            name: "contacts"
            caption: qsTr("Contacts")
            iconUrl: "res/contacts.png"
            selectable: true
        }
        ListElement {
            name: "trade"
            caption: qsTr("Trade BYC")
            iconUrl: "res/buy.png"
            selectable: false
        }
        ListElement {
            name: "services"
            caption: qsTr("Services")
            iconUrl: "res/merchants.png"
            selectable: false
        }
        ListElement {
            name: "community"
            caption: qsTr("Community")
            iconUrl: "res/community.png"
            selectable: false
        }
        ListElement {
            name: "faqs"
            caption: qsTr("FAQs")
            iconUrl: "res/faqs.png"
            selectable: false
        }
        ListElement {
            name: "options"
            caption: qsTr("Options")
            iconUrl: "img/Icon_11.png"
            selectable: false
        }
    }
    property string activeTab: sideBarModel.get(0).name
    property bool menuActive: false

    color: constants.sideBarColor
    border.color: "black"
    Column {
        anchors.fill: parent
        anchors.topMargin: 1
        spacing: -1
        Repeater {
            model: sideBarModel
            delegate: sidebarEntryDeleg
        }
    }
    Component {
        id: sidebarEntryDeleg
        Rectangle {
            id: rect
            property bool sel: selectable
            property var d: caption
            property var tabName: name
            property string iconPath: iconUrl
            property bool isActive: tabName == activeTab && !menuActive || tabName == "options" && menuActive
            width: parent.width
            height: 47
            color: ma.containsMouse ? Qt.lighter(mainColor, 2): mainColor
            property color mainColor: isActive ? constants.sideBarSelectionColor: constants.sideBarColor
            border.color: constants.sideBarSelectionColor
            Image {
                id: icon
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 17
                source: iconPath
                width: 22
                height: 22
                opacity: isActive ? 1.0: 0.5
            }

            TextLabel {
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: icon.right
                anchors.leftMargin: 5
                text: parent.d
                color: isActive ? "white": constants.sidebarTextColorInactive
                font.pixelSize: constants.sidebarFontSize
                font.family: mainFont.name
            }
            Rectangle {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: 3
                color: constants.sidebarSelectorColor
                visible: isActive
            }
            MouseArea{
                anchors.fill: parent
                id: ma
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
                onClicked: {

                    if(!parent.sel) {
//                        if (parent.tabName == "options")
//                            menuActive = true;
                    }
                    else {
                        activeTab = tabName;
                        interfaceTabActivated(tabName)
                    }
                    entryClicked(tabName)
                }
            }
        }
    }
    onEntryClicked: {
        menuActive = false;
        if(name == "trade")
            Qt.openUrlExternally("https://www.bytecent.com/trade.php");
        if(name == "services")
            Qt.openUrlExternally("https://www.bytecent.com/services.php");
        else if(name == "community")
            Qt.openUrlExternally("https://www.byctalk.com");
        else if(name == "faqs")
            Qt.openUrlExternally("https://www.bytecent.com/faqs.php");
        else if(name == "options")
            menuActive = true;
    }

    Row {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: constants.uiBorders/2
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: constants.uiBorders/2
        Image {
            visible: walletModel.walletEncryptionStatus != WalletModel.Unencrypted
            source: {
                if(walletModel.walletEncryptionStatus == WalletModel.Unlocked)
                    "icons/lock_open"
                else if(walletModel.walletEncryptionStatus == WalletModel.Locked)
                    "res/lock.png"
                else
                    ""
                }
            width: 20
            height: 20
            fillMode: Image.PreserveAspectFit
            anchors.bottom: parent.bottom
            MouseArea {
                anchors.fill: parent
                hoverEnabled: true

                onExited: statusTooltip.visible = false
                onPositionChanged: {

                    if(walletModel.walletEncryptionStatus == WalletModel.Unlocked)
                        statusTooltip.text = qsTr("Wallet UnLocked");
                    else if(walletModel.walletEncryptionStatus == WalletModel.Locked)
                        statusTooltip.text = qsTr("Wallet Locked");



                    statusTooltip.setPos(parent, mouse)
                    statusTooltip.visible = true
                }
            }
        }
        Image {
            source: bycClientModel.connectionsNumber > 4 ? "res/Connections4.png" : ("res/Connections%1.png").arg(bycClientModel.connectionsNumber)
            anchors.bottom: parent.bottom
            width: 20
            height: 20
//            visible:bycClientModel.connectionsNumber>0
            fillMode: Image.PreserveAspectFit
            MouseArea {
                anchors.fill: parent
                hoverEnabled: true

                onExited: statusTooltip.visible = false
                onPositionChanged: {
                    statusTooltip.text = qsTr("Connections: %1").arg(bycClientModel.connectionsNumber)
                    statusTooltip.setPos(parent, mouse)
                    statusTooltip.visible = true
                }
            }
        }
        Image {
            source: "res/Connectiom Status.png"
            anchors.bottom: parent.bottom
            width: 20
            height: 20
            fillMode: Image.PreserveAspectFit

            visible:bycClientModel.connectionsNumber>0
            MouseArea {
                anchors.fill: parent
                hoverEnabled: true

                onExited: statusTooltip.visible = false
                onPositionChanged: {
                    statusTooltip.text = qsTr("Connected to the Bytecent network")
                    statusTooltip.setPos(parent, mouse)
                    statusTooltip.visible = true
                }
            }
        }
    }
    ToolTip {
        id: statusTooltip
        text: qsTr("Connected to the Bytecent network")
        controlLeftRight: false
    }
}
