#include "AdsWebView.h"
#include <QTimer>
#include <QDebug>
#include <QDesktopServices>
AdsWebView::AdsWebView(QQuickItem *parent):
    QQuickPaintedItem(parent)
{

    setAcceptedMouseButtons(Qt::AllButtons);
    setAcceptHoverEvents(true);
    setAntialiasing(true);
    webView = new QWebView;
    helper = new RenderHelper(webView);
    connect(this, &AdsWebView::imgNeeded, helper, &RenderHelper::requestImage, Qt::QueuedConnection);
    connect(helper, &RenderHelper::image, this, &AdsWebView::fillPixmap, Qt::QueuedConnection);

    connect(this, &AdsWebView::heightChanged, [=] () { webView->setFixedHeight(this->height()*2); update();} );
    connect(this, &AdsWebView::widthChanged,  [=] () { webView->setFixedWidth (this->width()*2);  update();} );
    connect(this, &AdsWebView::xChanged,      [=] () { webView->setFixedWidth (this->width()*2);  update();} );
    connect(this, &AdsWebView::yChanged,      [=] () { webView->setFixedHeight(this->height()*2); update();} );
    connect(webView, &QWebView::loadFinished,  [=] () { update(); /*qDebug() << "loaded: " << webView->url();*/});
    connect(webView, &QWebView::linkClicked, [=] (const QUrl & url) {/*qDebug() << url; */QDesktopServices::openUrl(url);});
    //    webView->show();

    webView->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
    //QTimer::singleShot(3000, [=](){update();});
    setCursor(Qt::PointingHandCursor);
    requested = false;
}

AdsWebView::~AdsWebView()
{
    delete webView;
}

void AdsWebView::setUrl(QString url)
{
    webView->setUrl(url); emit urlChanged();
}

QString AdsWebView::url()
{
    return webView->url().toString();
}

void AdsWebView::fillPixmap(QPixmap pixmap)
{

    this->pixmap = pixmap;
    requested = false;
    update();

}

void AdsWebView::paint(QPainter *painter)
{
    if(!requested)
    {
        helper->moveToThread(webView->thread());
        requested = true;
        emit imgNeeded();
    }
    else
    {
        painter->drawPixmap(0,0 ,pixmap);


    }
    //    webView->render(painter);
}

void AdsWebView::mousePressEvent(QMouseEvent *event)
{
    (void) QApplication::instance()->sendEvent(webView,
                                               event);

    QApplication::processEvents();
}

void AdsWebView::mouseReleaseEvent(QMouseEvent *event)
{

    (void) QApplication::instance()->sendEvent(webView,
                                               event);
    QApplication::processEvents();
}

void AdsWebView::setUrl(QUrl url)
{
    webView->setUrl(url);
}
