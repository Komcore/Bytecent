import QtQuick 2.0

Rectangle {
    id: root
    default property alias content: d.children
    property alias caption: capt.text
    property bool okOnly: false
    property bool cancelOnly: false
    signal rejected()
    signal accepted()
    color: "#99000000"
    visible: false
    anchors.fill: parent

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
    }

    Rectangle {
        id: r
        anchors.centerIn: parent
        width: childrenRect.width
        height: childrenRect.height
        border.color: constants.earnTabPanelBorder
        Rectangle {
            id: header
            color:constants.windowHandleColor
            anchors.top: parent.top
            anchors.left: parent.left
            width: chArea.width
            height: 40
            TextLabel {
                color: "white"
                font.pixelSize: 20
                id: capt
                anchors.centerIn: parent
            }
        }
        Item {
            id: chArea
            anchors.top:header.bottom
            height: d.height + 2*constants.uiBorders
            width: d.width + 2*constants.uiBorders
            Item {
                id: d
                anchors.left: parent.left
                anchors.leftMargin: constants.uiBorders
                anchors.top: parent.top
                anchors.topMargin: constants.uiBorders

                height: childrenRect.height
                width: childrenRect.width
            }
        }
        Item {
            anchors.top: chArea.bottom
            anchors.left: parent.left
            width: chArea.width
            height: 28 + 2*constants.uiBorders
            ButtonType1 {
                id: cancelBtn
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.margins:constants.uiBorders
                text: qsTr("Ok")
                baseColor: "#53a6a6"
                onClicked: root.accepted()
                visible: !cancelOnly
            }
            ButtonType1 {
                baseColor: "#bd5151"
                anchors.top: parent.top
                anchors.right: cancelBtn.visible ? cancelBtn.left: parent.right
                anchors.margins:constants.uiBorders
                anchors.rightMargin: 3
                text: qsTr("Cancel")
                onClicked: {
                    root.visible = false
                    root.rejected()
                }
                visible: !okOnly
            }
        }


    }

}
