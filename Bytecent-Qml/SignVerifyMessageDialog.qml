import QtQuick 2.5

DialogBase {
    caption: mode == "sign" ? qsTr("Sign Message"): qsTr("Verify Message")
    property string mode: "sign"
    property alias address: addressEdit.text
    onVisibleChanged: {
        msgEdit.text = ""
        signatureEdit.text = ""
        if(visible)msgEdit.forceActiveFocus()
    }

    Column {
        spacing: constants.uiBorders
        TextLabel {
            width: 600
            color:constants.textColor
            text: mode == "sign" ?  qsTr("You can sign messages with your addresses to prove you own them. Be careful not to sign anything vague, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to."):
                                   qsTr("Enter the signing address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack.")

            font.pixelSize: 12
            wrapMode: Text.WordWrap
        }
        TextEdit1 {
            id:addressEdit
            width: 600
            height: 34
            placeholder: qsTr("Enter Bytecent Address")
        }
        MultilineTextEdit {
            id: msgEdit
            height: 200
            width: 600

        }
        TextLabel {
            width: 600
            color:constants.textColor
            text: qsTr("Signature")
            font.pixelSize: 12
            wrapMode: Text.WordWrap
        }
        TextEdit1 {
            id: signatureEdit
            width: 600
            height: 34
            placeholder: mode == "sign" ? qsTr("Click \"Sign Message\" to generate signature"): qsTr("Enter Bytecent signature")
            readOnly: mode == "sign"
        }
        ButtonType1 {
            anchors.right: parent.right
            text: mode == "sign" ? qsTr("Sign Message"): qsTr("Verify Message")
            baseColor: "#e78200"
            onClicked: {
                if(mode == "sign") {
                    signatureEdit.text = ""
                    signatureEdit.text = kernelAdapter.signMessage(addressEdit.text, msgEdit.text)
                } else {
                    signatureEdit.text = kernelAdapter.verifyMessage(signatureEdit.text, msgEdit.text, addressEdit.text)
                }
            }
        }
    }
    onAccepted: {
        console.log("Accepted")
        visible = false
    }    
}
