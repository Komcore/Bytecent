import QtQuick 2.0

Rectangle {
    id: root
    property string text
    property bool centerTextAlignment: false
    border.color: constants.earnTabPanelBorder
    Rectangle {
        id: captRect
        anchors.margins: 1
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: 40
        color: constants.earnTabPanelColor1
        Rectangle {
            height: 4
            color: constants.earnTabPanelColor2
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
        }
        TextLabel {
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: -2
            anchors.horizontalCenter: centerTextAlignment ? parent.horizontalCenter : undefined
            anchors.left: centerTextAlignment ? undefined : parent.left
            anchors.leftMargin: 13
            text: root.text
            font.pixelSize: 15
            color: "white"
        }
    }
    property var content
    onContentChanged: {
        content.parent = contRect
    }

    Rectangle {
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: captRect.bottom
        anchors.margins: 1
        anchors.topMargin: 0
        id: contRect
    }

}

