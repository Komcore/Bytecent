import QtQuick 2.3
import QtQuick.Layouts 1.2
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.0
import QtQuick.Controls 1.4

InterfaceTab {
    tabID: "earn"

    Column {
        anchors.fill: parent
        anchors.margins: 11
        spacing: 11

        Rectangle {
            border.color: constants.earnTabPanelBorder
            height: 60
            width: 768
            Rectangle {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                width:3
                anchors.margins: 1
                color: "#5479aa"
            }

            Row {
                anchors.fill: parent
                anchors.leftMargin: 14
                spacing: 11
                Image {
                    source: "res/EarnTabImage1.png"
                    anchors.verticalCenter: parent.verticalCenter
                }
                TextLabel {
                    font.pixelSize: 12
                    text: qsTr("You can earn Bytecent by using your CPU to assist with helping to secure the Bytecent network. <a href=\"http://www.bytecent.com\">Learn more...</a>")
                    onLinkActivated: Qt.openUrlExternally(link)
                    color:constants.textColor
                    linkColor: "#c66868"
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
        }


        Timer {
            interval: 2000
            running: true
            repeat: true
            onTriggered: {

                networkStatsModel.set(0, {'value': kernelAdapter.getDifficulty().toFixed(4).toString()})
                chainsPerDayChart.bottomText = miningController.getChainsPerDay().toFixed(4).toString();
                chainsPerDayChart.endValue = miningController.getChainsPerDay();
                ppsChart.bottomText =miningController.getPrimesPerSecond().toFixed(4).toString();
                ppsChart.endValue =miningController.getPrimesPerSecond()/10.0;
                networkStatsModel.set(2, {'value': Qt.formatDateTime(kernelAdapter.getLastBlockDate(), "hh:mm:ss")})

            }

        }

        Row {
            spacing: 10
            EarnTabPanel {
                width: 367
                height: 162
                text: qsTr("Earnings Statistics")
                content: Row {

                    anchors.leftMargin: 66
                    anchors.rightMargin: 66
                    anchors.fill: parent
                    spacing: 30
                    PieDiagram {
                        id: ppsChart
                        anchors.verticalCenter: parent.verticalCenter
                        width: 103
                        height: 103
                        endValue: 0

                        topText: qsTr("PPS")
                        bottomText: qsTr("0")
                        Behavior on endValue { NumberAnimation {duration: 1000}}


                    }
                    PieDiagram {
                        id: chainsPerDayChart
                        anchors.verticalCenter: parent.verticalCenter
                        width: 103
                        height: 103
                        endValue: 0
                        topText: qsTr("CPD")
                        bottomText: qsTr("0")
                        Behavior on endValue { NumberAnimation {duration: 1000}}

                    }
                }
            }
            EarnTabPanel {
                width: 73
                height: 162
                text: qsTr("CPU")
                centerTextAlignment: true


                content: Rectangle {
                    color: "white"
                    anchors.fill: parent
                    anchors.margins: 17
                    anchors.leftMargin: 13
                    anchors.rightMargin: 15
                    Rectangle {
                        id: cpuUsageIndicator
                        anchors.bottom: parent.bottom
                        anchors.right: parent.right
                        color: "#5376a5"
                        border.color: "#afc0d5"
                        width: 23
                        height: systemResourceReporter.cpuLoad * parent.height + 1
                        Behavior on height {
                            NumberAnimation { duration: 500 }
                        }
                    }

                    Column {
                        anchors.top: parent.top
                        anchors.left: parent.left
                        anchors.bottom: parent.bottom
                        anchors.right: cpuUsageIndicator.left
                        spacing: (height - 3) / 2 - 2
                        Repeater {
                            model: 3
                            Rectangle {
                                anchors.right: parent.right
                                width: 5
                                height: 1
                                color: "#eaeaea"
                                TextLabel {
                                    text: (2 - index) * 50
                                    font.pixelSize: 9
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    //                                    anchors.right: parent.right
                                    anchors.horizontalCenterOffset: -14
                                    color: "#bd5151"
                                }
                            }
                        }
                    }
                }
            }
            EarnTabPanel {
                width: 308
                height: 162
                text: qsTr("Network Statistics")




                ListModel {
                    id: networkStatsModel
                    ListElement {
                        caption: qsTr("Difficulty")
                        value: "..."
                    }
                    ListElement {
                        caption: qsTr("Blocks")
                        value: "..."
                    }
                    ListElement {
                        caption: qsTr("Last Block Time")
                        value: "..."
                    }
                    Component.onCompleted: {
                        bycClientModel.blocksNumberChanged.connect(function() {
                            set(1, {'value': bycClientModel.blocksNumber.toString()})
                        }
                        )
                    }

                }
                content: ListView {
                    anchors.fill: parent
                    anchors.leftMargin: 22
                    anchors.topMargin: 8
                    boundsBehavior: Flickable.StopAtBounds
                    model: networkStatsModel
                    delegate: Rectangle{
                        color: "transparent"
                        width:parent.width
                        height: 26
                        TextLabel {
                            anchors.left: parent.left
                            anchors.verticalCenter: parent.verticalCenter
                            text: caption + ":"
                            color: constants.textColor
                            font.pixelSize: 13

                        }
                        TextLabel {
                            text: value
                            anchors.left: parent.left
                            anchors.leftMargin: 189
                            anchors.verticalCenter: parent.verticalCenter
                            color: "#c66868"
                            font.pixelSize: 13
                        }
                    }
                }
            }
        }

        Row {
            spacing: 10
            EarnTabPanel {
                width: 450
                height: 298
                text: qsTr("Mining Setup")
                content: Rectangle {
                    anchors.fill: parent
                    anchors.topMargin: 14
                    anchors.bottomMargin: 15
                    anchors.leftMargin: 10
                    anchors.rightMargin: 9

                    Column {
                        anchors.topMargin: -9
                        anchors.fill: parent
                        spacing: 12
                        Rectangle {
                            id: captchaPanel
                            width: 429
                            height: 90
                            color: "#f6f6f6"
                            property string correctCode: ""
                            onCorrectCodeChanged: {

                                miningController.saveCaptcha(correctCode)

                            }
                            property int curSymbolIndex: -1
                            //                            onCurSymbolIndexChanged: console.log("curSymbol", correctCode[curSymbolIndex])
                            
                            border.color: constants.earnTabPanelBorder

                            Row {


                                anchors.fill: parent
                                anchors.margins: 10
                                spacing: 10
                                Image {
                                    source: "res/EarnTabPlay.png"
                                    anchors.verticalCenter: parent.verticalCenter
                                    MouseArea {
                                        anchors.fill: parent
                                        cursorShape: Qt.PointingHandCursor
                                        onClicked: {

                                            generateCaptcha()
                                        }
                                        visible: parent.opacity != 0

                                    }
                                    opacity: ! miningController.miningActive ? 1.0: 0.0
                                    Behavior on opacity { NumberAnimation {duration: 200}}
                                }
                                Timer {
                                    id: codeDisplayTimer
                                    repeat: false
                                    onTriggered: {

                                        if(captchaPanel.curSymbolIndex < captchaPanel.correctCode.length - 1) {
                                            captchaPanel.curSymbolIndex = captchaPanel.curSymbolIndex + 1;
                                            codeDisplayTimer.interval = Math.random() * 1000 + 2000;
                                            codeDisplayTimer.start()
                                        }
                                        else {
                                            captchaPanel.curSymbolIndex = -1

//                                            if(kbdGrid.allInput == captchaPanel.correctCode) {
//                                                console.log("code is correct")
//                                            }
//                                            else
//                                                console.log("code is not correct")
                                        }
                                    }
                                }

                                Rectangle {
                                    clip: true
                                    color: "#ececec"
                                    width: 180
                                    height: 71
                                    border.color: constants.earnTabPanelBorder
                                    Repeater {
                                        model: captchaPanel.correctCode.length
                                        TextLabel {
                                            text: ([qsTr('Zero'), qsTr('One'), qsTr('Two'), qsTr('Three'), qsTr('Four'),
                                                    qsTr('Five'), qsTr('Six'), qsTr('Seven'), qsTr('Eight'), qsTr('Nine')])[captchaPanel.correctCode[index]]
                                            font.pixelSize: 13
                                            color: "#5376a5"
                                            //                                           scale: Math.random() + 0.8
                                            //                                           rotation: Math.random() * 60.0 - 30.0
                                            id: captchaChunk
                                            opacity: captchaPanel.curSymbolIndex == index ? 1.0:0.0
                                            Behavior on opacity { NumberAnimation {duration: 500}}
                                            x: parent != null ? Math.random() * (parent.width - paintedWidth) : -1000
                                            y: parent != null ? Math.random() * (parent.height - paintedHeight) : -1000
                                        }
                                    }

                                    Column {
                                        anchors.centerIn: parent
                                        width: parent.width - 2*constants.uiBorders
                                        TextLabel {
                                            Behavior on opacity { NumberAnimation {duration: 200}}
                                            wrapMode: "WordWrap"
                                            width: parent.width
                                            color: "#5376a5"
                                            id: msgBackend
                                            font.pixelSize: 11
                                            opacity: 0
                                            visible: opacity!=0


                                        }
                                        TextLabel {
                                            Behavior on opacity { NumberAnimation {duration: 200}}
                                            width: parent.width
                                            wrapMode: "WordWrap"
                                            color: "#5376a5"
                                            id: timeLeftLabel
                                            font.pixelSize: 11
                                            opacity: 0
                                            visible: opacity!=0 && miningController.miningActive
                                        }
                                    }

                                }
                                GridLayout {
                                    id: kbdGrid

                                    opacity: ! miningController.miningActive ? 1.0: 0.0
                                    Behavior on opacity { NumberAnimation {duration: 200}}
                                    width: 181
                                    height: 71
                                    columns: 5
                                    columnSpacing: 0
                                    rowSpacing: 7
                                    property var keyOrder: [4,7,9,3,2,8,1,6,5,0]
                                    property string allInput
                                    onAllInputChanged: {miningController.setInput(allInput); /*console.log('captchaPanel.correctCode, allInput', captchaPanel.correctCode, allInput)*/}
                                    Repeater {
                                        model: kbdGrid.keyOrder.length
                                        Item {
                                            id: deleg
                                            height: btnImg.height
                                            width: btnImg.width
                                            property bool accepted: false
                                            Image {
                                                id: btnImg
                                                source: "res/EarnTab_" + kbdGrid.keyOrder[index] + ".png"
                                                MouseArea {
                                                    id: ma
                                                    hoverEnabled: true
                                                    anchors.fill: parent
                                                    cursorShape: Qt.PointingHandCursor
                                                    onClicked: {
                                                        //                                                        if(captchaPanel.correctCode[captchaPanel.curSymbolIndex] == kbdGrid.keyOrder[index]) {
                                                        deleg.accepted = true
                                                        kbdGrid.allInput += kbdGrid.keyOrder[index]
                                                        //                                                        }
                                                    }
                                                    visible: kbdGrid.opacity != 0
                                                }
                                            }
                                            Component.onCompleted: captchaPanel.onCorrectCodeChanged.connect(function(){accepted = false; kbdGrid.allInput = "";})

                                            Colorize {
                                                id: kbdDtnEff
                                                visible: ma.containsMouse || deleg.accepted
                                                anchors.fill: btnImg
                                                source: btnImg
                                                hue: deleg.accepted ? 0.47: 0.0
                                                saturation: 0.32
                                                lightness: -0.05
                                            }
                                        }
                                    }
                                }
                            }


                        }
                        TextLabel {
                            color: "#c15c5c"
                            text: "Press the green play button to view your 4 digit code, and then enter your unique code using the blue keypad and your mouse. You will begin mining after you have entered the correct code and clicking the start button."
                            width: 429
                            height: 38
                            wrapMode: Text.Wrap
                            font.pixelSize: 11
                            verticalAlignment: Text.AlignBottom
                            opacity: miningController.miningActive ? 0:1
                            Behavior on opacity { NumberAnimation {duration: 200}}
                        }
                        Rectangle {
                            width: 50
                            height: 30
                            border.color: "#b8b8b8"
                            color: "#e5e3e3"
                            TextLabel {
                                anchors.centerIn: parent
                                text: miningController.miningActive ? qsTr("Stop"):qsTr("Start")
                                color: "#666565"
                                font.pixelSize: 13
                            }
                            MouseArea {
                                anchors.fill: parent
                                onClicked: tryStart()
                                visible:kbdGrid.allInput.length >= captchaPanel.correctCode.length
                                cursorShape: Qt.PointingHandCursor

                            }
                            opacity: kbdGrid.allInput.length < captchaPanel.correctCode.length || captchaPanel.correctCode.length == 0? 0:1
                            Behavior on opacity { NumberAnimation {duration: 200}}
                        }
                    }
                }
            }
            EarnTabPanel {
                width: 308
                height: 298
                text: qsTr("Trade Bytecent")
                centerTextAlignment: false

                content: Item {
                    width: 300
                    height: 250
                    anchors.left: parent.left
                    anchors.top: parent.top

                    anchors.margins: 5
                    clip: true
                    AdsDisplay {
                        id: earnTabdAds

                        width: 300*10
                        height: 250*10
                        adsUrl: mainWindow.ads300x250Url
                    }
                }
            }
        }
    }
    function stop() {
        if(miningController.miningActive)
            miningController.mineButtonPushed()
    }


    function tryStart() {
        if(miningController.miningActive)
            resetCaptcha();
        miningController.mineButtonPushed()

    }
    function resetCaptcha() {
        function shuffle(o){
            for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
            return o;
        }
        var numbers = [0,1,2,3,4,5,6,7,8,9]
        //        numbers = shuffle(numbers)

        captchaPanel.correctCode = ""
        var t = ""
        for(var i = 0; i < 4; i ++) {
            var index = Math.round((Math.random() * (numbers.length - 1)))
            t += numbers[index];
            numbers.splice(index, 1);
            //            console.log(numbers)
        }
        captchaPanel.correctCode = t;

        kbdGrid.keyOrder = shuffle(kbdGrid.keyOrder);
        codeDisplayTimer.interval = Math.random() * 1000 + 2000;

        msgBackend.opacity = 0.0
    }

    function generateCaptcha() {
        resetCaptcha();
        captchaPanel.curSymbolIndex = 0
        codeDisplayTimer.start();
    }
    Component.onCompleted : {
        miningController.miningBackendEvent.connect(function(msg){console.log(msg);msgBackend.text = msg;msgBackend.opacity = 1.0})
        miningController.miningTimeLeft.connect(function(msg){console.log(msg);timeLeftLabel.text = msg;timeLeftLabel.opacity = 1.0})

        miningController.captchaError.connect(function() {captchaErrorDialog.show();resetCaptcha();})
        miningController.miningActiveChanged.connect(
                    function(){
                        if(!miningController.miningActive) {
                            resetCaptcha();
                        }
                    });
    }

    MessageDialog {
        id: messageDialog

        onAccepted: {
            console.log("messageDialog agree.")
        }
    }
    Window {
        id: captchaErrorDialog

        modality: Qt.ApplicationModal
        width: 300
        height: 100
        title: "Bytecent"
        visible: false
        color: constants.backgroundColor
        flags: Qt.Dialog
        Column {
            anchors.centerIn: parent
            spacing: constants.uiBorders
            TextLabel {
                id: txt1
                text: qsTr("Captcha is incorrect")
            }
            Button {
                text: qsTr("Ok")
                onClicked: captchaErrorDialog.hide()
            }

        }

    }

}
