
QT += core gui widgets qml quick
CONFIG += c++11
SOURCES +=  \
    #$$PWD/AdsWebView.cpp \
    $$PWD/Clipboard.cpp \
    $$PWD/SystemResourceReporter.cpp \
    $$PWD/QmlUI.cpp \
    $$PWD/MiningController.cpp \
    $$PWD/FilterProxyModel.cpp

RESOURCES += $$PWD/qml.qrc \
    $$PWD/images.qrc \
    $$PWD/icons.qrc \
    $$PWD/fonts.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =


HEADERS += \
    #$$PWD/AdsWebView.h \
    $$PWD/Clipboard.h \
    $$PWD/SystemResourceReporter.h\
    $$PWD/QmlUI.h \
    $$PWD/MiningController.h \
    $$PWD/FilterProxyModel.h
win32: LIBS += -lpdh
OTHER_FILES += *.qml

INCLUDEPATH += $$PWD

lupdate_only{
SOURCES = *.qml
}
