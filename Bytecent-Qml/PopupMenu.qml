import QtQuick 2.5
import QtQuick.Window 2.0
Rectangle {
    MouseArea {
        id: ma1
        propagateComposedEvents: true
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {

            root.hide()
        }
        visible: root.visible
        Component.onCompleted: {
            parent = backgroundItem
            anchors.fill = backgroundItem
            root.z = z + 1
        }
    }

    id: root

    property var model:[]

    visible: false
    width: childrenRect.width + 2*constants.uiBorders
    height: childrenRect.height + 2*constants.uiBorders
    border.color: constants.earnTabPanelBorder
    color: constants.backgroundColor
    //        color: "red"focus: true
    Keys.onEscapePressed: visible = false
    Column {
        x: constants.uiBorders
        y: constants.uiBorders
        width: childrenRect.width
        spacing: constants.uiBorders
        Repeater {

            model: root.model

            delegate: TextLabel {
                text: modelData.text
                color:constants.textColor
                font.bold: ma.containsMouse
                MouseArea {
                    id: ma
                    anchors.fill: parent
                    cursorShape: "PointingHandCursor"
                    hoverEnabled: true
                    onClicked: {
                        root.model[index]['action']()
                        root.hide()

                    }
                }

            }
        }
    }
    function show(item, x, y) {
        var point  = backgroundItem.mapFromItem(item, x, y)
        console.log("show", x, y, point)
//        point = Qt.point(200, 200)
        if(point.x + width < parent.width)
            root.x = point.x
        else
            root.x = point.x - width

        if(point.y + height < parent.height)
            root.y = point.y
        else
            root.y = point.y - height

        root.visible = true
        var z_ = -1;
        for(var i = 0; i < parent.children.length; i ++) {
            if( parent.children[i].z > z_)
                z_ = parent.children[i].z;
        }
        z = z_ + 1
        ma1.z = z - 0.5
//        z = 1000
    }
    function hide() {
        visible = false
        console.log("hide")
    }


    Component.onCompleted: parent =  backgroundItem
}
