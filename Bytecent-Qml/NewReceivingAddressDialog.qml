import QtQuick 2.5

DialogBase {
    caption: qsTr("New Receive Address")
    Row {
        height: 34
        spacing: constants.uiBorders
        TextLabel {
            width: 107
            horizontalAlignment: Qt.AlignRight
            anchors.verticalCenter: parent.verticalCenter
            color:constants.textColor
            text: qsTr("Label: ")
            font.pixelSize: 12
        }
        TextEdit1 {
            width: 599
            height: parent.height
            id: labelEdit
        }
    }
    onAccepted: {
        console.log("Accepted")
        visible = false;
        var res = kernelAdapter.createNewReceivingAddress(labelEdit.text)
        visible = true;
        if(res != "") {
            visible = false;
           labelEdit.text = ""
        }
        else {
//            labelEdit.text = res
        }
    }
    onRejected: {
        visible = false;
        labelEdit.text = ""
    }
    onVisibleChanged: if(visible)labelEdit.inputFocus = true

}
