import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import FilterProxyModel 1.0
import QtQuick.Dialogs 1.2

InterfaceTab {
    tabID: "contacts"
    onVisibleChanged: overviewAddresses.focus = true

    Column {
        anchors.fill: parent
        anchors.margins: 11
        spacing: 11

        Rectangle {
            id: headerBlock
            border.color: constants.earnTabPanelBorder
            height: 60
            width: 768
            Rectangle {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                width:3
                anchors.margins: 1
                color: "#5479aa"
            }

            Row {
                anchors.fill: parent
                anchors.leftMargin: 14
                spacing: constants.uiBorders
                Image {
                    source: "res/EarnTabImage1.png"
                    anchors.verticalCenter: parent.verticalCenter
                }
                TextLabel {
                    font.pixelSize: 12
                    text: qsTr("Always check the amount and receiving address before sending coins.")
                    onLinkActivated: Qt.openUrlExternally(link)
                    color:constants.textColor
                    linkColor: "#c66868"
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
        }
        Rectangle {
            id: tableArea
            width: parent.width-1
            height: parent.height - headerBlock.height - constants.uiBorders-1
            //            color: "#69a2c0"
            border.color: constants.earnTabPanelBorder


            ScrollView {
                verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn
                anchors.bottom: footerItem.top
                clip: true
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: headerItem.bottom
                anchors.topMargin: -1
                ListView {
                    id: overviewAddresses

                    //                footer: Item{height: 1; width: 5}

                    spacing: -1
                    delegate: Rectangle {
                        property var lab: label
                        property var addr: address
                        function startEdit() {
                            labEdit.start()
                        }

                        border.color: constants.earnTabPanelBorder
                        color: "transparent"
                        height: 42
                        width: parent.width
                        MouseArea {
                            anchors.fill: parent
                            acceptedButtons: Qt.LeftButton|Qt.RightButton
                            onClicked: {
                                overviewAddresses.focus = true

                                overviewAddresses.currentIndex = index;
                                if(mouse.button == Qt.RightButton)
                                    menu.show(parent, mouse.x, mouse.y)
                                mouse.accepted = false

                            }
                            onDoubleClicked: mouse.accepted = false
                            propagateComposedEvents: false

                        }
                        Row {
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            anchors.leftMargin: constants.uiBorders + 1
                            spacing: constants.uiBorders + 1
                            Image {
                                id: img
                                source: "res/B-red.png"
                                width: 22
                                opacity: 1
                                height: 22

                            }
                            EditableText {
                                id: labEdit
                                currentText: label
                                anchors.verticalCenter: parent.verticalCenter
                                width:150

                                onEdited: aModel.setDataByRole(index, "label", text)//console.log("EditableText EDITED", text)
                            }
                        }
                        TextLabel {
                            x: 210
                            text: address
                            anchors.verticalCenter: parent.verticalCenter
                            color:constants.textColor
                            font.pixelSize: 12

                        }


                        Rectangle {
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: constants.uiBorders + 3 + 10
                            width: 49
                            height: 27
                            radius: 2
                            color: "#567ba8"
                            TextLabel {
                                anchors.centerIn: parent
                                color: "white"
                                text: qsTr("Copy")
                                font.pixelSize: 12
                            }

                            MouseArea {
                                anchors.fill: parent
                                cursorShape: Qt.PointingHandCursor
                                onReleased: {
                                    clipboard.setText(address)
                                    parent.color = "#567ba8"
                                }
                                onPressed: parent.color = Qt.lighter(parent.color, 2.0)
                            }

                        }

                    }
                    model: FilterProxyModel {
                        id: aModel
                        source: addressesModel
                        filters: [{roles: ["type"], regexp: /S/}]
                        sort: "label"
                        order: FilterProxyModel.Asc
                    }

                    boundsBehavior: Flickable.StopAtBounds
                    bottomMargin: -1
                    highlight: Rectangle {

                        color: constants.earnTabPanelBorder
                    }
                    highlightFollowsCurrentItem: true

                    onCurrentItemChanged: {
                        currentAddress = currentItem.addr
                    }
                }
            }

            PopupMenu {
                id: menu
                model: [
                    {'text': qsTr("Copy Address"), 'action': (function(){
                        clipboard.setText(overviewAddresses.currentItem.addr);
                    })},
                    {'text': qsTr("Copy Label"), 'action': (function(){
                        clipboard.setText(overviewAddresses.currentItem.lab);
                    })},
                    {'text': qsTr("Edit"), 'action': (function(){
                        overviewAddresses.currentItem.startEdit();
                    })},
                    {'text': qsTr("Delete"), 'action': (function(){
                        aModel.removeRow(overviewAddresses.currentIndex);
                    })},
                    {'text': qsTr("Send Coins"), 'action': (function(){
                        sendCoinsRequest([{address: overviewAddresses.currentItem.addr, label: overviewAddresses.currentItem.lab, amount: 0.0, unit: 0}])
                    })},
                    {'text': qsTr("Verify Message"), 'action': (function(){
                        signVerifyMessageDialog.mode = "verify"
                        signVerifyMessageDialog.visible = true
                        signVerifyMessageDialog.address = currentAddress
                    })}
                ]
            }


            Rectangle {
                color: parent.color
                id: headerItem
                height: 44
                width: parent.width
                anchors.left: parent.left
                anchors.right: parent.right
                border.color: constants.earnTabPanelBorder
                anchors.top: parent.top
                TextLabel {
                    anchors.left: parent.left
                    anchors.leftMargin: 30
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 16
                    font.bold: true
                    color:constants.textColor
                    text: qsTr("Label")
                }
                TextLabel {
                    anchors.left: parent.left
                    id: t1
                    anchors.leftMargin: 295
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 16
                    font.bold: true
                    color:constants.textColor
                    text: qsTr("Address")
                }
            }
            Rectangle {
                id: footerItem
                color: parent.color
                height: 60
                width: parent.width
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                border.color: constants.earnTabPanelBorder
                RowLayout {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: constants.uiBorders + 1
                    anchors.right: parent.right
                    anchors.rightMargin: constants.uiBorders + 3
                    spacing: 3

                    ButtonType1 {
                        radius: 2
                        width: 120
                        height: 28
                        baseColor: "#bd5151"
                        text: qsTr("New Address")
                        onClicked: newSendingAddressDialog.visible = true
                    }

                    ButtonType1 {
                        radius: 2
                        width: 120
                        height: 28
                        baseColor: "#e78200"
                        text: qsTr("Verify Message")
                        onClicked: {
                            signVerifyMessageDialog.mode = "verify"
                            signVerifyMessageDialog.visible = true
                            signVerifyMessageDialog.address = currentAddress
                        }
                    }
                    ButtonType1 {
                        radius: 2
                        width: 120
                        height: 28
                        baseColor: "#567ba8"
                        text: qsTr("Delete")
                        onClicked: {
                            aModel.removeRow(overviewAddresses.currentIndex)
                        }
                    }
                    Item{Layout.fillWidth: true}
                    ButtonType1 {
                        radius: 2
                        width: 120
                        height: 28
                        baseColor: "#53a6a6"
                        text: qsTr("Export")
                        onClicked: exportDialog.open()

                        FileDialog {
                            id: exportDialog
                            title: qsTr("Please choose a file")
                            folder: shortcuts.home
                            selectMultiple: false
                            nameFilters: [ "CSV files (*.csv)", "All files (*)" ]
                            selectExisting: false
                            onAccepted: {
                                if(! aModel.writeCSV(exportDialog.fileUrl, [['label', 'Label'],['address', 'Address']]))
                                    console.log("Cannot export addresses to ",  exportDialog.fileUrl)
                            }
                        }
                    }

                }
            }
        }
    }
    property string currentAddress: ""
    signal sendCoinsRequest(var recepientsData)
    FontMetrics {
        id: fontMetrics1
        font.pixelSize: 11
        font.family:  mainFont.name
    }
}
