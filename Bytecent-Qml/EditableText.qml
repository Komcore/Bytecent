import QtQuick 2.5

Item {
    property string currentText
    property bool inEditing: false
    property bool editable: true
    signal edited(string text)
    height: inp.height
    TextInput {
        id: inp

        color:constants.textColor
        font.pixelSize: 12

        anchors.verticalCenter: parent.verticalCenter
        width: parent.width
        readOnly: true
        text: currentText
        font.family: "Arial"
        clip: true
        selectByMouse: true
            enabled: false

        Keys.onEscapePressed: {
            finish()
        }


        onFocusChanged: if(!focus && inEditing) finish()
        onEditingFinished: {
            if(text != currentText)
                edited(text)
            finish()
        }

    }
    MouseArea {
        id: ma
        anchors.fill: parent
        onDoubleClicked: {
            if(editable) {
            start()
            }
            mouse.accepted = false
        }
        onClicked: mouse.accepted = false
        propagateComposedEvents: true

    }
    function finish() {
        inEditing = false
        inp.text = Qt.binding(function(){return currentText})
        inp.readOnly = true
        ma.visible = true
        inp.deselect()
        inp.enabled = false
    }
    function start() {
        inp.enabled = true
        inEditing = true
        inp.readOnly = false
        ma.visible = false
        inp.focus = true
        inp.forceActiveFocus()
        inp.cursorVisible = true
        inp.cursorPosition = inp.text.length
        inp.selectAll()
    }
}

