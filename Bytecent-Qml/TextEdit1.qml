import QtQuick 2.4

Rectangle {
    height: 34
    id: root
    border.color: constants.earnTabPanelBorder
    color: constants.backgroundColor
    clip: true
    property alias text: inp.text
    property string placeholder
    property int placeholderSize: 12
    property alias readOnly: inp.readOnly
    property alias validator: inp.validator
    property alias  inputFocus: inp.focus
    property alias maxLength: inp.maximumLength
    property alias echoMode: inp.echoMode
    property int textRightMargin: constants.uiBorders/2
    property int textLeftMargin: constants.uiBorders/2
    property alias textColor: inp.color
    property alias inputMask: inp.inputMask
    property alias acceptableInput: inp.acceptableInput
    signal returnPressed()
    onInputFocusChanged: if(inputFocus){  inp.forceActiveFocus(); inp.cursorVisible = true;}
    signal editingFinished()
    TextInput {
        clip: true
        id: inp
        font.family: mainFont.name
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width - constants.uiBorders
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.rightMargin: textRightMargin
        anchors.leftMargin: textLeftMargin
        font.pixelSize: 12
        color: constants.textColor
//        focus: true
        text: ""
        activeFocusOnTab: true
        selectByMouse: true
        Keys.onReturnPressed: root.returnPressed()
        onEditingFinished: root.editingFinished();
    }
    TextLabel {
         anchors.verticalCenter: parent.verticalCenter
         anchors.left: parent.left
         anchors.leftMargin: constants.uiBorders/2
         font.pixelSize: placeholderSize
         visible: inp.text == ""
         color: constants.textColor

         text:  placeholder
    }
    MouseArea {
        anchors.fill: parent
        acceptedButtons:  Qt.RightButton
        onClicked: {
            menu.show(root, mouse.x, mouse.y)
        }
    }

    PopupMenu {
        parent: root.parent
        id: menu
        model: [
            {'text': qsTr("Copy"), 'action': (function(){
                clipboard.setText(text);
            })},
            {'text': qsTr("Paste"), 'action': (function(){
                text = clipboard.text();
            })}
        ]
    }
}
