import QtQuick 2.5

Rectangle {
    id: root
    property bool controlLeftRight: true
    property string text:""
    property point pos: Qt.point(0,0)
    property int bottomMargin: 0
    property double relWidth: 0.6
    visible: false
    clip: true
    width: txt.contentWidth + 2*constants.uiBorders
    height: txt.contentHeight < parent.height * 0.4 ? txt.contentHeight + 2*constants.uiBorders: parent.height * 0.4
    border.color: constants.earnTabPanelBorder
    color: constants.backgroundColor
    //        color: "red"focus: true
    Keys.onEscapePressed: visible = false
    TextLabel {
        id: txt
        width: dummy_text.width > root.parent.width * relWidth ?  root.parent.width * relWidth : dummy_text.width

        wrapMode: Text.Wrap


        x: constants.uiBorders
        y: constants.uiBorders
        text: root.text.trim()
        color:constants.textColor
        Text {
               id: dummy_text
               text: parent.text
               visible: false
           }


    }

    x:  if(!controlLeftRight || pos.x + width < parent.width) pos.x; else pos.x - width;
    y:  if(pos.y + height < parent.height - bottomMargin) pos.y; else pos.y - height;
    function setPos(item, mousePos) {
        pos = parent.mapFromItem(item, mousePos.x, mousePos.y)
    }
}
