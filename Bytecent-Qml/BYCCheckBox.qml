import QtQuick 2.0

Item {
    height: 22
    width: childrenRect.width
    id: root
    property bool checked
    property string text: ""
    signal clicked()
    onClicked: {
        checked = !checked
    }

    Rectangle {
        id: i1
        width: 22
        height: 22
        border.color: constants.earnTabPanelBorder
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        Rectangle {
            anchors.centerIn: parent
            visible: checked
            width: 14
            height: 14
            color: constants.earnTabPanelBorder
        }
        MouseArea {
            anchors.fill: parent
            cursorShape: "PointingHandCursor"
            onClicked: root.clicked()
        }
    }
    TextLabel {
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: i1.right
        anchors.leftMargin: constants.uiBorders
        text: root.text
    }
}
