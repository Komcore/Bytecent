#ifndef CLIPBOARD_H
#define CLIPBOARD_H

#include <QClipboard>
class Clipboard : public QObject
{
    Q_OBJECT
public:
    explicit Clipboard(QObject * parent = 0);
    virtual ~Clipboard() {}
public slots:
    void setText(QString text);
    QString text();
};

#endif // CLIPBOARD_H
