#ifndef QMLUI_H
#define QMLUI_H
#include <QQmlApplicationEngine>
//#include <AdsWebView.h>
#include <QQuickPaintedItem>

#include <Clipboard.h>
#include <SystemResourceReporter.h>
#include "MiningController.h"
#include <QQmlContext>
#include "../src/qt/macdockiconhandler.h"
#include "FilterProxyModel.h"
#include <QSystemTrayIcon>

class Notificator;
class QSystemTrayIcon;
class ClientModel;
class WalletModel;
class QmlUI: public QObject
{
    Q_OBJECT
    Q_PROPERTY(int maxPasswordLength READ maxPasswordLength NOTIFY maxPasswordLengthChanged)
public:


    QmlUI() {
        trayIcon = 0;
        m_maxPasswordLength = 1024;
//        qmlRegisterType<AdsWebView>("AdsWebView", 1,0, "AdsWebView");
        qmlRegisterType<FilterProxyModel>("FilterProxyModel", 1,0, "FilterProxyModel");
        engine.rootContext()->setContextProperty("clipboard", &clipboard);
        engine.rootContext()->setContextProperty("systemResourceReporter", &systemResourceReporter);
        engine.rootContext()->setContextProperty("kernelAdapter", this);
    }
    ~QmlUI();
    void initBYCInterface();
    void destroyBYCInterface();

    void setBYCClientModel(ClientModel* model);
    bool addBYCWallet(const QString& name, WalletModel *walletModel);
    bool setCurrentBYCWallet(const QString& name);

    Q_INVOKABLE  QString formatBYCAmount(qint64 amount, bool withSign);
    Q_INVOKABLE double getDifficulty();

    Q_INVOKABLE QDateTime getLastBlockDate();
    Q_INVOKABLE int getAddressModelItemIndex(QString address);
    Q_INVOKABLE QString sslVersion();
    int maxPasswordLength() const
    {
        return m_maxPasswordLength;
    }

    enum MessageClass {
        MC_ERROR,
        MC_DEBUG,
        CMD_REQUEST,
        CMD_REPLY,
        CMD_ERROR
    };


public slots:
    void show();
    void detectShutdown();

    ////USE MODELS DIRECTLY INSTEAD OF THIS↓
//    void setNumConnections(int count);
//    void setNumBlocks(int count, int nTotalBlocks);
//    void setEncryptionStatus(int status)
    ////USE MODELS DIRECTLY INSTEAD OF this↑

    void message(const QString &title, const QString &message, unsigned int style, bool *ret = NULL);

    void askFee(qint64 nFeeRequired, bool *payFee);

    //
    void handleURI(QString strURI);


    //TODO: HANDLE walletModel encryptionStatusChanged
    //TODO: client model all props
    //TODO: wallet model all props
    //TODO: bitcoingui.cpp: 601 in qml
    //TODO: bitcoingui.cpp: 616 in qml
    //TODO: bitcoingui.cpp: 780 in qml
    //TODO: 820, 827 in QML
    //TODO: 870 in qml
    void onIncomingTransaction(const QModelIndex &parent, int start, int);

    QString createNewReceivingAddress(QString label);
    QString createSendingAddress(QString address, QString label);
    QString signMessage(QString address, QString message);
    QString verifyMessage(QString signature, QString message, QString address);
    bool sendCoins(QVariantList recipientsJSON);
    bool backupWallet(QUrl path);

    bool setWalletEncrypted(bool encrypted, const QString &passphrase);
    // Passphrase only needed when unlocking
    bool unlockWallet(const QString &passPhrase=QString());
    bool changePassphrase(const QString &oldPass, const QString &newPass);

    void openDebugLogfile();
    void openOptionsHelpBox();


    void execCMD(QString cmd);


    QString getSendConfirmationText(QVariantList recipientsJSON);

    void setFeeAgreed();
    void finishCallback(){emit callBackFinished();}

    void updateTrayHideAction(bool v);
signals:
    void displayUnitUpdated(QString);
    void incomingTransaction(QString date, qint64 amount, QString type, QString address);
    void backendReady();
    void maxPasswordLengthChanged(int maxPasswordLength);
    void cmdRequest(const QString &command);
    void stopExecutor();

    void cmdResponse(QString text);
    void feeAsked(QString text);
    void callBackFinished();

    void trayIconTriggered();
    void toggleHide(bool v);
    void needToQuit();
private:

    QSystemTrayIcon *trayIcon;
    Notificator *notificator;
    ClientModel * clientModel;
    WalletModel * walletModel;

    Clipboard clipboard;
    MiningController * miningController;
    SystemResourceReporter systemResourceReporter;
    QQmlApplicationEngine engine;
    void createTrayIconMenu();
    int m_maxPasswordLength;

    QString categoryClass(int category);

    bool m_feeAgreed;

    QAction * m_toggleVisibleAction;
private slots:
    void RPCmessage(int category, const QString &message, bool html = false);
    void trayIconActivated(QSystemTrayIcon::ActivationReason reason);
    void dockIconActivated();
};


class RPCExecutorAdapter : public QObject
{
    Q_OBJECT
public:
    bool parseCommandLine(std::vector<std::string> &args, const std::string &strCommand);
public slots:
    void request(const QString &command);

signals:
    void reply(int category, const QString &command);
};


#endif // QMLUI_H
