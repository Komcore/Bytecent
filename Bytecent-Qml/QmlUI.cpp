#include "QmlUI.h"
#include "../src/ui_interface.h"
#include "../src/init.h"
#include "../src/qt/clientmodel.h"
#include "../src/qt/walletmodel.h"
#include "../src/qt/notificator.h"
#include "../src/qt/transactionfilterproxy.h"
#include "../src/qt/trans_addressfilterproxy.h"
#include "../src/qt/transactiontablemodel.h"
#include "../src/qt/bitcoinunits.h"
#include "../src/qt/optionsmodel.h"
#include "../src/bitcoinrpc.h"
#include "../src/qt/addresstablemodel.h"
#include "QSystemTrayIcon"
#include <QApplication>
#include <QMessageBox>
#include <QMenu>
#include "guiutil.h"
#include <QThread>

QmlUI::~QmlUI()
{
    emit stopExecutor();
}

void QmlUI::initBYCInterface()
{
#ifndef Q_OS_MAC
    trayIcon = new QSystemTrayIcon(this);

    trayIcon->setToolTip(tr("Bytecent client"));
    trayIcon->setIcon(QIcon(":/icons/toolbar"));
    trayIcon->show();
#endif

    notificator = new Notificator(QApplication::applicationName(), trayIcon);
}

void QmlUI::destroyBYCInterface()
{
    if(trayIcon) // Hide tray icon, as deleting will let it linger until quit (on Ubuntu)
        trayIcon->hide();
#ifdef Q_OS_MAC
    MacDockIconHandler::instance()->setMainWindow(NULL);
#endif
}

void QmlUI::setBYCClientModel(ClientModel *model)
{
    qmlRegisterType<ClientModel>("ClientModel", 1,0, "ClientModel");
    engine.rootContext()->setContextProperty("bycClientModel", model);

    qmlRegisterType<OptionsModel>("OptionsModel", 1,0, "OptionsModel");
    engine.rootContext()->setContextProperty("optionsModel", model->getOptionsModel());

    qmlRegisterType<BitcoinUnits>("BitcoinUnits", 1,0, "BitcoinUnits");
    if(clientModel)
    {
        // Replace some strings and icons, when using the testnet
        if(clientModel->isTestNet())
        {
#ifndef Q_OS_MAC
            QApplication::setWindowIcon(QIcon(":icons/bytecent_testnet"));
            //setWindowIcon(QIcon(":icons/bytecent_testnet"));
#else
            MacDockIconHandler::instance()->setIcon(QIcon(":icons/bytecent_testnet"));
#endif

            if(trayIcon)
            {
                // Just attach " [testnet]" to the existing tooltip
                trayIcon->setToolTip(trayIcon->toolTip() + QString(" ") + tr("[testnet]"));
                trayIcon->setIcon(QIcon(":/icons/toolbar"));
            }
        }
    }
    createTrayIconMenu();
}
#define NUM_ITEMS 4

bool QmlUI::addBYCWallet(const QString &name, WalletModel *walletModel)
{
    if(walletModel && walletModel->getOptionsModel())
    {
        qmlRegisterType<WalletModel>("WalletModel", 1,0, "WalletModel");

        engine.rootContext()->setContextProperty("walletModel", walletModel);
        engine.rootContext()->setContextProperty("optionsModel", walletModel->getOptionsModel());

        engine.rootContext()->setContextProperty("transactionsModel", walletModel->getTransactionTableModel());

        engine.rootContext()->setContextProperty("addressesModel", walletModel->getAddressTableModel());
        connect(walletModel->getAddressTableModel(), SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),
                walletModel->getTransactionTableModel(), SLOT(resetAll()));

        connect(walletModel->getAddressTableModel(), SIGNAL(rowsInserted(QModelIndex,int,int)),
                walletModel->getTransactionTableModel(), SLOT(resetAll()));
        connect(walletModel->getAddressTableModel(), SIGNAL(rowsRemoved(QModelIndex,int,int)),
                walletModel->getTransactionTableModel(), SLOT(resetAll()));

        connect(walletModel->getTransactionTableModel(), SIGNAL(rowsInserted(QModelIndex,int,int)),
                this, SLOT(onIncomingTransaction(QModelIndex,int,int)));


        this->walletModel = walletModel;



        // Keep up to date with wallet
        //        setBalance(model->getBalance(), model->getUnconfirmedBalance(), model->getImmatureBalance());
        //        connect(model, SIGNAL(balanceChanged(qint64, qint64, qint64)), this, SLOT(setBalance(qint64, qint64, qint64)));

        //        connect(model->getTransactionTableModel(), SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)), this, SLOT(updateTransactionsTotalCount()));

        engine.rootContext()->setContextProperty("bycWalletName", name);





        QThread *thread = new QThread;
        RPCExecutorAdapter *executor = new RPCExecutorAdapter();
        executor->moveToThread(thread);

        // Replies from executor object must go to this object
        connect(executor, SIGNAL(reply(int,QString)), this, SLOT(RPCmessage(int,QString)));
        // Requests from this object must go to executor
        connect(this, SIGNAL(cmdRequest(QString)), executor, SLOT(request(QString)));

        // On stopExecutor signal
        // - queue executor for deletion (in execution thread)
        // - quit the Qt event loop in the execution thread
        connect(this, SIGNAL(stopExecutor()), executor, SLOT(deleteLater()));
        connect(this, SIGNAL(stopExecutor()), thread, SLOT(quit()));
        // Queue the thread for deletion (in this thread) when it is finished
        connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

        // Default implementation of QThread::run() simply spins up an event loop in the thread,
        // which is what we want.
        thread->start();




        return true;
    }
    return false;
}

bool QmlUI::setCurrentBYCWallet(const QString &name)
{
    ;
    return false;
}



void QmlUI::createTrayIconMenu()
{
    QMenu *trayIconMenu;
#ifndef Q_OS_MAC
    // return if trayIcon is unset (only on non-Mac OSes)
    if (!trayIcon)
        return;
    qDebug() << "Creating tray icon";
    trayIconMenu = new QMenu(0);
    trayIcon->setContextMenu(trayIconMenu);

    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(trayIconActivated(QSystemTrayIcon::ActivationReason)));
#else
    // Note: On Mac, the dock icon is used to provide the tray's functionality.
    MacDockIconHandler *dockIconHandler = MacDockIconHandler::instance();
    connect(dockIconHandler, SIGNAL(dockIconClicked()), SLOT(dockIconActivated()));
    trayIconMenu = dockIconHandler->dockMenu();
#endif

    // Configuration of the tray icon (or dock icon) icon menu
    m_toggleVisibleAction = trayIconMenu->addAction("Visible");
    m_toggleVisibleAction->setCheckable(true);
    m_toggleVisibleAction->setChecked(true);
    connect(m_toggleVisibleAction, SIGNAL(triggered(bool)), this, SIGNAL(toggleHide(bool)));
    trayIconMenu->addSeparator();
    //    trayIconMenu->addAction(sendCoinsAction);
    //    trayIconMenu->addAction(receiveCoinsAction);
    //        trayIconMenu->addSeparator();
    //    trayIconMenu->addAction(signMessageAction);
    //    trayIconMenu->addAction(verifyMessageAction);
    //        trayIconMenu->addSeparator();
    //    trayIconMenu->addAction(optionsAction);
    //    trayIconMenu->addAction(openRPCConsoleAction);
#ifndef Q_OS_MAC // This is built-in on Mac
    //    trayIconMenu->addSeparator();
    //trayIconMenu->addAction(quitAction);
    QAction * a = trayIconMenu->addAction("Quit");
    connect(a, SIGNAL(triggered(bool)), this, SIGNAL(needToQuit()));
#endif
}

QString QmlUI::categoryClass(int category)
{
    switch(category)
    {
    case CMD_REQUEST:  return "cmd-request"; break;
    case CMD_REPLY:    return "cmd-reply"; break;
    case CMD_ERROR:    return "cmd-error"; break;
    default:                       return "misc";
    }
}

void QmlUI::RPCmessage(int category, const QString &message, bool html)
{
    QTime time = QTime::currentTime();
    QString timeString = time.toString();
    QString out;
    out += "<table><tr><td class=\"time\" width=\"65\">" + timeString + "</td>";
    //    out += "<td class=\"icon\" width=\"32\"><img src=\"" + categoryClass(category) + "\"></td>";
    out += "<td class=\"message " + categoryClass(category) + "\" valign=\"middle\">";
    if(html)
        out += message;
    else
        out += GUIUtil::HtmlEscape(message, true);
    out += "</td></tr></table>";
    emit cmdResponse(out);
}

void QmlUI::trayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    if(reason == QSystemTrayIcon::Trigger)
    {
        emit trayIconTriggered();
    }
}

void QmlUI::dockIconActivated()
{
    emit trayIconTriggered();
}

void QmlUI::message(const QString &title, const QString &message, unsigned int style, bool *ret)
{
    QString strTitle = tr("Bytecent"); // default title
    // Default to information icon
    int nMBoxIcon = QMessageBox::Information;
    int nNotifyIcon = Notificator::Information;

    // Override title based on style
    QString msgType;
    switch (style) {
    case CClientUIInterface::MSG_ERROR:
        msgType = tr("Error");
        break;
    case CClientUIInterface::MSG_WARNING:
        msgType = tr("Warning");
        break;
    case CClientUIInterface::MSG_INFORMATION:
        msgType = tr("Information");
        break;
    default:
        msgType = title; // Use supplied title
    }
    if (!msgType.isEmpty())
        strTitle += " - " + msgType;

    // Check for error/warning icon
    if (style & CClientUIInterface::ICON_ERROR) {
        nMBoxIcon = QMessageBox::Critical;
        nNotifyIcon = Notificator::Critical;
    }
    else if (style & CClientUIInterface::ICON_WARNING) {
        nMBoxIcon = QMessageBox::Warning;
        nNotifyIcon = Notificator::Warning;
    }

    // Display message
    if (style & CClientUIInterface::MODAL) {
        // Check for buttons, use OK as default, if none was supplied
        QMessageBox::StandardButton buttons;
        if (!(buttons = (QMessageBox::StandardButton)(style & CClientUIInterface::BTN_MASK)))
            buttons = QMessageBox::Ok;

        QMessageBox mBox((QMessageBox::Icon)nMBoxIcon, strTitle, message, buttons);
        int r = mBox.exec();
        if (ret != NULL)
            *ret = r == QMessageBox::Ok;
    }
    else
    {
        //        notificator->notify((Notificator::Class)nNotifyIcon, strTitle, message);
    }
}

void QmlUI::askFee(qint64 nFeeRequired, bool *payFee)
{
    QString strMessage = tr("This transaction is over the size limit. You can still send it for a fee of %1, "
                            "which goes to the nodes that process your transaction and helps to support the network. "
                            "Do you want to pay the fee?").arg(BitcoinUnits::formatWithUnit(BitcoinUnits::BTC, nFeeRequired));

    m_feeAgreed = false;
    emit feeAsked(strMessage);
    QEventLoop l;
    connect(this, SIGNAL(callBackFinished()), &l, SLOT(quit()));
    l.exec();

    *payFee = m_feeAgreed;
}

void QmlUI::handleURI(QString strURI)
{
    //TODO: D&D in Qml!
}

//void QmlUI::incomingTransaction(const QString &date, int unit, qint64 amount, const QString &type, const QString &address)
//{
//    //TODO: in qml
//    message((amount)<0 ? tr("Sent transaction") : tr("Incoming transaction"),
//             tr("Date: %1\n"
//                "Amount: %2\n"
//                "Type: %3\n"
//                "Address: %4\n")
//                  .arg(date)
//                  .arg(BitcoinUnits::formatWithUnit(unit, amount, true))
//                  .arg(type)
//            .arg(address), CClientUIInterface::MSG_INFORMATION);
//}

QString QmlUI::formatBYCAmount(qint64 amount, bool withSign)
{
    return BitcoinUnits::formatWithUnit(walletModel->getOptionsModel()->getDisplayUnit(), amount, withSign);
}

double QmlUI::getDifficulty()
{
    return GetDifficulty(pindexBest);
}


QDateTime QmlUI::getLastBlockDate()
{
    return clientModel->getLastBlockDate();
}

int QmlUI::getAddressModelItemIndex(QString address)
{
    int idx = walletModel->getAddressTableModel()->lookupAddress(address);
    return idx;
}

QString QmlUI::sslVersion()
{
    return SSLeay_version(SSLEAY_VERSION);
}

void QmlUI::show()
{
    miningController = new MiningController(clientModel, walletModel);
    engine.rootContext()->setContextProperty("miningController", miningController);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    

    RPCmessage(CMD_REPLY, (tr("Welcome to the Bytecent RPC console.") + "<br>" +
                           tr("Use up and down arrows to navigate history, and <b>Ctrl-L</b> to clear screen.") + "<br>" +
                           tr("Type <b>help</b> for an overview of available commands.")), true);



    emit backendReady();
}

void QmlUI::detectShutdown()
{
    if (ShutdownRequested())
        QMetaObject::invokeMethod(QCoreApplication::instance(), "quit", Qt::QueuedConnection);
}

void QmlUI::onIncomingTransaction(const QModelIndex& parent, int start, int /*end*/)
{
    // Prevent balloon-spam when initial block download is in progress
    if(!walletModel || !clientModel || clientModel->inInitialBlockDownload())
        return;

    TransactionTableModel *ttm = walletModel->getTransactionTableModel();

    QString date = ttm->index(start, TransactionTableModel::Date, parent).data().toString();
    qint64 amount = ttm->index(start, TransactionTableModel::Amount, parent).data(Qt::EditRole).toULongLong();
    QString type = ttm->index(start, TransactionTableModel::Type, parent).data().toString();
    QString address = ttm->index(start, TransactionTableModel::ToAddress, parent).data().toString();


    emit incomingTransaction(date, amount, type, address);
}

QString QmlUI::createNewReceivingAddress(QString label)
{
    QString address = walletModel->getAddressTableModel()->addRow(
                AddressTableModel::Receive,
                label,
                "");


    if(address.isEmpty())
    {
        switch(walletModel->getAddressTableModel()->getEditStatus())
        {
        case AddressTableModel::OK:
            // Failed with unknown reason. Just reject.
            break;
        case AddressTableModel::NO_CHANGES:
            // No changes were made during edit operation. Just reject.
            break;
        case AddressTableModel::INVALID_ADDRESS:
            QMessageBox::warning(0, "New Receiving Address",
                                 tr("The entered address \"%1\" is not a valid Bytecent address."),//.arg(ui->addressEdit->text()),
                                 QMessageBox::Ok, QMessageBox::Ok);
            break;
        case AddressTableModel::DUPLICATE_ADDRESS:
            QMessageBox::warning(0, "New Receiving Address",
                                 tr("The entered address \"%1\" is already in the address book."),//.arg(ui->addressEdit->text()),
                                 QMessageBox::Ok, QMessageBox::Ok);
            break;
        case AddressTableModel::WALLET_UNLOCK_FAILURE:
            QMessageBox::critical(0, "New Receiving Address",
                                  tr("Could not unlock wallet."),
                                  QMessageBox::Ok, QMessageBox::Ok);
            break;
        case AddressTableModel::KEY_GENERATION_FAILURE:
            QMessageBox::critical(0, "New Receiving Address",
                                  tr("New key generation failed."),
                                  QMessageBox::Ok, QMessageBox::Ok);
            break;

        }
    }
    return address;
}

QString QmlUI::createSendingAddress(QString address, QString label)
{
    QString res = walletModel->getAddressTableModel()->addRow(
                AddressTableModel::Send,
                label,
                address);


    if(res.isEmpty())
    {
        switch(walletModel->getAddressTableModel()->getEditStatus())
        {
        case AddressTableModel::OK:
            // Failed with unknown reason. Just reject.
            break;
        case AddressTableModel::NO_CHANGES:
            // No changes were made during edit operation. Just reject.
            break;
        case AddressTableModel::INVALID_ADDRESS:
            QMessageBox::warning(0, "New Sending Address",
                                 tr("The entered address \"%1\" is not a valid Bytecent address.").arg(address),
                                 QMessageBox::Ok, QMessageBox::Ok);
            break;
        case AddressTableModel::DUPLICATE_ADDRESS:
            QMessageBox::warning(0, "New Sending Address",
                                 tr("The entered address \"%1\" is already in the address book.").arg(address),
                                 QMessageBox::Ok, QMessageBox::Ok);
            break;
        case AddressTableModel::WALLET_UNLOCK_FAILURE:
            QMessageBox::critical(0, "New Sending Address",
                                  tr("Could not unlock wallet."),
                                  QMessageBox::Ok, QMessageBox::Ok);
            break;
        case AddressTableModel::KEY_GENERATION_FAILURE:
            QMessageBox::critical(0, "New Sending Address",
                                  tr("New key generation failed."),
                                  QMessageBox::Ok, QMessageBox::Ok);
            break;

        }
    }
    return res;
}

QString QmlUI::signMessage(QString address, QString message)
{
    /* Clear old signature to ensure users don't get confused on error with an old signature displayed */
    //    ui->signatureOut_SM->clear();

    CBitcoinAddress addr(address.toStdString());
    if (!addr.IsValid())
    {
        //        ui->addressIn_SM->setValid(false);
        //        ui->statusLabel_SM->setStyleSheet("QLabel { color: red; }");
        //        ui->statusLabel_SM->setText(tr("The entered address is invalid.") + QString(" ") + tr("Please check the address and try again."));
        return "The entered address is invalid.";
    }
    CKeyID keyID;
    if (!addr.GetKeyID(keyID))
    {
        //        ui->addressIn_SM->setValid(false);
        //        ui->statusLabel_SM->setStyleSheet("QLabel { color: red; }");
        //        ui->statusLabel_SM->setText(tr("The entered address does not refer to a key.") + QString(" ") + tr("Please check the address and try again."));
        return "The entered address does not refer to a key.";
    }

    WalletModel::UnlockContext ctx(walletModel->requestUnlock());
    if (!ctx.isValid())
    {
        //        ui->statusLabel_SM->setStyleSheet("QLabel { color: red; }");
        //        ui->statusLabel_SM->setText(tr("Wallet unlock was cancelled."));
        return "";
    }

    CKey key;
    if (!pwalletMain->GetKey(keyID, key))
    {
        //        ui->statusLabel_SM->setStyleSheet("QLabel { color: red; }");
        //        ui->statusLabel_SM->setText(tr("Private key for the entered address is not available."));
        return "Private key for the entered address is not available.";
    }

    CDataStream ss(SER_GETHASH, 0);
    ss << strMessageMagic;
    ss << message.toStdString();

    std::vector<unsigned char> vchSig;
    if (!key.SignCompact(Hash(ss.begin(), ss.end()), vchSig))
    {
        //        ui->statusLabel_SM->setStyleSheet("QLabel { color: red; }");
        //        ui->statusLabel_SM->setText(QString("<nobr>") + tr("Message signing failed.") + QString("</nobr>"));
        return "Message signing failed.";
    }

    //    ui->statusLabel_SM->setStyleSheet("QLabel { color: green; }");
    //    ui->statusLabel_SM->setText(QString("<nobr>") + tr("Message signed.") + QString("</nobr>"));

    //    ui->signatureOut_SM->setText(QString::fromStdString(EncodeBase64(&vchSig[0], vchSig.size())));
    return QString::fromStdString(EncodeBase64(&vchSig[0], vchSig.size()));
}

QString QmlUI::verifyMessage(QString signature, QString message, QString address)
{
    CBitcoinAddress addr(address.toStdString());
    if (!addr.IsValid())
    {
        //        ui->addressIn_VM->setValid(false);
        //        ui->statusLabel_VM->setStyleSheet("QLabel { color: red; }");
        //        ui->statusLabel_VM->setText(tr("The entered address is invalid.") + QString(" ") + tr("Please check the address and try again."));
        return "The entered address is invalid.";
    }
    CKeyID keyID;
    if (!addr.GetKeyID(keyID))
    {
        //        ui->addressIn_VM->setValid(false);
        //        ui->statusLabel_VM->setStyleSheet("QLabel { color: red; }");
        //        ui->statusLabel_VM->setText(tr("The entered address does not refer to a key.") + QString(" ") + tr("Please check the address and try again."));
        return "The entered address does not refer to a key.";
    }

    bool fInvalid = false;
    std::vector<unsigned char> vchSig = DecodeBase64(signature.toStdString().c_str(), &fInvalid);

    if (fInvalid)
    {
        //        ui->signatureIn_VM->setValid(false);
        //        ui->statusLabel_VM->setStyleSheet("QLabel { color: red; }");
        //        ui->statusLabel_VM->setText(tr("The signature could not be decoded.") + QString(" ") + tr("Please check the signature and try again."));
        return "The signature could not be decoded.";
    }

    CDataStream ss(SER_GETHASH, 0);
    ss << strMessageMagic;
    ss << message.toStdString();

    CKey key;
    if (!key.SetCompactSignature(Hash(ss.begin(), ss.end()), vchSig))
    {
        //        ui->signatureIn_VM->setValid(false);
        //        ui->statusLabel_VM->setStyleSheet("QLabel { color: red; }");
        //        ui->statusLabel_VM->setText(tr("The signature did not match the message digest.") + QString(" ") + tr("Please check the signature and try again."));
        return "The signature did not match the message digest.";
    }

    if (!(CBitcoinAddress(key.GetPubKey().GetID()) == addr))
    {
        //        ui->statusLabel_VM->setStyleSheet("QLabel { color: red; }");
        //        ui->statusLabel_VM->setText(QString("<nobr>") + tr("Message verification failed.") + QString("</nobr>"));
        return "Message verification failed.";
    }

    //    ui->statusLabel_VM->setStyleSheet("QLabel { color: green; }");
    //    ui->statusLabel_VM->setText(QString("<nobr>") + tr("Message verified.") + QString("</nobr>"));
    return "Message verified";
}

bool QmlUI::sendCoins(QVariantList recipientsJSON)
{
    QList<SendCoinsRecipient> recipients;

    for(int i = 0; i < recipientsJSON.count(); ++i)
    {
        QVariantMap m = recipientsJSON[i].toMap();
        QString label = m["label"].toString();
        QString address = m["address"].toString();
        //        double amount = m["amount"].toDouble();
        int unit = m["unit"].toInt();
        qint64 val_out = 0;
        bool valid = BitcoinUnits::parse(unit, m["amount"].toString(), &val_out);
        if(!valid)return false;
        if (!walletModel->validateAddress(address)) return false;

        SendCoinsRecipient rv;

        rv.address = address;
        rv.label = label;
        rv.amount = val_out;
        recipients.append(rv);

    }

    if(recipients.isEmpty())
    {
        return false;
    }
    //    QStringList formatted;
    //    foreach(const SendCoinsRecipient &rcp, recipients)
    //    {
    //        formatted.append(tr("<b>%1</b> to %2 (%3)").arg(BitcoinUnits::formatWithUnit(BitcoinUnits::BTC, rcp.amount), rcp.label.toHtmlEscaped(), rcp.address));
    //    }


    //    QMessageBox::StandardButton retval = QMessageBox::question(0, tr("Confirm send coins"),
    //                          tr("Are you sure you want to send %1?").arg(formatted.join(tr(" and "))),
    //          QMessageBox::Yes|QMessageBox::Cancel,
    //          QMessageBox::Cancel);

    //    if(retval != QMessageBox::Yes)
    //    {
    //        return false;
    //    }

    WalletModel::UnlockContext ctx(walletModel->requestUnlock());
    if(!ctx.isValid())
    {
        // Unlock wallet was cancelled
        return false;
    }

    WalletModel::SendCoinsReturn sendstatus = walletModel->sendCoins(recipients);
    switch(sendstatus.status)
    {
    case WalletModel::InvalidAddress:
        QMessageBox::warning(0, tr("Send Coins"),
                             tr("The recipient address is not valid, please recheck."),
                             QMessageBox::Ok, QMessageBox::Ok);
        break;
    case WalletModel::InvalidAmount:
        QMessageBox::warning(0, tr("Send Coins"),
                             tr("The amount to pay must be at least one cent (0.01)."),
                             QMessageBox::Ok, QMessageBox::Ok);
        break;
    case WalletModel::AmountExceedsBalance:
        QMessageBox::warning(0, tr("Send Coins"),
                             tr("The amount exceeds your balance."),
                             QMessageBox::Ok, QMessageBox::Ok);
        break;
    case WalletModel::AmountWithFeeExceedsBalance:
        QMessageBox::warning(0, tr("Send Coins"),
                             tr("The total exceeds your balance when the %1 transaction fee is included.").
                             arg(BitcoinUnits::formatWithUnit(BitcoinUnits::BTC, sendstatus.fee)),
                             QMessageBox::Ok, QMessageBox::Ok);
        break;
    case WalletModel::DuplicateAddress:
        QMessageBox::warning(0, tr("Send Coins"),
                             tr("Duplicate address found, can only send to each address once per send operation."),
                             QMessageBox::Ok, QMessageBox::Ok);
        break;
    case WalletModel::TransactionCreationFailed:
        QMessageBox::warning(0, tr("Send Coins"),
                             tr("Error: Transaction creation failed!"),
                             QMessageBox::Ok, QMessageBox::Ok);
        break;
    case WalletModel::TransactionCommitFailed:
        QMessageBox::warning(0, tr("Send Coins"),
                             tr("Error: The transaction was rejected. This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here."),
                             QMessageBox::Ok, QMessageBox::Ok);
        break;
    case WalletModel::Aborted: // User aborted, nothing to do
        break;
    case WalletModel::OK:
        return true;
    }
    return false;
}

bool QmlUI::setWalletEncrypted(bool encrypted, const QString &passphrase)
{
    SecureString oldpass;
    oldpass.reserve(maxPasswordLength());
    oldpass.assign(passphrase.toStdString().c_str());
    return walletModel->setWalletEncrypted(encrypted, oldpass);
}

bool QmlUI::unlockWallet(const QString &passPhrase)
{
    SecureString oldpass;
    oldpass.reserve(maxPasswordLength());

    oldpass.assign(passPhrase.toStdString().c_str());
    bool b = walletModel->setWalletLocked(false, oldpass);
    qDebug () << " QmlUI::unlockWallet" << b;
    return b;
}

bool QmlUI::changePassphrase(const QString &oldPass, const QString &newPass)
{
    SecureString oldpass, newpass;
    oldpass.reserve(maxPasswordLength());
    newpass.reserve(maxPasswordLength());
    oldpass.assign(oldPass.toStdString().c_str());
    newpass.assign(newPass.toStdString().c_str());

    return walletModel->changePassphrase(oldpass, newpass);
}

void QmlUI::openDebugLogfile()
{
    GUIUtil::openDebugLogfile();
}

void QmlUI::openOptionsHelpBox()
{
    GUIUtil::HelpMessageBox help;
    help.exec();
}

void QmlUI::execCMD(QString cmd)
{
    RPCmessage(CMD_REQUEST, cmd);
    emit cmdRequest(cmd);
}

QString QmlUI::getSendConfirmationText(QVariantList recipientsJSON)
{
    QList<SendCoinsRecipient> recipients;

    for(int i = 0; i < recipientsJSON.count(); ++i)
    {
        QVariantMap m = recipientsJSON[i].toMap();
        QString label = m["label"].toString();
        QString address = m["address"].toString();
        //        double amount = m["amount"].toDouble();
        int unit = m["unit"].toInt();
        qint64 val_out = 0;
        BitcoinUnits::parse(unit, m["amount"].toString(), &val_out);


        SendCoinsRecipient rv;

        rv.address = address;
        rv.label = label;
        rv.amount = val_out;
        recipients.append(rv);

    }


    QStringList formatted;
    foreach(const SendCoinsRecipient &rcp, recipients)
    {
        formatted.append(tr("<b>%1</b> to %2 (%3)").arg(BitcoinUnits::formatWithUnit(BitcoinUnits::BTC, rcp.amount), rcp.label.toHtmlEscaped(), rcp.address));
    }

    return tr("Are you sure you want to send %1?").arg(formatted.join(tr(" and ")));

}

void QmlUI::setFeeAgreed()
{
    m_feeAgreed = true;
}

void QmlUI::updateTrayHideAction(bool v)
{
    m_toggleVisibleAction->setChecked(v);
}


bool QmlUI::backupWallet(QUrl url)
{
    QString filename = url.toLocalFile();
    if (!filename.isEmpty()) {
        if (!walletModel->backupWallet(filename)) {
            message(tr("Backup Failed"), tr("There was an error trying to save the wallet data to the new location."),
                    CClientUIInterface::MSG_ERROR);
            return false;
        }
        else
        {
            message(tr("Backup Successful"), tr("The wallet data was successfully saved to the new location."),
                    CClientUIInterface::MSG_INFORMATION);
            return true;
        }

    }
    return false;
}

bool RPCExecutorAdapter::parseCommandLine(std::vector<std::string> &args, const std::string &strCommand)
{
    enum CmdParseState
    {
        STATE_EATING_SPACES,
        STATE_ARGUMENT,
        STATE_SINGLEQUOTED,
        STATE_DOUBLEQUOTED,
        STATE_ESCAPE_OUTER,
        STATE_ESCAPE_DOUBLEQUOTED
    } state = STATE_EATING_SPACES;
    std::string curarg;
    foreach(char ch, strCommand)
    {
        switch(state)
        {
        case STATE_ARGUMENT: // In or after argument
        case STATE_EATING_SPACES: // Handle runs of whitespace
            switch(ch)
            {
            case '"': state = STATE_DOUBLEQUOTED; break;
            case '\'': state = STATE_SINGLEQUOTED; break;
            case '\\': state = STATE_ESCAPE_OUTER; break;
            case ' ': case '\n': case '\t':
                if(state == STATE_ARGUMENT) // Space ends argument
                {
                    args.push_back(curarg);
                    curarg.clear();
                }
                state = STATE_EATING_SPACES;
                break;
            default: curarg += ch; state = STATE_ARGUMENT;
            }
            break;
        case STATE_SINGLEQUOTED: // Single-quoted string
            switch(ch)
            {
            case '\'': state = STATE_ARGUMENT; break;
            default: curarg += ch;
            }
            break;
        case STATE_DOUBLEQUOTED: // Double-quoted string
            switch(ch)
            {
            case '"': state = STATE_ARGUMENT; break;
            case '\\': state = STATE_ESCAPE_DOUBLEQUOTED; break;
            default: curarg += ch;
            }
            break;
        case STATE_ESCAPE_OUTER: // '\' outside quotes
            curarg += ch; state = STATE_ARGUMENT;
            break;
        case STATE_ESCAPE_DOUBLEQUOTED: // '\' in double-quoted text
            if(ch != '"' && ch != '\\') curarg += '\\'; // keep '\' for everything but the quote and '\' itself
            curarg += ch; state = STATE_DOUBLEQUOTED;
            break;
        }
    }
    switch(state) // final state
    {
    case STATE_EATING_SPACES:
        return true;
    case STATE_ARGUMENT:
        args.push_back(curarg);
        return true;
    default: // ERROR to end in one of the other states
        return false;
    }
}

void RPCExecutorAdapter::request(const QString &command)
{
    std::vector<std::string> args;
    if(!parseCommandLine(args, command.toStdString()))
    {
        emit reply(QmlUI::CMD_ERROR, QString("Parse error: unbalanced ' or \""));
        return;
    }
    if(args.empty())
        return; // Nothing to do
    try
    {
        std::string strPrint;
        // Convert argument list to JSON objects in method-dependent way,
        // and pass it along with the method name to the dispatcher.
        json_spirit::Value result = tableRPC.execute(
                    args[0],
                RPCConvertValues(args[0], std::vector<std::string>(args.begin() + 1, args.end())));

        // Format result reply
        if (result.type() == json_spirit::null_type)
            strPrint = "";
        else if (result.type() == json_spirit::str_type)
            strPrint = result.get_str();
        else
            strPrint = write_string(result, true);

        emit reply(QmlUI::CMD_REPLY, QString::fromStdString(strPrint));
    }
    catch (json_spirit::Object& objError)
    {
        try // Nice formatting for standard-format error
        {
            int code = find_value(objError, "code").get_int();
            std::string message = find_value(objError, "message").get_str();
            emit reply(QmlUI::CMD_ERROR, QString::fromStdString(message) + " (code " + QString::number(code) + ")");
        }
        catch(std::runtime_error &) // raised when converting to invalid type, i.e. missing code or message
        {   // Show raw JSON object
            emit reply(QmlUI::CMD_ERROR, QString::fromStdString(write_string(json_spirit::Value(objError), false)));
        }
    }
    catch (std::exception& e)
    {
        emit reply(QmlUI::CMD_ERROR, QString("Error: ") + QString::fromStdString(e.what()));
    }
}
