import QtQuick 2.0

Rectangle {
    radius: 2
    width: 120
    height: 28
    property alias text: lbl.text
    property var baseColor: "red"
    property var textColor: "white"
    color: ma.pressed ?Qt.darker(baseColor, 1.5) :baseColor
    signal clicked()
    TextLabel {
        id: lbl
        anchors.centerIn: parent
        color: textColor
        font.pixelSize: 12
    }
    MouseArea {
        id: ma
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        onClicked: parent.clicked()
    }
}
