import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import FilterProxyModel 1.0
DialogBase {
    caption: qsTr("Select address")
    Rectangle {
        width: 600
        height: 400
        //            color: "#69a2c0"
        border.color: constants.earnTabPanelBorder
        ScrollView {
            verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn
            anchors.bottom: parent.bottom
            clip: true
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: headerItem.bottom
            anchors.topMargin: -1
            ListView {
                id: overviewAddresses

                spacing: -1
                delegate: Rectangle {
                    border.color: constants.earnTabPanelBorder
                    color: "transparent"
                    height: 42
                    width: parent.width
                    Row {
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: constants.uiBorders + 1
                        spacing: constants.uiBorders + 1
                        Image {
                            id: img
                            source: "res/B-red.png"
                            width: 22
                            opacity: 1
                            height: 22

                        }
                        TextLabel {
                            text: label
                            anchors.verticalCenter: parent.verticalCenter
                            color:constants.textColor
                            font.pixelSize: 12

                        }
                    }
                    TextLabel {
                        x: 210
                        text: address
                        anchors.verticalCenter: parent.verticalCenter
                        color:constants.textColor
                        font.pixelSize: 12

                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            overviewAddresses.currentIndex = index

                        }
                    }
                    property var lab: label
                    property var addr: address

                }
                model: FilterProxyModel {
                    id: model
                    source: addressesModel
                    sort: "label"
                    filters: [{roles: ["type"], regexp: /S/}]
                    order: FilterProxyModel.Asc
                }

                boundsBehavior: Flickable.StopAtBounds
                bottomMargin: -1
                highlight: Rectangle {

                    color: constants.earnTabPanelBorder
                }
                highlightFollowsCurrentItem: true
                onCurrentItemChanged: {
                    currentLabel = currentItem.lab
                    currentAddress = currentItem.addr
                }
            }
        }


        Rectangle {
            color: parent.color
            id: headerItem
            height: 44
            width: parent.width
            anchors.left: parent.left
            anchors.right: parent.right
            border.color: constants.earnTabPanelBorder
            anchors.top: parent.top
            TextLabel {
                anchors.left: parent.left
                anchors.leftMargin: 30
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 16
                font.bold: true
                color:constants.textColor
                text: qsTr("Label")
            }
            TextLabel {
                anchors.left: parent.left
                id: t1
                anchors.leftMargin: 295
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 16
                font.bold: true
                color:constants.textColor
                text: qsTr("Address")
            }
        }
    }
    property string currentLabel: ""
    property string currentAddress: ""
    property var callback
    onAccepted: {
        callback(currentLabel, currentAddress);
        visible = false;
    }
}
