import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtWebKit 3.0
import QtMultimedia 5.5

Window {
    FontLoader { id: mainFont; source: "/fonts/Montserrat-Regular.otf" }
    FontLoader { id: logoFont; source: "/fonts/ROUNDEL_.otf" }
    FontLoader { id: logoCaptionFont; source: "/fonts/Electromagnetic-Lungs.otf" }
    FontLoader { id: windowHandleFont; source: "/fonts/Montserrat-Bold.otf" }
    Constants {
        id: constants
    }

    id: mainWindow
    x: 0; y: 0
    width: 946
    height: 626
    visible: false
    maximumHeight: height
    maximumWidth: width
    minimumHeight: height
    minimumWidth: width

    flags: Qt.FramelessWindowHint | Qt.WindowMinimizeButtonHint | Qt.Window

    AdsProvider {
        id: adsProvider
    }
    property alias ads728x90Url: adsProvider.ads728x90Url
    property alias ads300x250Url: adsProvider.ads300x250Url


    Background{
        id: backgroundItem
        x: 0; y: 0
        width: 946
        height: 626
        anchors.fill: parent

        Window_Frame{
            id: windowHandle
        }
        Item {
            anchors.left: parent.left
            anchors.top: windowHandle.bottom
            anchors.right: parent.right
            anchors.bottom: parent.bottom


            Item {
                anchors.left: sidebar.right
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.bottom: parent.bottom

                Dashboard_Tab {  }
                Earn_Tab{
                    id: earnTab
                }
                Receive_Tab{ }

                Send_Tab{
                    id: sendTab
                }
                Transactions_Tab {      }
                Contacts_Tab {
                    onSendCoinsRequest: {
                        sendTab.recipientsData = recepientsData;
                        sidebar.activeTab = sendTab.tabID
                    }
                }

                Options_Menu{
                    id: optionsMenu
                    visible: sidebar.menuActive
                    onVisibleChanged: if(!visible)sidebar.menuActive = false
                }
            }
            Menu_Side_Bar{
                id: sidebar
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.bottomMargin: 1
                anchors.bottom: parent.bottom
                anchors.top: parent.top
                width: 154
                onMenuActiveChanged: optionsMenu.visible = menuActive;
            }
            NewReceivingAddressDialog {
                id: newReceivingAddressDialog
            }
            NewSendingAddressDialog {
                id: newSendingAddressDialog
            }

            SignVerifyMessageDialog {
                mode: "sign"
                id: signVerifyMessageDialog
            }
            SelectAddressDialog {
                id: selectAddressDialog
            }
            TransactionDescriptionDialog {
                id: transactionDescriptionDialog
            }
            ConfirmSendDialog {
                id:confirmSendDialog
            }

            WalletPasswordDialog {
                id: walletPasswordDialog
            }
            AboutDialog {
                id: aboutDialog
            }

            SyncDialog {
                id: syncDialog
            }
            AskFeeDialog {
                id: askFeeDialog
            }

            AdsDisplay {
                opacity: 0
                adsUrl:adsProvider.siteUrl
            }
            SettingsWindow {
                id: settingsWindow
            }
            DebugWindow {
                id: debugWindow
            }
        }
    }

    Component.onCompleted: {
        console.log("UI Loaded");

        kernelAdapter.backendReady.connect(

                    function () {
                        bycClientModel.message.connect(function(title, message, style){console.log('bycClientModel.message', title, message, style)})

                        bycClientModel.blockSourceChanged.connect(syncDialog.handleBlockStatus)
                        bycClientModel.blocksNumberChanged.connect(syncDialog.handleBlockStatus)
                        bycClientModel.blocksTotalNumberChanged.connect(syncDialog.handleBlockStatus)

                        walletModel.message.connect(function(title, message, style){console.log('walletModel.message', title, message, style)})
                        walletModel.requireUnlock.connect(walletPasswordDialog.tryUnlock)

                        kernelAdapter.incomingTransaction.connect(function(date, amount,  type,  address){
                            console.log('kernelAdapter.incomingTransaction', date, amount,  type,  address)
                            notificationPlayer.play()
                        })
                        kernelAdapter.trayIconTriggered.connect(function(){
                            mainWindow.showNormal()
                        })

                        kernelAdapter.toggleHide.connect(function(v){
                            mainWindow.setVisible(v);
                        })
                        kernelAdapter.needToQuit.connect(toQuit)

                        mainWindow.show();
                    }
                    );
    }
    Audio {
        id: notificationPlayer
        source: "res/notification.mp3"
        onErrorStringChanged: console.log("audio error", errorString)
    }

    property string activeTab: sidebar.activeTab
    signal interfaceTabActivated(string id)

    function toQuit() {
        earnTab.stop();
        mainWindow.hide()
        Qt.quit();
    }
}
