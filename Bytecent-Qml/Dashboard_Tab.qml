import QtQuick 2.5

import WalletModel 1.0
import FilterProxyModel 1.0


InterfaceTab {
    tabID: "dashboard"

    Item {
        anchors.left: parent.left
        anchors.margins: constants.uiBorders
        anchors.top: parent.top
        width: 728
        height: 90
        clip: true
        id: dashboardAds
        AdsDisplay {
            width: 728*10
            height: 90*10

            adsUrl: mainWindow.ads728x90Url
        }
    }


    Column {
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: constants.uiBorders
        anchors.rightMargin: constants.uiBorders +2
        Image {
            width: constants.socialIconsSize; height: width
            source: "res/youtube.png"
            MouseArea {anchors.fill: parent; cursorShape: "PointingHandCursor";
                onClicked: Qt.openUrlExternally("https://www.youtube.com/channel/UCTVJkXJ5LxkWj2lWsHrN4cA")}
        }
        Image {
            width: constants.socialIconsSize; height: width
            source: "res/twitter.png"
            MouseArea {anchors.fill: parent; cursorShape: "PointingHandCursor";
                onClicked: Qt.openUrlExternally("https://twitter.com/bytecent_byc?lang=en")}
        }
        Image {
            width: constants.socialIconsSize; height: width
            source: "res/facebook.png"
            MouseArea {anchors.fill: parent; cursorShape: "PointingHandCursor";
                onClicked: Qt.openUrlExternally("https://www.facebook.com/bytecent")}
        }
    }

    ListView {
        id: statsView
        clip: true
        anchors.margins: constants.uiBorders

        anchors.topMargin: 4
        anchors.bottomMargin: 0
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.top: dashboardAds.bottom
        width: 245
        model: dashboardStatsModel
        spacing: 2
        delegate: Rectangle {
            color: "white"
            border.color: "#cfcfcf"
            width: parent.width
            height: 45
            Rectangle {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: 3
                color: highlightColor
            }
            Image {
                id: icon
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 12
                source: iconUrl
                width: 22
                height: 22
            }

            TextLabel {
                id: leftText
                color: constants.textColor
                font.pixelSize: constants.sidebarFontSize + 1
                text: caption + ":"
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: icon.right
                anchors.leftMargin: 9
            }
            TextLabel {
                color: constants.sidebarTextColorInactive
                font.pixelSize: constants.sidebarFontSize + 1
                text: value
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: icon.right
                anchors.leftMargin: 5 + constants.uiBorders + statsView.keyLabelWidths[group]
            }
            Component.onCompleted: {
                if(statsView.labelMaxTextWidth[group] == undefined)
                    statsView.labelMaxTextWidth[group] = {};
                statsView.labelMaxTextWidth[group][caption] = leftText.width
                statsView.labelMaxTextWidthChanged();
            }

        }
        section.property: "group"
        section.criteria: ViewSection.FullString
        section.delegate: Item {
            width: parent.width
            height: 9
        }
        boundsBehavior: Flickable.StopAtBounds

        property var labelMaxTextWidth: ({})
        property var keyLabelWidths: ({})

        onLabelMaxTextWidthChanged: {
            var ret = {};
            for(var gr in labelMaxTextWidth) {
                var w = 0;
                for(var i in labelMaxTextWidth[gr])
                    if(labelMaxTextWidth[gr][i] > w)
                        w =  labelMaxTextWidth[gr][i];
                //                console.log('maxLeftLabelTextWidth', w)
                ret[gr] = w
            }
            //            console.log('keyLabelWidths', ret)

            keyLabelWidths = ret;
        }
    }

    ListModel {
        id: dashboardStatsModel
        ListElement {
            name: "balance"
            caption: qsTr("Balance")
            iconUrl: "res/Safe Icon.png"
            group: "activity"
            value: "..."
            highlightColor: "#ea4f54"
        }
        ListElement {
            name: "pending"
            caption: qsTr("Pending")
            iconUrl: "res/Safe Icon.png"
            group: "activity"
            value: "..."
            highlightColor: "#ea4f54"
        }
        ListElement {
            name: "activity"
            caption: qsTr("Activity")
            iconUrl: "res/Safe Icon.png"
            group: "activity"
            value:  "..."
            highlightColor: "#ea4f54"
        }

        ListElement {
            name: "wallet"
            caption: qsTr("Wallet")
            iconUrl: "res/CPU Icon.png"
            group: "wallet"
            value: "..."
            highlightColor: "#5479aa"
        }
        ListElement {
            name: "status"
            caption: qsTr("Status")
            iconUrl: "res/CPU Icon.png"
            group: "wallet"
            value: "..."
            highlightColor: "#5479aa"
        }
        ListElement {
            name: "peers"
            caption: qsTr("Peers")
            iconUrl: "res/CPU Icon.png"
            group: "wallet"
            value: "..."
            highlightColor: "#5479aa"
        }

        ListElement {
            name: "byc_value"
            caption: qsTr("BYC Value")
            iconUrl: "res/Bank Icon.png"
            group: "globalStats"
            value: qsTr("updating...")
            highlightColor: "#54a6a6"
        }
        ListElement {
            name: "circulation"
            caption: qsTr("Circulation")
            iconUrl: "res/Bank Icon.png"
            group: "globalStats"
            value: qsTr("updating...")
            highlightColor: "#54a6a6"
        }

        ListElement {
            name: "market_cap"
            caption: qsTr("Market Cap")
            iconUrl: "res/Bank Icon.png"
            group: "globalStats"
            value: qsTr("updating...")
            highlightColor: "#54a6a6"
        }
        Component.onCompleted: {
            downloadStats();

            walletModel.statsBalanceChanged.connect(setBalance);
            walletModel.statsUnconfirmedBalanceChanged.connect(setPendingBalance);
            walletModel.statsImmatureBalanceChanged.connect(setPendingBalance);
            transactionsModel.totalCountChanged.connect(setTransactionsCount);

            setBalance();
            setPendingBalance();
            setTransactionsCount();

            bycClientModel.connectionsNumberChanged.connect(setConnCount);
            setConnCount();

            walletModel.encryptionStatusChanged.connect(setEncrStatus)
            setEncrStatus();
        }

        function setBalance() {
            set(0, {'value': kernelAdapter.formatBYCAmount(walletModel.statsBalance, false)})
        }
        function setPendingBalance() {
            set(1, {'value': kernelAdapter.formatBYCAmount(walletModel.statsUnconfirmedBalance + walletModel.statsImmatureBalance, false)})
        }
        function setTransactionsCount() {
            set(2, {'value': qsTr("%n Transaction(s)", "", transactionsModel.totalCount)})
        }
        function setConnCount() {
            set(5, {'value': bycClientModel.connectionsNumber.toString()})

            if(bycClientModel.connectionsNumber > 0)
                set(4, {'value': qsTr("Connected")})
            else
                set(4, {'value': qsTr("Disconnected")})
        }
        function setEncrStatus() {
            switch(walletModel.walletEncryptionStatus) {
            case WalletModel.Locked:
                set(3, {'value': qsTr("Locked")}); break;
            case WalletModel.Unlocked:
                set(3, {'value': qsTr("Unlocked")}); break;
            case WalletModel.Unencrypted:
                set(3, {'value': qsTr("Unencrypted")}); break;
            }
        }

        function downloadStats() {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function processRequest()   {
                if ( xmlHttp.readyState == 4 && xmlHttp.status == 200 )    {
                    var l = xmlHttp.responseText.split(";")
                    if (l.length == 3) {
                        set(6, {'value': l[0]})
                        set(7, {'value': l[1]})
                        set(8, {'value': '$'+l[2]})
                    }
                }
            }
            xmlHttp.open( "GET",  "https://bytecent.com/app-stats/", true );
            xmlHttp.send( null );
        }
    }
    Timer {
        id: globalStatsTimer
        interval: 10000
        running: true
        repeat: true
        onTriggered: {
            dashboardStatsModel.downloadStats();
        }
    }



    FontMetrics {
        id: fontMetrics2
        //        font.pixelSize: 10
        font.family: mainFont.name

    }


    Column {
        anchors.right: parent.right
        anchors.top: dashboardAds.bottom
        anchors.bottom: parent.bottom
        anchors.left: statsView.right
        anchors.margins: constants.uiBorders + 2

        spacing: constants.uiBorders + 1

        Rectangle {
            width: parent.width
            height: (parent.height - constants.uiBorders) / 2
            color: "#464646"
            Image {
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                source: "img/Clock.png"
                opacity: 0.2
            }
            ListView {
                id: overviewTransactions
                clip:true
                anchors.fill: parent
                header: Rectangle {
                    color: "transparent"
                    height: 30
                    width: parent.width
                    TextLabel {
                        anchors.left: parent.left
                        anchors.leftMargin: constants.uiBorders
                        anchors.verticalCenter: parent.verticalCenter
                        font.pixelSize: 16
                        font.bold: true
                        color: "white"
                        text: qsTr("Transaction History")
                    }
                }
                footer: Rectangle {
                    color: "transparent"
                    height: 26
                    width: parent.width
                    Rectangle {
                        radius: 9
                        width: 72
                        height: 17
                        anchors.centerIn: parent
                        color: constants.backgroundColor
                        TextLabel {
                            anchors.centerIn: parent
                            text: qsTr("View More")
                            color: "#7c7c7c"
                            font.pixelSize: 10
                        }
                        MouseArea {
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            onReleased: {
                                parent.color = constants.backgroundColor
                                sidebar.activeTab = "transactions"
                            }
                            onPressed: parent.color = Qt.darker(parent.color, 3.0)
                        }
                    }
                }
                spacing: -1
                delegate: Rectangle {
                    border.color: "#7a7b7c"
                    color: "transparent"
                    height: 40
                    width: parent.width
                    id: root

                    Image {
                        id: ic
                        source: txIconUrl
                        width: 22
                        height: 22
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: constants.uiBorders/2
                        opacity: 0.6
                        fillMode: Image.PreserveAspectFit
                    }
                    Image {
                        visible: statusDescription != "confirmed" && typeID == 1
                        fillMode: Image.PreserveAspectFit
                        id: statusIcn
                        source: statusIconUrl
                        width: 22
                        height: 22
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: ic.right
                        anchors.leftMargin: constants.uiBorders/2
                    }
                    Row {
                        id: row
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: statusIcn.visible ? statusIcn.right: ic.right
                        anchors.leftMargin: constants.uiBorders/2
                        spacing: constants.uiBorders


                        TextLabel {
                            text: formattedAddress
                            anchors.verticalCenter: parent.verticalCenter
                            color: "white"
                            font.pixelSize: 11
                            width: !statusIcn.visible ? 240:240  - statusIcn.width - constants.uiBorders/2
                        }
                        TextLabel {
                            text: Qt.formatDateTime(date, "MM/dd/yyyy hh:mm")
                            anchors.verticalCenter: parent.verticalCenter
                            color: "white"
                            font.pixelSize: 11
                        }

                    }

                    TextLabel {
                        anchors.right: parent.right
                        anchors.rightMargin: constants.uiBorders
                        anchors.verticalCenter: parent.verticalCenter
                        text: formattedAmountWithUnit
                        color: "white"
                        font.pixelSize: 11
                    }
                }
                model: FilterProxyModel {
                    source: transactionsModel
                    sort: "date"
                    order: FilterProxyModel.Desc
                    limit: 4
                }
                boundsBehavior: Flickable.StopAtBounds
            }
        }

        Rectangle {
            width: parent.width
            height: (parent.height - constants.uiBorders) / 2
            color: "#69a2c0"
            Image {
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                source: "img/B_9.png"
                opacity: 0.2
            }

            ListView {
                id: overviewAddresses
                clip:true
                anchors.fill: parent
                header: Rectangle {
                    color: "transparent"
                    height: 31
                    width: parent.width
                    TextLabel {
                        anchors.left: parent.left
                        anchors.leftMargin: constants.uiBorders
                        anchors.verticalCenter: parent.verticalCenter
                        font.pixelSize: 16
                        font.bold: true
                        color: "white"
                        text: qsTr("Bytecent Addresses")
                    }
                }
                footer: Rectangle {
                    color: "transparent"
                    height: 27
                    width: parent.width
                    Rectangle {
                        radius: 9
                        width: 72
                        height: 17
                        anchors.centerIn: parent
                        color: constants.backgroundColor
                        TextLabel {
                            anchors.centerIn: parent
                            text: "View More"
                            color: "#7c7c7c"
                            font.pixelSize: 10
                        }
                        MouseArea {
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            onReleased: {
                                parent.color = constants.backgroundColor
                                sidebar.activeTab = "receive"
                            }
                            onPressed: parent.color = Qt.darker(parent.color, 3.0)
                        }
                    }
                }
                spacing: -1
                delegate: Rectangle {
                    border.color: "#8eb6c8"
                    color: "transparent"
                    height: 40
                    width: parent.width
                    Row {
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: constants.uiBorders/2
                        spacing: constants.uiBorders/2
                        Image {
                            source: "img/B_9.png"
                            width: 22
                            opacity: 0.6
                            height: 22
                        }

                        TextLabel {
                            id: addrText
                            text: address
                            anchors.verticalCenter: parent.verticalCenter
                            color: "white"
                            font.pixelSize: 11

                        }

                    }
                    Rectangle {
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        anchors.rightMargin: constants.uiBorders
                        width: 48
                        height: 27
                        radius: 4
                        color: "#567ba8"
                        TextLabel {
                            anchors.centerIn: parent
                            color: "white"
                            text: qsTr("Copy")
                            font.pixelSize: 12
                        }
                        MouseArea {
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            onReleased: {
                                clipboard.setText(addrText.text)
                                parent.color = "#567ba8"
                            }
                            onPressed: parent.color = Qt.lighter(parent.color, 2.0)
                        }

                    }

                }
                model: FilterProxyModel {
                    source: addressesModel
                    limit: 4
                    filters: [{roles: ["type"], regexp: /R/}]
                }

                boundsBehavior: Flickable.StopAtBounds
            }
        }
    }

    FontMetrics {
        id: fontMetrics1
        font.pixelSize: 11
        font.family:  mainFont.name
    }
}
