import QtQuick 2.0

Item {
    property var tabsList: []
    default property alias tabs: d.children

    onTabsChanged: {
        var t = []
        for(var i = 0; i < tabs.length; i ++)  {
            tabs[i].visible = false;
            if(tabs[i].title != undefined) {
                t.push(tabs[i])
                tabs[i].anchors.fill = d

            }
        }
        tabsList = t;
        if(tabsList.length > 0) activeIndex = 0;
    }
    property int activeIndex: -1
    Rectangle {
        id: header
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        color: "white"
        height: 40
        Rectangle {
            height: 1
            border.color: constants.earnTabPanelBorder
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 1
        }
        Row {
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            Repeater {
                model: tabsList.length
                Rectangle {
                    width: tl.width + 2*constants.uiBorders
                    height: 40
                    border.color: constants.earnTabPanelBorder
                    color: index == activeIndex ? "white": constants.earnTabPanelBorder
                    TextLabel {
                        id: tl
                        text: tabs[index].title
                        anchors.centerIn: parent
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: activeIndex = index
                        cursorShape: "PointingHandCursor"
                    }
                    Component.onCompleted: {
                        tabs[index].visible = Qt.binding(function(){return index == activeIndex})

                    }
                }
            }
        }

    }
    Rectangle {
        id: d
        color:"white"
        anchors.left: parent.left
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: constants.uiBorders
        anchors.topMargin: -1

    }
}
