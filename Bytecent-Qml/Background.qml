import QtQuick 2.5

Item {
    Rectangle {
        anchors.fill: parent
        color: constants.backgroundColor
        border.color: "#828282"
    }
}
