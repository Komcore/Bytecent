import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
InterfaceTab {
    tabID: "send"
    property var recipientsData: [{address: "", label: "", amount: 0.0, unit: 0}]
    Column {
        anchors.fill: parent
        anchors.margins: 11
        spacing: 11

        Rectangle {
            id: headerBlock
            border.color: constants.earnTabPanelBorder
            height: 60
            width: 768
            Rectangle {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                width:3
                anchors.margins: 1
                color: "#5479aa"
            }

            Row {
                anchors.fill: parent
                anchors.leftMargin: 14
                spacing: constants.uiBorders
                Image {
                    source: "res/EarnTabImage1.png"
                    anchors.verticalCenter: parent.verticalCenter
                }
                TextLabel {
                    font.pixelSize: 12
                    text: qsTr("Use this form to send Bytecent to anyone with a Bytecent address. You can have more than one recipient at a time")
                    onLinkActivated: Qt.openUrlExternally(link)
                    color:constants.textColor
                    linkColor: "#c66868"
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
        }

        Rectangle {
            clip:true
            width: parent.width-1
            height: parent.height  - headerBlock.height - constants.uiBorders - 1
            //            color: "#69a2c0"
            border.color: constants.earnTabPanelBorder

            ScrollView {
                anchors.fill: parent
                anchors.topMargin: 20
                anchors.bottomMargin: 53

                ListView {
                    model: recipientsData.length
                    spacing: 27

                    delegate: Column {
                        z: -index
                        //                    anchors.top: parent.top
                        //                    anchors.topMargin: 20
                        //                    anchors.left: parent.left
                        spacing: 16

                        Row {
                            height: 34
                            spacing: constants.uiBorders
                            TextLabel {
                                width: 107
                                horizontalAlignment: Qt.AlignRight
                                anchors.verticalCenter: parent.verticalCenter
                                color:constants.textColor
                                text: qsTr("Pay To: ")
                                font.pixelSize: 12
                            }
                            TextEdit1 {
                                width: 599
                                id: adddressEdit

                                height: parent.height
                                textRightMargin: 123
                                onTextChanged: {
                                    var l = addressesModel.labelForAddress(text)
                                    if(l != "")
                                        setRecipientData(index, {'label': l, 'address': text})
                                    else
                                        setRecipientData(index, {'address': text})
                                }
                                property var modelText: recipientsData[index].address
                                onModelTextChanged: {

                                    if(text != modelText) {
                                        text = modelText
                                    }
                                }

                                Rectangle {
                                    anchors.right: parent.right
                                    width: 123
                                    color: constants.earnTabPanelBorder
                                    height: parent.height
                                    Row {
                                        anchors.fill: parent
                                        anchors.rightMargin: 15
                                        anchors.leftMargin: 15
                                        spacing: 11
                                        Image {
                                            source: "res/sendtab-icon1.png"
                                            anchors.verticalCenter: parent.verticalCenter
                                            MouseArea {
                                                anchors.fill: parent
                                                cursorShape: "PointingHandCursor"
                                                onClicked: {
                                                    selectAddressDialog.callback = (function(label, address) {
                                                        setRecipientData(index, {'address': address, 'label': label})
                                                    });
                                                    selectAddressDialog.visible = true;
                                                }
                                            }
                                        }
                                        Image {
                                            source: "res/sendtab-icon2.png"
                                            anchors.verticalCenter: parent.verticalCenter
                                            MouseArea {
                                                anchors.fill: parent
                                                cursorShape: "PointingHandCursor"
                                                onClicked: {
                                                    setRecipientData(index, {'address': clipboard.text()})
                                                }
                                            }

                                        }
                                        Image {
                                            source: "res/sendtab-icon3.png"
                                            anchors.verticalCenter: parent.verticalCenter
                                            MouseArea {
                                                anchors.fill: parent
                                                cursorShape: "PointingHandCursor"
                                                onClicked: {
                                                    recipientsData.splice(index, 1);
                                                    recipientsDataChanged()
                                                }
                                            }

                                        }
                                    }
                                }
                                clip: true

                            }
                        }
                        Row {
                            height: 34
                            spacing: constants.uiBorders
                            TextLabel {
                                width: 107
                                horizontalAlignment: Qt.AlignRight
                                anchors.verticalCenter: parent.verticalCenter
                                color:constants.textColor
                                text: qsTr("Label: ")
                                font.pixelSize: 12
                            }
                            TextEdit1 {
                                id: labelEdit
                                width: 599
                                height: parent.height
                                property var modelText: recipientsData[index].label
                                onModelTextChanged: if(text != modelText) text = modelText

                                onTextChanged: {
                                    setRecipientData(index, {'label': text})
                                }
                            }
                        }
                        Row {
                            height: 34
                            spacing: constants.uiBorders
                            TextLabel {
                                width: 107
                                horizontalAlignment: Qt.AlignRight
                                anchors.verticalCenter: parent.verticalCenter
                                color:constants.textColor
                                text: qsTr("Amount: ")
                                font.pixelSize: 12

                            }
                            BYCSpinBox {
                                width: 195
                                height: parent.height
                                id: amountInput

                                property double modelText: recipientsData[index].amount
                                onModelTextChanged: {
                                    if(value != modelText) value = modelText
                                }
                                value: modelText
                                validator: DoubleValidator{bottom: 0;}
                                onValueChanged: {
                                    setRecipientData(index, {'amount': value})
                                }

                            }

                            BYCComboBox {
                                property var modelText: items[recipientsData[index].unit]
                                onModelTextChanged: if(text != modelText) text = modelText

                                items: ["BYC", "mBYC", "µBYC"]
                                onTextChanged: {
                                    setRecipientData(index, {'unit': items.indexOf(text)})
                                }
                            }

                            Item {
                                height: parent.height
                                width: 10
                                Row {
                                    height: parent.height
                                    spacing: 3
                                    TextLabel {
                                        anchors.verticalCenter: parent.verticalCenter
                                        text: qsTr("Your Current Balance:")
                                        font.pixelSize: 12
                                        color:constants.textColor
                                    }
                                    TextLabel {
                                        anchors.verticalCenter: parent.verticalCenter

                                        text: kernelAdapter.formatBYCAmount(walletModel.statsBalance, false)
                                        font.pixelSize: 12
                                        color:"#5eacac"
                                    }
                                }
                            }
                        }
                    }
                }
            }



            RowLayout {
                anchors.leftMargin: 12
                anchors.bottomMargin: 16
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.rightMargin: constants.uiBorders + 3
                spacing: 3

                ButtonType1 {
                    radius: 2
                    width: 120
                    height: 28
                    baseColor: "#bd5151"
                    text: qsTr("Add Recipient")
                    onClicked: {
                        recipientsData.push({address: "", label: "", amount: 0.0, unit: 0})
                        recipientsDataChanged()
                    }
                }
                ButtonType1 {
                    radius: 2
                    width: 120
                    height: 28
                    baseColor: "#5479aa"
                    text: qsTr("Clear All")
                    onClicked: recipientsData = [{address: "", label: "", amount: 0.0, unit: 0}]
                }

                Item{Layout.fillWidth: true}

                ButtonType1 {
                    radius: 2
                    width: 120
                    height: 28
                    baseColor: "#53a6a6"
                    text: qsTr("Send")
                    onClicked: {
                        forceActiveFocus()
                        confirmSendDialog.sendInfo = recipientsData;
                        confirmSendDialog.visible = true
                    }
                    Component.onCompleted: {
                        confirmSendDialog.accepted.connect(
                                    trySend
                                    );
                    }
                    function trySend() {
                        for(var i = 0; i < recipientsData.length; i ++) {
                            var l = addressesModel.labelForAddress(recipientsData[i].address)
                            if(l != "")
                                recipientsData[i].label = l;
                        }


                        if(kernelAdapter.sendCoins(recipientsData)) {
                            recipientsData = []
                            recipientsData = [{address: "", label: "", amount: 0.0, unit: 0}]
                        }
                    }
                }

            }
        }

    }
    onVisibleChanged: if(!visible) recipientsData = [{address: "", label: "", amount: 0.0, unit: 0}]
    function setRecipientData(index, obj) {
        var ch = false;
        for(var k in obj) {
            if(recipientsData[index][k] != obj[k]) {
                recipientsData[index][k] = obj[k];
                ch = true;
            }
        }
        if(ch)
            recipientsDataChanged();
    }
}
