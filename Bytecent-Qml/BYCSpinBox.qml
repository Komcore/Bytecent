import QtQuick 2.5
TextEdit1 {
    width: 195
    height: 34
    id: root
    textRightMargin: 22
    property double maximumValue: 100
    property double minimumValue: 0
    property double value: 0
    property int decimals: 3
    text: "0"
    onValueChanged: text = value.toFixed(decimals)
    onEditingFinished: value = Number(text)
    onReturnPressed: value = Number(text)
    Rectangle {
        border.color: "#e8e6e6"
        color: "#e8e6e6"
        width: 1
        height: parent.height -2
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 20
    }
    Item {
        height: parent.height
        width: 20
        anchors.right: parent.right
        Image {
            source: "res/sendtab-spinner-up.png"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.top
            anchors.verticalCenterOffset: parent.height/3 - 1
            width: 10
            fillMode: Image.PreserveAspectFit
            MouseArea {
                anchors.fill: parent
                cursorShape: "PointingHandCursor"
                onClicked: if(root.value <= maximumValue - 1)  root.value = value + 1.0
            }
        }
        Image {
            source: "res/sendtab-spinner-down.png"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.top
            anchors.verticalCenterOffset: parent.height*2/3 + 3
            width: 10
            fillMode: Image.PreserveAspectFit
            MouseArea {
                anchors.fill: parent
                cursorShape: "PointingHandCursor"
                onClicked: if(root.value >= minimumValue + 1) root.value = root.value - 1.0
            }
        }
    }
}
