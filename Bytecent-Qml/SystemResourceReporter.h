#ifndef SYSTEMRESOURCEREPORTER_H
#define SYSTEMRESOURCEREPORTER_H

#include <QObject>
#include <QTimer>

class SystemResourceReporter : public QObject
{
    Q_OBJECT
    Q_PROPERTY(float cpuLoad READ cpuLoad  NOTIFY cpuLoadChanged)
    Q_PROPERTY(int updateInterval READ updateInterval WRITE setUpdateInterval NOTIFY updateIntervalChanged)
    QTimer timer;
public:
    explicit SystemResourceReporter(QObject *parent = 0);
    float cpuLoad();
    int updateInterval();
    void setUpdateInterval(int interval);
signals:
    void cpuLoadChanged();
    void updateIntervalChanged();
public slots:
private:
    float load;
private slots:
    void timerSlot();


};

#endif // SYSTEMRESOURCEREPORTER_H
