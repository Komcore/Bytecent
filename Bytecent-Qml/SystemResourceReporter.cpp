#include "SystemResourceReporter.h"
#include <QDebug>


#ifdef Q_OS_MAC
#include <mach/mach_init.h>
#include <mach/mach_error.h>
#include <mach/mach_host.h>
#include <mach/vm_map.h>

static unsigned long long _previousTotalTicks = 0;
static unsigned long long _previousIdleTicks = 0;

float CalculateCPULoad(unsigned long long idleTicks, unsigned long long totalTicks)
{
  unsigned long long totalTicksSinceLastTime = totalTicks-_previousTotalTicks;
  unsigned long long idleTicksSinceLastTime  = idleTicks-_previousIdleTicks;
  float ret = 1.0f-((totalTicksSinceLastTime > 0) ? ((float)idleTicksSinceLastTime)/totalTicksSinceLastTime : 0);
  _previousTotalTicks = totalTicks;
  _previousIdleTicks  = idleTicks;
  return ret;
}
float GetCPULoad()
{
   host_cpu_load_info_data_t cpuinfo;
   mach_msg_type_number_t count = HOST_CPU_LOAD_INFO_COUNT;
   if (host_statistics(mach_host_self(), HOST_CPU_LOAD_INFO, (host_info_t)&cpuinfo, &count) == KERN_SUCCESS)
   {
      unsigned long long totalTicks = 0;
      for(int i=0; i<CPU_STATE_MAX; i++) totalTicks += cpuinfo.cpu_ticks[i];
      float sysLoadPercentage = CalculateCPULoad(cpuinfo.cpu_ticks[CPU_STATE_IDLE], totalTicks);
//      qDebug() << "sysLoadPercentage" << i << sysLoadPercentage;
      return sysLoadPercentage;
   }
   else return -1.0f;
}
void init() {

}

#endif

#ifdef Q_OS_WIN

#include "TCHAR.h"
#include "pdh.h"

static PDH_HQUERY cpuQuery;
static PDH_HCOUNTER cpuTotal;

void init()
{
    PdhOpenQuery(NULL, 0, &cpuQuery);
    PdhAddCounter(cpuQuery, L"\\Processor(_Total)\\% Processor Time", 0, &cpuTotal);
    PdhCollectQueryData(cpuQuery);
}

float GetCPULoad()
{
    PDH_FMT_COUNTERVALUE counterVal;

    PdhCollectQueryData(cpuQuery);
    PdhGetFormattedCounterValue(cpuTotal, PDH_FMT_DOUBLE, NULL, &counterVal);
//    qDebug() << counterVal.doubleValue;
    return counterVal.doubleValue / 100.0;
}
#endif


SystemResourceReporter::SystemResourceReporter(QObject *parent) : QObject(parent)
{
    connect(&timer, &QTimer::timeout, this, &SystemResourceReporter::timerSlot);
    timer.setInterval(300);
    timer.start();
    load = 0;
    init();
}

float SystemResourceReporter::cpuLoad()
{
    return load;
}

int SystemResourceReporter::updateInterval()
{
    return timer.interval();
}

void SystemResourceReporter::setUpdateInterval(int interval)
{
    timer.setInterval(interval);
    emit updateIntervalChanged();
}



void SystemResourceReporter::timerSlot()
{
    float l = GetCPULoad();
    if(l!=load)
    {
        load = l;
        emit cpuLoadChanged();
    }
}

