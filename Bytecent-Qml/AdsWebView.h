#ifndef ADSWEBVIEW_H
#define ADSWEBVIEW_H

#include <QQuickPaintedItem>
#include <QWebView>
#include <QMutexLocker>
#include <QApplication>

class RenderHelper: public QObject
{
    Q_OBJECT
    QWebView *webView;
public:
    RenderHelper(QWebView *wView, QObject* parent=0):QObject(parent),webView(wView) {}
public slots:
    void requestImage()
    {
        const qreal pixmapDevicePixelRatio = qreal(((QApplication*)qApp)->devicePixelRatio());
        QPixmap pixmap  (webView->size() * pixmapDevicePixelRatio);
        pixmap.setDevicePixelRatio(pixmapDevicePixelRatio);

        webView->render(&pixmap);
        emit image(pixmap);
    }
signals:
    void image(QPixmap image);
};
class AdsWebView : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)
public:
    AdsWebView(QQuickItem *parent = 0);
    virtual ~AdsWebView();
    void setUrl(QString url);
    QString url();
protected:
    void paint(QPainter *painter);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
private:
    QWebView * webView;
    QPixmap pixmap;
    RenderHelper *helper;
    bool requested;
signals:
    void showed();
    void urlChanged();
    void imgNeeded();
public slots:
    void setUrl(QUrl url);
private slots:
    void fillPixmap(QPixmap pixmap);
};

#endif // ADSWEBVIEW_H
