TEMPLATE = app
include(qml_ui.pri)

# Default rules for deployment.
include(deployment.pri)
SOURCES += $$PWD/main.cpp
INCLUDEPATH +=/usr/local/opt/boost/include \
              /usr/local/opt/openssl/include \
              /usr/local/Cellar/berkeley-db4/4.8.30/include \
              ../ThirdParty/Clang/leveldb/include \
              ../src
LIBS +=  -lssl -lcrypto
DEFINES += QML_UI_TEST
