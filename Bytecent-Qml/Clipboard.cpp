#include "Clipboard.h"
#include <QApplication>


Clipboard::Clipboard(QObject *parent) : QObject(parent)
{

}

void Clipboard::setText(QString text)
{
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(text);
}

QString Clipboard::text()
{
    QClipboard *clipboard = QApplication::clipboard();
    return clipboard->text();
}
