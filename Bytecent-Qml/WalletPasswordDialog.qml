import QtQuick 2.5

DialogBase {


    property string mode: modes.unlock
    caption: modes.captMap[mode]
    Column {
        spacing: constants.uiBorders
        TextLabel {
            color:constants.textColor
            text: modes.descrMap[mode]
            font.pixelSize: 12
            textFormat: Text.RichText
        }

        Row {
            height: 34
            spacing: constants.uiBorders
            TextLabel {
                width: 107
                horizontalAlignment: Qt.AlignRight
                anchors.verticalCenter: parent.verticalCenter
                color:constants.textColor
                text: mode == modes.encrypt || mode == modes.unlock ? qsTr("Password: ") : qsTr("Old Password")
                font.pixelSize: 12
            }
            TextEdit1 {
                id: passw1Edit
                width: 599
                height: parent.height
                maxLength: kernelAdapter.maxPasswordLength
                echoMode: TextInput.Password
            }
        }
        Row {
            visible: mode == modes.encrypt || mode == modes.changePass
            height: 34
            spacing: constants.uiBorders
            TextLabel {
                width: 107
                horizontalAlignment: Qt.AlignRight
                anchors.verticalCenter: parent.verticalCenter
                color:constants.textColor
                text: mode != modes.changePass ? qsTr("Repeat Password: "): qsTr("New Password")
                font.pixelSize: 12
            }
            TextEdit1 {
                id: passw2Edit
                width: 599
                height: parent.height
                maxLength: kernelAdapter.maxPasswordLength
                echoMode: TextInput.Password
            }
        }
        TextLabel {
            id: msg
            color: "red"
            text: ""
            visible: text != ""
            font.pixelSize: 12
        }

        QtObject {
            id: modes
            property string encrypt: "Encrypt"
            property string decrypt: "Decrypt"
            property string unlock: "Unlock"
            property string changePass: "ChangePass"
            property var captMap: {
                "Encrypt": qsTr("Encrypt Wallet"),
                        "Decrypt": qsTr("Decrypt Wallet"),
                        "Unlock":  qsTr("Unlock Wallet"),
                        "ChangePass": qsTr("Change Password")
            }
            property var descrMap: {
                "Encrypt": qsTr("Enter the new passphrase to the wallet.<br/>Please use a passphrase of <b>10 or more random characters</b>, or <b>eight or more words</b>."),
                        "Decrypt": qsTr("This operation needs your wallet passphrase to decrypt the wallet."),
                        "Unlock":  qsTr("This operation needs your wallet passphrase to unlock the wallet."),
                        "ChangePass": qsTr("Enter the old and new passphrase to the wallet.")
            }
        }

    }
    onAccepted: {
        switch(mode) {
        case modes.unlock:
            if(kernelAdapter.unlockWallet(passw1Edit.text)) {
                walletModel.finishUnlock();
                visible = false;
                msg.text = "";
                passw1Edit.text = ""
                passw2Edit.text = ""
            }
            else
                msg.text = qsTr("Password is incorrect. Try again.")
            break;
        case modes.encrypt:
            if(passw1Edit.text == passw2Edit.text) {
                if(kernelAdapter.setWalletEncrypted(true, passw1Edit.text))
                {
                    msg.text = "";
                    visible = false;
                    passw1Edit.text = ""
                    passw2Edit.text = ""
                }
                else
                    msg.text = qsTr("Cannot encrypt Wallet!");
            }
            else
                msg.text = qsTr("Passwords do not match")
            break;
        case modes.changePass:
            if(kernelAdapter.changePassphrase(passw1Edit.text, passw2Edit.text)) {
                msg.text = ""
                visible = false;
                passw1Edit.text = ""
                passw2Edit.text = ""
            }
            else {
                msg.text = qsTr("The password entered for the Wallet decryption was incorrect.")
            }

            break;
        }
    }
    onRejected: {
        passw1Edit.text = ""
        passw2Edit.text = ""
        walletModel.finishUnlock();
        visible = false;
        msg.text = ""
    }
    onVisibleChanged: if(visible)passw1Edit.inputFocus = true
    function tryUnlock() {
        mode = modes.unlock
        visible = true;
    }
    function tryEncrypt() {
        mode = modes.encrypt
        visible = true;
    }
    function tryChangePasswd() {
        mode = modes.changePass
        visible = true;
    }
}
