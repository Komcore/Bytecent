import QtQuick 2.5

DialogBase {
    caption: qsTr("New Contact Address")
    Column {
        spacing: constants.uiBorders
        Row {
            height: 34
            spacing: constants.uiBorders
            TextLabel {
                width: 107
                horizontalAlignment: Qt.AlignRight
                anchors.verticalCenter: parent.verticalCenter
                color:constants.textColor
                text: qsTr("Label: ")
                font.pixelSize: 12
            }
            TextEdit1 {
                id: labelEdit
                width: 599
                height: parent.height

            }
        }
        Row {
            height: 34
            spacing: constants.uiBorders
            TextLabel {
                width: 107
                horizontalAlignment: Qt.AlignRight
                anchors.verticalCenter: parent.verticalCenter
                color:constants.textColor
                text: qsTr("Address: ")
                font.pixelSize: 12
            }
            TextEdit1 {
                id: addressEdit
                width: 599
                height: parent.height

            }
        }
    }
    onAccepted: {
        console.log("Accepted")
        if(kernelAdapter.createSendingAddress(addressEdit.text, labelEdit.text) != "")
            visible = false;
        labelEdit.text = ""
        addressEdit.text = ""
    }
    onRejected: {
        labelEdit.text = ""
        addressEdit.text = ""
    }
    onVisibleChanged: if(visible)labelEdit.inputFocus = true

}
