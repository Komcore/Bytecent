import QtQuick 2.0

Item {
    id: root
    clip: false
    width: 100
    height: 62
    property int diameter: Math.min(width, height)
    property string topText
    property string bottomText
    property double startValue: 0
    property double endValue: 0
    property int thickness: 10
    onStartValueChanged: canvas.requestPaint()
    onEndValueChanged: canvas.requestPaint()
    Canvas {
        id: canvas
        width: diameter*2
        height: diameter*2
        x:0
        y:0
        transform: Scale {
                    xScale: .5
                    yScale: .5
                }

        anchors.top: parent.top
        anchors.left: parent.left

        antialiasing: true


        onPaint: {
            var ctx = canvas.getContext('2d');

            ctx.save();
            ctx.scale(2, 2);


            ctx.translate(root.diameter/2, root.diameter/2);

            ctx.clearRect(0, 0, canvas.width, canvas.width);
            ctx.fillStyle = '#dbe7f5';


            ctx.beginPath();
            ctx.ellipse(-(diameter/2-2), -(diameter/2-2), diameter-4, diameter-4)
            ctx.closePath();
            ctx.fill()
            ctx.fillStyle = 'white';
            ctx.beginPath();
            ctx.ellipse(-(diameter/2-2-thickness), -(diameter/2-2-thickness), diameter-4-thickness*2, diameter-4-thickness*2)
            ctx.closePath();
            ctx.fill()

            ctx.fillStyle = '#4b88ca';
            ctx.strokeStyle = "#4b88ca";
            ctx.beginPath();
            ctx.moveTo(0, -(diameter/2-2 - thickness));
            ctx.arc(0,0,diameter/2-2 - thickness, (startValue-90)/180.0 * Math.PI, (endValue-90)/180.0 * Math.PI, false);
            ctx.arc(0,0,diameter/2-2 , (endValue-90)/180.0 * Math.PI, (startValue-90)/180.0 * Math.PI, true)

            ctx.closePath();
            ctx.fill()

            ctx.lineWidth = 1;


            ctx.restore();
        }
        Column {
            anchors.centerIn: parent
            TextLabel {
                anchors.horizontalCenter: parent.horizontalCenter
                text: topText
                font.pixelSize: 24
                font.bold: true
                color: "#7b7b7b"
            }
            TextLabel {
                anchors.horizontalCenter: parent.horizontalCenter
                text: bottomText
                font.pixelSize: 24
                font.bold: true
                color: "#b74141"
            }
        }
    }

}

