import QtQuick 2.4

Rectangle {
    border.color: constants.earnTabPanelBorder
    color: constants.backgroundColor
    clip: true
    property alias text: inp.text
    property string placeholder
    property int placeholderSize: 12
    property alias readOnly: inp.readOnly
    TextEdit {
        id: inp
        font.family: mainFont.name
        anchors.fill: parent
        anchors.margins: constants.uiBorders/2
        font.pixelSize: 12
        color: constants.textColor
//        focus: true
        text: ""
        activeFocusOnTab: true
        readOnly: false
        selectByMouse: true
        wrapMode: TextEdit.WordWrap
    }
    TextLabel {
         anchors.verticalCenter: parent.verticalCenter
         anchors.left: parent.left
         anchors.leftMargin: constants.uiBorders/2
         font.pixelSize: placeholderSize
         visible: inp.text == ""
         color: constants.textColor

         text:  placeholder
    }
}
