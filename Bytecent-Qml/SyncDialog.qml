import QtQuick 2.5
import QtQuick.Controls 1.4
import ClientModel 1.0

DialogBase {
    cancelOnly: true
    caption: qsTr("Bytecent Synchronization")
    property bool notNeeded: false
    property bool firstSync: true
    property bool newVisible: false
    Column {
        //        anchors.centerIn: parent
        width: childrenRect.width
        height: childrenRect.height
        spacing: constants.uiBorders

        TextLabel {
            id: txt1
            text: qsTr("Synchronizing")
            visible: bycClientModel.connectionsNumber >= 1
        }
        TextLabel {
            text: qsTr("Connecting to the network, this may take a few minutes.")
            visible: bycClientModel.connectionsNumber < 1
        }
        TextLabel {
            id: txt2
            text: qsTr("")
            visible: bycClientModel.connectionsNumber >= 1
        }
        BusyIndicator {
            anchors.horizontalCenter: txt2.horizontalCenter
            running: true
        }

    }
    onRejected: {
        if( !notNeeded) {

            mainWindow.toQuit()

        }
    }


    function handleBlockStatus() {
        switch(bycClientModel.blockSource) {
        case ClientModel.BLOCK_SOURCE_INVALID:
            //                console.log('BLOCK_SOURCE_INVALID')

            break;
        case ClientModel.BLOCK_SOURCE_NONE:
            //                console.log('BLOCK_SOURCE_NONE')
            break;
        case ClientModel.BLOCK_SOURCE_REINDEX:
            notNeeded = false;
            newVisible = true;
            txt1.text = qsTr("Reindexing blocks on disk...")
            break;
        case ClientModel.BLOCK_SOURCE_DISK:
            notNeeded = false;
            newVisible = true;
            txt1.text = qsTr("Importing blocks from disk...")
            break;
        case ClientModel.BLOCK_SOURCE_NETWORK:
            notNeeded = false;
            newVisible = true;
            txt1.text = qsTr("Synchronizing with network...");
            break;
        }

        var lastBlockDate = kernelAdapter.getLastBlockDate();
        var curDate = new Date();
        var secs = (curDate.getTime() - lastBlockDate.getTime())/1000;
        var blockCount = bycClientModel.blocksNumber;
        var totalCount = bycClientModel.blocksTotalNumber;
        if(blockCount < totalCount) {
            txt2.text = qsTr("Processed %1 of %2 (estimated) blocks of transaction history.").arg(blockCount).arg(totalCount);
        }
        else
            txt2.text = qsTr("Processed %1 blocks of transaction history.").arg(blockCount);

        //            console.log(secs)
        if((!firstSync && secs < constants.maxUnsyncMinutes*60)
                || (firstSync && secs < 5 * 60) /*||
                blockCount == totalCount*/)
        {
            firstSync = false;
            notNeeded = true;
            newVisible = false;
        }
        else
        {
            var timeBehindText;
            if(secs < 60*60)
            {
                timeBehindText = qsTr("%n minute(s)","",secs/(60));
            }
            else if(secs < 48*60*60)
            {
                timeBehindText = qsTr("%n hour(s)","",secs/(60*60));
            }
            else if(secs < 14*24*60*60)
            {
                timeBehindText = qsTr("%n day(s)","",secs/(24*60*60));
            }
            else
            {
                timeBehindText = qsTr("%n week(s)","",secs/(7*24*60*60));
            }
            txt2.text += "\n" + timeBehindText;
            //            txt2.text += "\n(%1/%2)".arg(blockCount).arg(totalCount)

            notNeeded = false;
            newVisible = true;
        }
    }
    visible: newVisible || bycClientModel.connectionsNumber < 1
}
