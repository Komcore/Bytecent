import QtQuick 2.4
import QtQuick.Window 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
DialogBase {
    caption:  qsTr("Debug")

    okOnly: true
    onAccepted: visible = false
    property var statsData: [
        {l:qsTr("Bytecent Core"),            v:""},
        {l:qsTr("Client name"),              v:bycClientModel.clientName()},
        {l:qsTr("Client version"),           v:bycClientModel.formatFullVersion()},
        {l:qsTr("Using OpenSSL version"),    v:kernelAdapter.sslVersion()},
        {l:qsTr("Build date"),               v:bycClientModel.formatBuildDate()},
        {l:qsTr("Startup time"),             v:bycClientModel.formatClientStartupTime()},
        {l:qsTr("Network"),                  v:""},
        {l:qsTr("Number of connections"),    v:bycClientModel.connectionsNumber},
        {l:qsTr("On testnet"),               v:bycClientModel.isTestNet()?qsTr("Yes"):qsTr("No")},
        {l:qsTr("Block chain"),              v:""},
        {l:qsTr("Current number of blocks"), v:bycClientModel.blocksNumber},
        {l:qsTr("Estimated total blocks"),   v:bycClientModel.blocksTotalNumber},
        {l:qsTr("Last block time"),          v:Qt.formatDateTime( bycClientModel.lastBlockDate, Qt.TextDate)}
    ]

    BYCTabView {
        width:550
        height: 400

        Rectangle {
            property string title: qsTr("Information")
            GridLayout {
                anchors.fill: parent
                anchors.margins: constants.uiBorders
                columns: 2

                Repeater {
                    model: statsData.length * 2
                    TextLabel{
                        text: if(index%2 == 0)
                                  statsData[index/2].l;
                              else
                                  statsData[(index - 1)/2].v
                        font.bold: index%2 == 0 && statsData[index/2].v == ""

                    }

                }

                TextLabel {
                    text: qsTr("Debug log file")
                    font.bold: true
                }
                TextLabel{}
                ButtonType1 {
                    text: qsTr("Open")
                    onClicked: kernelAdapter.openDebugLogfile()
                    baseColor: "#e78200"
                }

                TextLabel{}
                TextLabel {
                    text: qsTr("Command-line options ")
                    font.bold: true
                }
                TextLabel{}
                ButtonType1 {
                    text: qsTr("Show")
                    onClicked: kernelAdapter.openOptionsHelpBox()
                    baseColor: "#e78200"
                }
                TextLabel{}
            }
        }
        Rectangle {
            property string title: qsTr("Console")
            ColumnLayout {
                anchors.fill: parent
                anchors.margins: constants.uiBorders

                spacing: constants.uiBorders
                TextArea {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    textFormat: TextEdit.RichText
                    id: txtArea
                    text: cmdLog
                    readOnly: true
                    onTextChanged: {
                        readOnly = false
                        cursorPosition = text.length-1
                        readOnly = true
                    }
                }
                RowLayout {
                    Layout.minimumHeight: 30
                    TextEdit1 {
                        height: 34
                        Layout.fillWidth: true
                        onReturnPressed: {
                            kernelAdapter.execCMD(text)
                            text = ""
                        }
                    }
                    ButtonType1 {
                        height: 34
                        text: qsTr("Clear")
                        onClicked: cmdLog = ""
                        baseColor: "#e78200"
                    }
                }
            }
        }
    }

    property string cmdLog: ""
    Component.onCompleted: {
            kernelAdapter.cmdResponse.connect(appendText)
    }
    function appendText(msg) {
        cmdLog += msg
    }
}
