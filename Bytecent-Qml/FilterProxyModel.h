#ifndef FILTERPROXYMODEL_H
#define FILTERPROXYMODEL_H

#include <QObject>
#include <QSortFilterProxyModel>
#include <QUrl>

class FilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(QObject* source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(int limit READ limit WRITE setLimit NOTIFY limitChanged)
    Q_PROPERTY(QString sort READ sortRole WRITE setSort NOTIFY sortChanged)
    Q_PROPERTY(QVariantList filters READ filters WRITE setFilters NOTIFY filtersChanged)
    QAbstractItemModel * m_source;

    int m_limit;

    QVariant m_minimum;

    QVariant m_maximum;

    QString m_sort;
    int m_lastRowCount;
public:
    explicit FilterProxyModel(QObject *parent = 0);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QObject* source() const;

    int limit() const;

    QString sortRole() const;

    enum Order { Asc, Desc };
    Q_ENUM(Order)

    Q_PROPERTY(Order order READ order WRITE setOrder NOTIFY orderChanged)

    Order order() const;

    QVariantList filters() const
    {
        return m_filters;
    }

public slots:
    void setSource(QObject* source);

    void setLimit(int limit);

    void setSort(QString sort);

    void setOrder(Order order);

    bool writeCSV(QUrl filename, QVariantList columns);

    void removeRow(int rowIndex);

    void setFilters(QVariantList filters);

    void setDataByRole(int index, QString role, QVariant value);
    void reset();
signals:
    void sourceChanged(QObject *source);

    void limitChanged(int limit);

    void sortChanged(QString sortRole);

    void orderChanged(Order order);

    void filtersChanged(QVariantList filters);

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex & source_parent) const;
private:
    Order m_order;

    QVariantList m_filters;

    QHash<QString, int> m_roleHash;
};

#endif // FILTERPROXYMODEL_H
