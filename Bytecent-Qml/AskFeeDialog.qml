import QtQuick 2.5

DialogBase {
    caption: qsTr("Confirm Transaction Fee")
    property alias text: lab.text
    TextLabel {
        id: lab
        width: 500
        wrapMode: Text.WordWrap
        textFormat: Text.RichText
        color:constants.textColor

        font.pixelSize: 12
    }

    onAccepted: {
        kernelAdapter.setFeeAgreed()
        visible = false;
        kernelAdapter.finishCallback()
    }
    onRejected: {
        visible = false;
        kernelAdapter.finishCallback()
    }
    Component.onCompleted: {
        kernelAdapter.feeAsked.connect(ask);
    }
    function ask(t) {
        text = t;
        visible = true
    }
}
