import QtQuick 2.4
import QtQuick.Controls 1.4
import QtWebKit 3.0
import QtWebKit.experimental 1.0


InterfaceTab {
    tabID: "trade"
    ScrollView {
        anchors.fill: parent
        AdsDisplay {
            id: webview
            url: "http://bytecent.com/app-trade"
            anchors.fill: parent


        }
    }
    onActivated: {

        webview.reload()
    }
}
