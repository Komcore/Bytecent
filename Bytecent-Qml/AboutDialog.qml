﻿import QtQuick 2.5

DialogBase {
    caption: "About Bytecent"
    okOnly: true

    Column {
        Repeater {
            model: [
                qsTr("<b>Bytecent</b> version %1").arg(bycClientModel.formatFullVersion()),
                qsTr("Copyright &copy; 2013-2016 Komcore Corporation. All rights reserved."),
                qsTr("This computer software is protected by copyright law and international treaties. Unauthorized reproduction or distribution of this software, or any portion of it, may result in severe civil and criminal penalties, and will be prosecuted to the maximum extent possible under the law.")

            ]
            delegate: TextEdit {
                text: modelData
                textFormat: TextEdit.RichText
                width: 600
                color:constants.textColor
                readOnly: true
                selectByMouse: true
                wrapMode: Text.WordWrap
            }
        }


    }

    onAccepted: {
        visible = false
    }
}
