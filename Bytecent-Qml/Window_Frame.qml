import QtQuick 2.5
import OptionsModel 1.0
Rectangle {
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.right: parent.right
    color: constants.windowHandleColor
    anchors.margins: 1
    height: 60

    Row {
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: constants.uiBorders
        spacing: constants.uiBorders /2
        Image {
            source: "res/Logo.png"
            anchors.verticalCenter: parent.verticalCenter

        }


        Text {
            font.family: logoCaptionFont.name
            font.pixelSize: 25
            color: "white"
            text: "Bytecent"
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: 2

        }
    }

    MouseArea {
        anchors.fill: parent
        property var clickPos: "1,1"

        onPressed: {
            clickPos  = Qt.point(mouse.x,mouse.y)
        }

        onPositionChanged: {
            var delta = Qt.point(mouse.x-clickPos.x, mouse.y-clickPos.y)
            mainWindow.x += delta.x;
            mainWindow.y += delta.y;
        }
    }
    Row {
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.rightMargin: constants.uiBorders * 1.5
        spacing: constants.uiBorders
        Text {
            font.family: windowHandleFont.name
            font.pixelSize: 20
            opacity: 0.6
             color: !ma1.containsMouse ? "white": "#e70013"
            text: "_"
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: -4
            MouseArea {
                id: ma1
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                onClicked: mainWindow.showMinimized()
                hoverEnabled: true
            }

        }

        Text {
            font.family: windowHandleFont.name
            font.pixelSize: 20
            opacity: 0.6
            color: !ma2.containsMouse ? "white": "#e70013"
            text: "X"
            anchors.verticalCenter: parent.verticalCenter
            MouseArea {
                id: ma2
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                onClicked: {
                    if(!optionsModel.getOption(OptionsModel.MinimizeOnClose))
                        mainWindow.toQuit();
                    else
                    {
                        mainWindow.showMinimized();
                        if(optionsModel.getOption(OptionsModel.MinimizeToTray))
                        {
                            mainWindow.hide();
                            kernelAdapter.updateTrayHideAction(false);
                        }
                    }
                }
                hoverEnabled: true
            }

        }
    }
}
