import QtQuick 2.0

QtObject {
    property var sideBarColor: "#393939"
    property var sideBarSelectionColor: "#313131"
    property var windowHandleColor: "#282828"
    property var sidebarTextColorInactive: Qt.rgba(159.0/255.0,159.0/255.0,159.0/255.0, 1)
    property var sidebarFontSize: 12
    property var sidebarSelectorColor: Qt.rgba(233.0/255.0,34.0/255.0,45.0/255.0, 1)
    property var uiBorders: 11
    property var socialIconsSize: 30
    property var backgroundColor: "#f0f0f0"
    property var statsViewDecorBorderColor: Qt.rgba(86.0/255.0,122.0/255.0,168.0/255.0, 1)
    property int adsShowInterval:60000
    property int sitesVisitInterval:30000

    property var earnTabPanelColor1: "#ea5055"
    property var earnTabPanelColor2: "#bf2d32"
    property var earnTabPanelBorder: "#cfcfcf"
    property var textColor: "#868686"
    property int maxUnsyncMinutes: 120
}

