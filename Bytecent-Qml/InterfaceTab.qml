import QtQuick 2.5

Item {
    property string tabID
    visible: activeTab == tabID
    anchors.fill: parent
    width: 100
    height: 62
    signal activated()
    Component.onCompleted: {
        interfaceTabActivated.connect(activatedHandler)
    }
    function activatedHandler(id) {
        if(id == tabID)
            activated()
    }
}

