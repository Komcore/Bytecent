import QtQuick 2.5
import QtQuick.Dialogs 1.2
import WalletModel 1.0
Rectangle {
    id: root
    width: 154
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    border.color: "black"
    ListModel {
        id: optionsEntriesModel

        ListElement {
            name: "backupWallet"
            caption: qsTr("Backup Wallet")
            available: true
        }
        ListElement {
            name: "encryptWallet"
            caption: qsTr("Encrypt Wallet")
            available: true
        }
        ListElement {
            name: "changePassword"
            caption: qsTr("Change Password")
            available: true
        }
        ListElement {
            name: "options"
            caption: qsTr("Settings")
            available: true
        }
        ListElement {
            name: "signMessage"
            caption: qsTr("Sign Message")
            available: true
        }
        ListElement {
            name: "verifyMessage"
            caption: qsTr("Verify Message")
            available: true
        }
        ListElement {
            name: "debug"
            caption: qsTr("Debug")
            available: true
        }
        ListElement {
            name: "about"
            caption: qsTr("About")
            available: true
        }
        Component.onCompleted: {
            walletModel.walletEncryptionStatusChanged.connect(updateElems)
            updateElems()
        }
        function updateElems() {
//            console.log(walletModel.walletEncryptionStatus == WalletModel.Unencrypted)
            setProperty(1, "available", walletModel.walletEncryptionStatus == WalletModel.Unencrypted);

        }

    }

    color: constants.sideBarSelectionColor
    Column {
        anchors.fill: parent
        anchors.leftMargin: 1
        anchors.topMargin: 1
        spacing: -1
        Repeater {
            model: optionsEntriesModel
            delegate: optionsDeleg
        }
    }

    Component {
        id: optionsDeleg
        Rectangle {
            id: rect
            property var d: caption
            property var entryName: name
            property bool avail: available
            width: parent.width
            height: 47
            color: ma.containsMouse ?Qt.lighter(constants.sideBarSelectionColor, 2) :constants.sideBarSelectionColor
            TextLabel {
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 17
                text: parent.d
                color:  "white"
                font.pixelSize: constants.sidebarFontSize
                opacity: parent.avail ? 1.0 : 0.2
            }

            MouseArea{
                id: ma
                enabled: parent.avail
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {

//                    console.log("Option entry", parent.entryName)
                    if(parent.entryName == "options")
                        settingsWindow.visible = true
                    else if(parent.entryName == "debug")
                        debugWindow.visible = true
                    else if(parent.entryName == "signMessage") {
                        signVerifyMessageDialog.mode = "sign"
                        signVerifyMessageDialog.visible = true
                    }
                    else if(parent.entryName == "verifyMessage") {
                        signVerifyMessageDialog.mode = "verify"
                        signVerifyMessageDialog.visible = true
                    }
                    else if(parent.entryName == "backupWallet") {
                        fileDialog.open()
                    }
                    else if(parent.entryName == "encryptWallet") {
                        walletPasswordDialog.tryEncrypt()
                    }
                    else if(parent.entryName == "changePassword") {
                        walletPasswordDialog.tryChangePasswd()
                    }
                    else if(parent.entryName == "about") {
                        aboutDialog.visible = true
                    }
                    root.visible= false;
                }
                cursorShape: Qt.PointingHandCursor

            }
        }
    }

    FileDialog {
        id: fileDialog
        title: qsTr("Backup Wallet")
        folder: shortcuts.documents
        selectExisting: false
        nameFilters: [qsTr("Wallet Data (*.dat)")]
        onAccepted: kernelAdapter.backupWallet(fileUrl)
    }
}
