#include "FilterProxyModel.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>
FilterProxyModel::FilterProxyModel(QObject *parent):
    QSortFilterProxyModel(parent)
{
    m_limit = -1;
    m_source = 0;
    setDynamicSortFilter(true);
    m_lastRowCount = 0;





}

int FilterProxyModel::rowCount(const QModelIndex &parent) const
{
    if(m_limit != -1)
    {
        return std::min(QSortFilterProxyModel::rowCount(parent), m_limit);
    }
    else
    {
        return QSortFilterProxyModel::rowCount(parent);
    }
}

QObject* FilterProxyModel::source() const
{
    return m_source;
}

int FilterProxyModel::limit() const
{
    return m_limit;
}

QString FilterProxyModel::sortRole() const
{
    return m_sort;
}

FilterProxyModel::Order FilterProxyModel::order() const
{
    return m_order;
}

void FilterProxyModel::setSource(QObject *source)
{
    if (m_source == source)
        return;

    if(qobject_cast<QAbstractItemModel*>(source) != 0)
    {
        m_source = qobject_cast<QAbstractItemModel*>(source);

        m_roleHash.clear();
        foreach (int role, m_source->roleNames().keys())
            m_roleHash.insert(m_source->roleNames()[role], role);

        setSourceModel(m_source);

        connect(m_source, &QAbstractItemModel::dataChanged, [=] () {
            if(m_source)
            {
                if(m_lastRowCount != m_source->rowCount())
                {
//                    qDebug() << "m_source rowCount" << m_source->rowCount() << "FilterProxyModel" << rowCount();
                    beginResetModel();
                    endResetModel();
                    m_lastRowCount = sourceModel()->rowCount();
                }
            }
        });

        connect(m_source, &QAbstractItemModel::rowsInserted, [=] () {
            if(m_source)
            {
                if(m_lastRowCount != m_source->rowCount())
                {
//                    qDebug() << "m_source rowCount" << m_source->rowCount() << "FilterProxyModel" << rowCount();
                    beginResetModel();
                    endResetModel();
                    m_lastRowCount = sourceModel()->rowCount();
                }
            }
        });



        if(m_roleHash.contains(m_sort))
        {
            QSortFilterProxyModel::setSortRole(m_roleHash[m_sort]);
            QSortFilterProxyModel::sort(0, m_order == Desc ? Qt::DescendingOrder: Qt::AscendingOrder);
        }
        beginResetModel();
        endResetModel();

        emit sourceChanged(m_source);
    }
}

void FilterProxyModel::setLimit(int limit)
{
    if (m_limit == limit)
        return;

    m_limit = limit;
    emit limitChanged(limit);
    beginResetModel();

    endResetModel();
    //    setSourceModel(0);
    setSourceModel(m_source);
}

void FilterProxyModel::setSort(QString sortRole)
{
    if (m_sort == sortRole)
        return;

    m_sort = sortRole;
    emit sortChanged(m_sort);

    if(sourceModel())
    {
        if(!m_roleHash.contains(m_sort))
            return;

        QSortFilterProxyModel::setSortRole(m_roleHash[m_sort]);
        QSortFilterProxyModel::sort(0, m_order == Desc ? Qt::DescendingOrder: Qt::AscendingOrder);

        beginResetModel();
        endResetModel();
    }
}

void FilterProxyModel::setOrder(FilterProxyModel::Order order)
{
    if (m_order == order)
        return;

    m_order = order;
    emit orderChanged(order);

    if(sourceModel())
    {
        if(!m_roleHash.contains(m_sort))
            return;

        QSortFilterProxyModel::setSortRole(m_roleHash[m_sort]);
        QSortFilterProxyModel::sort(0, m_order == Desc ? Qt::DescendingOrder: Qt::AscendingOrder);

        beginResetModel();
        endResetModel();
    }

}

static void writeValue(QTextStream &f, const QString &value)
{
    QString escaped = value;
    escaped.replace('"', "\"\"");
    f << "\"" << escaped << "\"";
}

static void writeSep(QTextStream &f)
{
    f << ",";
}

static void writeNewline(QTextStream &f)
{
    f << "\n";
}



bool FilterProxyModel::writeCSV(QUrl filename, QVariantList columns)
{
    QFile file(filename.toLocalFile());
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;
    QTextStream out(&file);

    int numRows = rowCount();


    QList<QPair<int, QString> > colDescr;
    for(int i = 0; i < columns.count(); i ++)
    {
        QVariantList col = columns.at(i).toList();
        if(col.size() == 2)
        {

            int role = sourceModel()->roleNames().values().indexOf(col[0].toString().toUtf8());
            if(role !=-1)
            {
                role = sourceModel()->roleNames().keys()[role];

                QPair<int, QString> p;
                p.first = role;
                p.second = col[1].toString();
                colDescr.append(p);
            }
        }

    }
    // Header row
    for(int i=0; i<colDescr.size(); ++i)
    {
        if(i!=0)
        {
            writeSep(out);
        }
        writeValue(out, colDescr[i].second);
    }
    writeNewline(out);

    // Data rows
    for(int j=0; j<numRows; ++j)
    {
        for(int i=0; i<colDescr.size(); ++i)
        {
            if(i!=0)
            {
                writeSep(out);
            }
            QVariant data = index(j, 0).data(colDescr[i].first);
            writeValue(out, data.toString());
        }
        writeNewline(out);
    }

    file.close();

    return file.error() == QFile::NoError;
}

void FilterProxyModel::removeRow(int rowIndex)
{
    QSortFilterProxyModel::removeRow(rowIndex);
}

void FilterProxyModel::setFilters(QVariantList filters)
{
    if (m_filters == filters)
        return;

    m_filters = filters;
    emit filtersChanged(filters);

    if(sourceModel())
    {
        beginResetModel();
        endResetModel();
    }
}

void FilterProxyModel::setDataByRole(int index, QString role, QVariant value)
{
    setData(FilterProxyModel::index(index, 0, QModelIndex()), value, m_roleHash[role]);
}

void FilterProxyModel::reset()
{
    beginResetModel();
    endResetModel();
}

bool FilterProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    if(m_filters.isEmpty()) return true;
    QModelIndex index = sourceModel()->index(source_row, 0, source_parent);
    bool accept = true;
    foreach(QVariant f, m_filters)
    {
        QVariantMap filter = f.toMap();
        QVariantList roles = filter["roles"].toList();


        QVariant min = filter["min"];
        QVariant max = filter["max"];
        QRegExp regExp = filter["regexp"].toRegExp();
        if(regExp.isEmpty())
            regExp = QRegExp(filter["regexp"].toString());
        QVariantList exactVals = filter["exactValues"].toList();
        if(!filter["min"].isValid() &&
           !filter["max"].isValid() &&
                !filter["regexp"].isValid() &&
                !filter["exactValues"].isValid())
        {
           continue;
        }
        bool inverted = false;
        if(filter["inverted"].isValid())
            inverted = filter["inverted"].toBool();

        bool thereAreGoodRoles = false;

        foreach(QVariant r, roles)
        {
            QVariant d = index.data(m_roleHash[r.toString()]);
            if(!d.isValid()) continue;

            bool found = false;
            bool notFound = true;
            foreach(QVariant exactVal, exactVals)
                if(exactVal == d)
                {
                    found = true;
                    notFound = false;
                }
            if((found && !inverted) || (!exactVals.isEmpty() && notFound && inverted))
            {
                thereAreGoodRoles = true;
                break;
            }


            if((!inverted && !regExp.isEmpty() && regExp.indexIn(d.toString()) != -1) ||
                    (inverted && !regExp.isEmpty() && regExp.indexIn(d.toString()) == -1) ||
                    (filter["regexp"].isValid() && regExp.isEmpty()))
            {
                thereAreGoodRoles = true;
                break;
            }

            if(min.isValid() && max.isValid())
            {
                if((!inverted && d >= min && d <= max) || (inverted && (d <= min || d >= max)))
                {
                    thereAreGoodRoles = true;
                    break;
                }
            }
            else if(min.isValid())
            {
                if((!inverted && d >= min)|| (inverted && d <= min))
                {
                    thereAreGoodRoles = true;
                    break;
                }
            }
            else if(max.isValid())
            {
                if((!inverted && d <= max)|| (inverted&& d >= max))
                {
                    thereAreGoodRoles = true;
                    break;
                }
            }
        }
        if(!thereAreGoodRoles)
        {
            accept = false;
            break;
        }
    }

    return accept;

}
