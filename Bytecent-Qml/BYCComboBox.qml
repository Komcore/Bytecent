import QtQuick 2.4

Rectangle {
    width: 112
    height: 34
    border.color: constants.earnTabPanelBorder
    color: constants.backgroundColor
    property alias text: txt.text
    property var items:[]
    property int currentIndex: 0
    onCurrentIndexChanged: text = items[currentIndex]
    onItemsChanged: if(items[currentIndex] != undefined) text = items[currentIndex]
    signal activated()
    TextLabel {
        id: txt
        anchors.left: parent.left
        anchors.leftMargin: 13
        color:constants.textColor
        anchors.verticalCenter: parent.verticalCenter
    }

    Item {
        height: parent.height
        width: 20
        anchors.right: parent.right
        Image {
            source: "res/sendtab-combobox-icon.png"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: 1
            width: 10
            fillMode: Image.PreserveAspectFit
        }

    }
    MouseArea {
        anchors.fill: parent
        cursorShape: "PointingHandCursor"
        onClicked: {
            if(items.length > 0){
                dropDownMenu.visible = true
                dropDownMenu.focus = true
            }
            activated()
        }
    }
    Rectangle {
        x: 5
        y: 5
        id: dropDownMenu
        visible: false
        width: parent.width
        height: childrenRect.height + 2*constants.uiBorders
        border.color: constants.earnTabPanelBorder
        color: constants.backgroundColor
        //        color: "red"focus: true
        Keys.onEscapePressed: visible = false
        Column {
            x: constants.uiBorders
            y: constants.uiBorders
            width: parent.width - 2*constants.uiBorders
            spacing: constants.uiBorders
            Repeater {

                model: items

                delegate: TextLabel {
                    text: modelData
                    color:constants.textColor
                    font.bold: ma.containsMouse
                    MouseArea {
                        id: ma
                        anchors.fill: parent
                        cursorShape: "PointingHandCursor"
                        hoverEnabled: true
                        onClicked: {
                            currentIndex = index;
                            dropDownMenu.visible = false;
                        }
                    }
                }
            }
        }
    }
}
