import QtQuick 2.4
import QtQuick.Controls 1.4
import QtWebKit 3.0


InterfaceTab {
    tabID: "services"
    ScrollView {
        anchors.fill: parent
        AdsDisplay {
            id: webview
            url: "http://bytecent.com/app-services"
            anchors.fill: parent
        }
    }
    onActivated: {

        webview.reload()
    }
}
