import QtQuick 2.4
import QtQuick.Window 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import OptionsModel 1.0
import BitcoinUnits 1.0
DialogBase {
    caption: qsTr("Settings")

    okOnly: true
    onAccepted: {
        forceActiveFocus();
        visible = false
    }

    BYCTabView {


        width:550
        height: 350

        Rectangle {
            BitcoinUnits {
                id: unitsModel
            }

            property string title: qsTr("Main")
            ColumnLayout {
                anchors.fill: parent
                anchors.margins: constants.uiBorders
                spacing: constants.uiBorders
                TextLabel {
                    Layout.fillWidth: true
                    wrapMode: Text.WordWrap
                    text: qsTr("Optional transaction fee per kB that helps make sure your transactions processed quickly. Most transactions are 1 kB.")

                }
                RowLayout {
                    TextLabel {
                        text: qsTr("Pay transaction fee")

                    }
                    BYCSpinBox {
                        maximumValue: 1e9
                        id: feeSB
                        Layout.minimumWidth: 150
                        onVChanged: value = v;
                        property var v: unitsModel.valueToUnits(getOpt(OptionsModel.Fee), getOpt(OptionsModel.DisplayUnit))
                        decimals: 3
                        onValueChanged: optionsModel.setOption(OptionsModel.Fee, unitsModel.unitsToValue(value, feeUnit.currentIndex));
                        validator: DoubleValidator{bottom: 0;}
                    }
                    BYCComboBox {
                        id: feeUnit
                        Layout.minimumWidth: 90
                        items: unitsModel.toList()
                        property int lastIndex: 0
                        onCurrentIndexChanged:{
                            //                                optionsModel.setOption(OptionsModel.Fee, unitsModel.unitsToValue(feeSB.value, getOpt(OptionsModel.DisplayUnit)));
                            feeSB.value = unitsModel.valueToUnits(
                                        unitsModel.unitsToValue(feeSB.value,
                                                                lastIndex),
                                        currentIndex)
                            lastIndex = currentIndex
                        }
                    }
                }
                BYCCheckBox {
                    onVChanged:  checked = v
                    property var v: getOpt(OptionsModel.StartAtStartup)
                    text: qsTr("Start Bytecent on system login")
                    onClicked:optionsModel.setOption(OptionsModel.StartAtStartup, checked);
                }
                Item {Layout.fillHeight: true}
                RowLayout {
                    Item {Layout.fillWidth: true}
                    ButtonType1 {
                        text: qsTr("Reset Options")
                        onClicked: optionsModel.resetAll()
                        baseColor: "#e78200"
                    }
                }
            }
        }
        Rectangle {
            property string title: qsTr("Network")
            ColumnLayout {
                anchors.fill: parent
                anchors.margins: constants.uiBorders
                BYCCheckBox {
                    text: qsTr("Map port using UPnP")
                    onClicked:optionsModel.setOption(OptionsModel.MapPortUPnP, checked);
                    onVChanged:  checked = v
                    property var v: getOpt(OptionsModel.MapPortUPnP)
                }
                BYCCheckBox {
                    text: qsTr("Connect through SOCKS proxy")
                    onClicked:optionsModel.setOption(OptionsModel.ProxyUse, checked);
                    onVChanged:  checked = v
                    property var v: getOpt(OptionsModel.ProxyUse)
                }
                RowLayout {
                    TextLabel {
                        text: qsTr("Proxy IP:")

                    }
                    TextEdit1 {
                        placeholder: qsTr("127.0.0.1")
                        width: 100
                        inputMask: "000.000.000.000"
                        textColor: acceptableInput ? constants.textColor: "red"
                        onTextChanged: if(acceptableInput) optionsModel.setOption(OptionsModel.ProxyIP, text);
                        onVChanged: text = v
                        property var v: getOpt(OptionsModel.ProxyIP)
                    }
                    TextLabel {
                        text: qsTr("Port:")
                    }
                    TextEdit1 {
                        width: 50
                        placeholder: qsTr("9050")
                        Layout.maximumWidth: 50
                        validator: IntValidator{bottom: 0; top: 65535}
                        onTextChanged:  optionsModel.setOption(OptionsModel.ProxyPort, text);
                        onVChanged: text = v
                        property var v: getOpt(OptionsModel.ProxyPort)
                    }
                    TextLabel {
                        text: qsTr("SOCKS version:")
                    }
                    BYCComboBox {
                        items: [5, 4]
                        Layout.maximumWidth: 50
                        onCurrentIndexChanged:optionsModel.setOption(OptionsModel.ProxySocksVersion, text)
                        onVChanged: {

                            currentIndex = v
                        }
                        property var v: items.indexOf(getOpt(OptionsModel.ProxySocksVersion))
                    }
                }
                Item {
                    Layout.fillHeight: true
                }
            }
        }
        Rectangle {
            property string title: qsTr("Window")
            ColumnLayout {
                anchors.fill: parent
                anchors.margins: constants.uiBorders
                BYCCheckBox {
                    text: qsTr("Minimize to the tray instead of taskbar")
                    onClicked:optionsModel.setOption(OptionsModel.MinimizeToTray, checked);
                    property var v: getOpt(OptionsModel.MinimizeToTray)
                    onVChanged: checked = v;
                }
                BYCCheckBox {
                    text: qsTr("Minimize on close")
                    onClicked:optionsModel.setOption(OptionsModel.MinimizeOnClose, checked);
                    onVChanged: checked = v
                    property var v: getOpt(OptionsModel.MinimizeOnClose)
                }
                Item {
                    Layout.fillHeight: true
                }
            }
        }
        Rectangle {
            property string title: "Display"
            ColumnLayout {
                anchors.fill: parent
                anchors.margins: constants.uiBorders

                GridLayout {

                    columns: 2
                    TextLabel {
                        text: qsTr("User Interface language:")
                        //                        Layout.alignment: Qt.AlignRight
                    }
                    BYCComboBox {
                        items: ["(default)", qsTr("English")]
                    }
                    TextLabel {
                        text: qsTr("Unit to show amounts in:")
                        //                        Layout.alignment: Qt.AlignRight

                    }
                    BYCComboBox {
                        items:unitsModel.toList()
                        onVChanged: currentIndex = v
                        property var v: getOpt(OptionsModel.DisplayUnit)
                        onCurrentIndexChanged:{
                            optionsModel.setOption(OptionsModel.DisplayUnit, currentIndex);
                            transactionsModel.resetAll()
                            //                                feeUnit.currentIndex = currentIndex
                        }

                    }

                }
                BYCCheckBox {
                    text: qsTr("Display addresses in transaction list")
                    onVChanged: checked = v
                    property var v: getOpt(OptionsModel.DisplayAddresses)
                    onClicked: {
                        optionsModel.setOption(OptionsModel.DisplayAddresses, checked);
                        transactionsModel.resetAll()
                    }

                }
                Item {Layout.fillHeight: true}

            }

        }
    }
    Component.onCompleted: {
        optionsModel.update();
    }

    function getOpt(OptID) {
        if(optionsModel.resetting) {
            return optionsModel.getOption(OptID);
        }
        else
        {
            return optionsModel.getOption(OptID);
        }
    }
}
