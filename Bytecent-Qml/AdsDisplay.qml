import QtQuick 2.5
import QtWebKit 3.0
import QtWebKit.experimental 1.0
import "user_agent_list.js" as UserAgents

WebView {
    experimental.certificateVerificationDialog: Item {
        Component.onCompleted: {
            model.accept();
        }
    }
    property url adsUrl
    opacity: loadProgress / 100.0
    onAdsUrlChanged: {
        experimental.userAgent = UserAgents.getRandomAgent()
        url = adsUrl
    }

    onNavigationRequested: {
        if(request.navigationType == WebView.LinkClickedNavigation){


            var schemaRE = /^\w+:/;
                       if (!schemaRE.test(request.url)) {
                           request.action = WebView.AcceptRequest;
                       } else {
                           request.action = WebView.IgnoreRequest;
                           Qt.openUrlExternally(request.url);
                       }

        }

    }

    onLoadingChanged: {
        if(loadRequest.status == WebView.LoadSucceededStatus) {
            experimental.evaluateJavaScript("var links = document.links, i, length;

                    for (i = 0, length = links.length; i < length; i++) {
                        links[i].target == '_blank' && links[i].removeAttribute('target');
                    }", function(){});

        }
    }
}
