#include <QEventLoop>

#include "MiningController.h"
#ifndef QML_UI_TEST
#include "../src/ui_interface.h"
#include "../src/init.h"
#include "../src/qt/clientmodel.h"
#include "../src/qt/walletmodel.h"
#include "../src/qt/notificator.h"
#include "../src/qt/transactionfilterproxy.h"
#include "../src/qt/trans_addressfilterproxy.h"
#include "../src/qt/transactiontablemodel.h"
#include "../src/qt/bitcoinunits.h"
#include "../src/qt/optionsmodel.h"
#include "../src/bitcoinrpc.h"
#include "../src/qt/bitcoinunits.h"
#include "../src/captcha.h"
#include "../src/bitcoinrpc.h"
#endif
#include <QNetworkAccessManager>
#include <QtNetwork/QHostInfo>
#include <QNetworkInterface>
#include <QDesktopServices>
#include <QMessageBox>
#include <QString>
#include <QDateTime>
extern Captcha gCaptcha;


MiningController::MiningController(ClientModel *client, WalletModel *wallet, QObject *parent):
    clientModel(client),
    walletModel(wallet),
    mineInit(false),
    balanceInit(false),  
    checked(false),   
    version(2300),
    ID(2),    
    clientVersion(2300),
    depth(6),
    blankName(""),
    currentTime(""),
    m_miningActive(false),
    QObject(parent)
{

    // Create tabs

    buttonText = "Start Earning Bytecent!";
}

double MiningController::getChainsPerDay()
{
    if (!mineInit)
    {
        return 0;
    }
    else
    {
        return dChainsPerDay;

    }
}

double MiningController::getPrimesPerSecond()
{
    if (!mineInit)
    {
        return 0;
    }
    else
    {
        return dPrimesPerSec;
    }
}

qint64 MiningController::getBalance(QString address)
{
    std::string a = address.toStdString();
    return GetOneAccountBalance(a);
}

int MiningController::getNumBlocksOfPeers()
{
    return clientModel->getNumBlocksOfPeers();
}


void MiningController::setAddress(QString a)
{
    qaddress = a;
}
   

//////////////////////////////////////////////////////////////////////////////////////////

void MiningController::saveCaptcha(QString captchaString)
{
    QTime now = QTime::currentTime();
    qsrand(now.msec());
    QString str;

    str = captchaString;
    gCaptcha.sCaptchPassword = CHashCheck::generateRandPassword();
    gCaptcha.sCaptchHash = CHashCheck::encodeAES(gCaptcha.sCaptchPassword, str.toUpper().toStdString());
}

void MiningController::mineButtonPushed()
{
    gCaptcha.sCaptchData = input.toStdString();

    //check for captcha hash
    if (buttonText.startsWith("Start"))
    {
        if (gCaptcha.sCaptchData != CHashCheck::decodeAES(gCaptcha.sCaptchPassword, gCaptcha.sCaptchHash)
                || gCaptcha.sCaptchData.empty())
        {        
            emit miningBackendEvent(tr("Incorrect characters entered!"));
            emit captchaError(); 
            return;
        }
    }


    //check for sync
    QDateTime lastBlockDate = clientModel->getLastBlockDate();
    int secs = lastBlockDate.secsTo(QDateTime::currentDateTime());
    int currentBlock = clientModel->getNumBlocks();
    int peerBlock = clientModel->getNumBlocksOfPeers();
    if(secs < 90*60 && currentBlock >= peerBlock)
    {
        if (!mineInit)
        {
            //            minerpage->findChild<QPushButton *>("tryButton")->setText("Stop Earning Bytecent");
            buttonText = "Stop Earning Bytecent";
        }
        if (mineInit)
        {
            //            minerpage->findChild<QPushButton *>("tryButton")->setText("Start Earning Bytecent!");
            buttonText = "Start Earning Bytecent";
        }
        checked = true;
        tryCaptcha();
    }
    else
    {
 
        emit miningBackendEvent(tr("Please wait until wallet is in sync to mine coins."));
        return;
    }
}

void MiningController::setInput(QString inp)
{
    input = inp;
}

void MiningController::tryCaptcha()
{
    //    QValidatedLineEdit * captchaEnter = minerpage->findChild<QValidatedLineEdit *>("captchaEnter");
    if ((mineInit && checked) || (mineInit && !balanceInit))
    {

        buttonText = "Start Earning Bytecent";       
        checked = false;
        stopMining();
        return;
    }
    if (checked)
    {
        //        captchaEnter->setFocus();
    }
    if (input.toStdString()
            == CHashCheck::decodeAES(gCaptcha.sCaptchPassword, gCaptcha.sCaptchHash) && checked)
    {

        currentBalance = 0;
        startMining();
        emit miningBackendEvent(tr("That's correct. Earning BYC!"));
        return;

    }
    if (input.toStdString()
            == CHashCheck::decodeAES(gCaptcha.sCaptchPassword, gCaptcha.sCaptchHash) && checked)
    {
        emit miningBackendEvent(tr("That's incorrect. Not earning!"));
        buttonText ="Start Earning Bytecent!";
        return;
    }
}

void MiningController::startMining()
{
    bool onOrOff;

        onOrOff = true;
        GenerateBitcoins(onOrOff, pwalletMain);
        m_miningActive = true;
        miningActiveChanged();
        mineInit = true;
}


void MiningController::stopMining()
{
    bool onOrOff;

        onOrOff = false;
        GenerateBitcoins(onOrOff, pwalletMain);
        m_miningActive = false;
        miningActiveChanged();
        mineInit = false;
}

