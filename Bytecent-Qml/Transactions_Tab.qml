import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import FilterProxyModel 1.0
import QtQuick.Dialogs 1.2

InterfaceTab {
    tabID: "transactions"

    property var dateFilter: {roles: ['date']}
    property var typeFilter: {roles: ['typeID']}
    property var strFilter: {roles: ['label', 'address', 'formattedAddress']}
    property var amountFilter: ({roles: ['amount'], min: 0, max :0, inverted: true})
    property var transactionFilters:[dateFilter, typeFilter, strFilter, amountFilter]

    onVisibleChanged: overviewAddresses.focus = true
    Rectangle {
        id: topBlock
        z: 10
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 11
        anchors.rightMargin: 12


        height: childrenRect.height +  2*constants.uiBorders

        //            color: "#69a2c0"
        border.color: constants.earnTabPanelBorder

        Column {
            anchors.topMargin: constants.uiBorders
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.rightMargin: constants.uiBorders
            anchors.leftMargin: constants.uiBorders
            spacing: constants.uiBorders
            z: 10

            RowLayout {
                z:10
                height: 34
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: constants.uiBorders

                BYCComboBox {
                    id: dateFilterInput
                    text: qsTr("ALL")
                    items: [qsTr("ALL"), qsTr("Today"), qsTr("This week"), qsTr("This month"), qsTr("Last month"), qsTr("This year"), qsTr("Range...")]
                    width: 100
                    z: 10

                    onTextChanged: dateFilter = calcDateFilter()
                }
                BYCComboBox {
                    id: typeFilterInput
                    items: [qsTr("ALL"), qsTr("Received with"), qsTr("Sent to"), qsTr("To yourself"), qsTr("Mined"), qsTr("Other")]
                    text: qsTr("ALL")
                    width: 100
                    z: 10
                    onTextChanged: {
                        if(text == qsTr("ALL")) {
                            typeFilter = {roles: ['typeID']}
                            return;
                        }
                        var di = {};
                        di[items[1]] = [4];
                        di[items[2]] = [2,3];
                        di[items[3]] = [6];
                        di[items[4]] = [1];
                        di[items[5]] = [0];

                        var val = di[text];
                        typeFilter = {roles: ['typeID'], exactValues: val}
                    }
                }
                TextEdit1 {
                    Layout.fillWidth: true
                    placeholder: qsTr("Enter address or label to search")
                    height: parent.height
                    onTextChanged: strFilter = {roles: ['label', 'address', 'formattedAddress'], regexp: text}
                }
                TextEdit1 {
                    width: 100
                    height: parent.height
                    placeholder: qsTr("Min amount")
                    validator: DoubleValidator{bottom: 0}
                    onTextChanged: {
                        var val = parseFloat(text)
                        if(!isNaN(val)) {
                            val = Math.abs(val) * 100000000;
                            amountFilter =  ({roles: ['amount'], min: -val, max: val, inverted: true})
                        }
                        else
                            amountFilter = {};
                    }
                }
            }
            Row {
                z:1
                id: rangeDataInput
                visible: dateFilterInput.text == qsTr("Range...")
                onVisibleChanged: {
                    tableArea.Layout.fillHeight = false
                    tableArea.Layout.fillHeight = true
                }

                height: 34
                spacing: constants.uiBorders
                TextLabel {
                    text: qsTr("Range:")
                    anchors.verticalCenter: parent.verticalCenter
                    color: constants.textColor

                }
                BYCComboBox {
                    id: fromInput
                    z:10
                    width: 120
                    Calendar {
                        id: fromPicker
                        visible: false
                        onClicked: {
                            visible = false
                            fromInput.text = Qt.formatDate(date, "dd/MM/yyyy")
                            dateFilter = calcDateFilter()
                        }
                    }
                    text:  Qt.formatDate(new Date(), "dd/MM/yyyy")

                    onActivated: {
                        fromPicker.visible = true
                    }
                }
                TextLabel {
                    text: qsTr("to")
                    anchors.verticalCenter: parent.verticalCenter
                    color: constants.textColor

                }
                BYCComboBox {
                    id: toInput
                    z:1
                    width: 120
                    Calendar {
                        id: toPicker
                        visible: false
                        onClicked: {
                            visible = false
                            toInput.text = Qt.formatDate(date, "dd/MM/yyyy")
                            dateFilter = calcDateFilter()
                        }
                    }
                    text:  Qt.formatDate(new Date(), "dd/MM/yyyy")

                    onActivated: {
                        toPicker.visible = true
                    }
                }
            }
        }


    }
    Rectangle {
        id: tableArea
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: topBlock.bottom
        anchors.bottom: parent.bottom
        anchors.margins: 11
        anchors.rightMargin: 12

        //            color: "#69a2c0"
        border.color: constants.earnTabPanelBorder

        ScrollView {
            verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn
            anchors.bottom: footerItem.top
            clip: true
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: headerItem.bottom
            anchors.topMargin: -1
            ListView {
                id: overviewAddresses
                //                footer: Item{height: 1; width: 5}

                spacing: -1
                delegate: FocusScope {
                    height: 42
                    width: parent.width
                    property var lab: label
                    property var addr: address
                    property var trId: txID
                    property var am: formattedAmount
                    property var descr: longDescription
                    function startEdit() {
                        labEdit.start()
                    }
                    Rectangle {
                        height: 42
                        width: parent.width
                        border.color: constants.earnTabPanelBorder
                        color: "transparent"

                        MouseArea {
                            anchors.fill: parent
                            acceptedButtons: Qt.LeftButton|Qt.RightButton
                            onClicked: {
                                //                                overviewAddresses.focus = true

                                overviewAddresses.currentIndex = index;
                                if(mouse.button == Qt.RightButton)
                                    menu.show(parent, mouse.x, mouse.y)
                                mouse.accepted = false

                            }
                            onDoubleClicked: mouse.accepted = false
//                            propagateComposedEvents: true

                        }

                        Image {
                            fillMode: Image.PreserveAspectFit
                            source: txIconUrl
                            width: 22
                            opacity: 1
                            height: 22
                            anchors.verticalCenter: parent.verticalCenter
                            x: 10

                        }
                        Image {
                            fillMode: Image.PreserveAspectFit

                            source: statusIconUrl
                            width: 22
                            opacity: 1
                            height: 22
                            anchors.verticalCenter: parent.verticalCenter
                            x: col0.x + col0.width/2 - width/2 + 20
                            MouseArea {
                                anchors.fill: parent
                                hoverEnabled: true

                                onExited: tooltip.visible = false
                                onPositionChanged: {
                                    tooltip.text = descr
                                    tooltip.setPos(parent, mouse)
                                    tooltip.visible = true
                                }
                                onWheel: {
                                    wheel.accepted = false
                                    tooltip.visible = false
                                }
                            }
                        }

                        TextLabel {
                            text: Qt.formatDateTime(date, "MM/dd/yyyy hh:mm")
                            anchors.verticalCenter: parent.verticalCenter
                            horizontalAlignment: Text.AlignRight
                            color:constants.textColor
                            font.pixelSize: 12
                            x: col1.x + col1.width/2 - width/2 + 10
                        }
                        TextLabel {
                            x: col2.x + col2.width/2 - 40
                            text: type
                            anchors.verticalCenter: parent.verticalCenter
                            color:constants.textColor
                            font.pixelSize: 12

                        }
                        EditableText {
                            focus: true
                            id: labEdit
                            currentText: formattedAddress
                            anchors.verticalCenter: parent.verticalCenter
                            x: col3.x + 30
                            width: col4.x - col3.x
                            editable: address != ''
                            onEdited: {
                                var i = kernelAdapter.getAddressModelItemIndex(address);
                                if(i != -1){
                                    addressesModel.setLabelForItem(i, text);
                                    aModel.reset()
                                } else {
                                    kernelAdapter.createSendingAddress(address, text)
                                    console.log(i)
                                }
                            }
                        }
                        TextLabel {
                            x: col4.x + col4.width/2 - width/2 + 10
                            text: formattedAmount
                            anchors.verticalCenter: parent.verticalCenter
                            color:constants.textColor
                            font.pixelSize: 12
                        }

                    }
                }
                model: FilterProxyModel {
                    id: aModel
                    source: transactionsModel
                    sort: "date"
                    order: FilterProxyModel.Desc
                    filters:transactionFilters
                }

                boundsBehavior: Flickable.StopAtBounds
                bottomMargin: -1

                highlight: Rectangle {

                    color: constants.earnTabPanelBorder
                }
                highlightFollowsCurrentItem: true

                onFlickingChanged: {
                    tooltip.visible = false
                }
            }
            Component.onCompleted: {
                flickableItem.flickingChanged.connect(function(){tooltip.visible = false})
            }
        }


        PopupMenu {
            id: menu
            model: [
                {'text': qsTr("Copy Address"), 'action': (function(){
                    clipboard.setText(overviewAddresses.currentItem.addr);
                })},
                {'text': qsTr("Copy Label"), 'action': (function(){
                    clipboard.setText(overviewAddresses.currentItem.lab);
                })},
                {'text': qsTr("Copy Amount"), 'action': (function(){
                    clipboard.setText(overviewAddresses.currentItem.am);
                })},
                {'text': qsTr("Copy Transaction ID"), 'action': (function(){
                    clipboard.setText(overviewAddresses.currentItem.trId);
                })},
                {'text': qsTr("Edit Label"), 'action': (function(){
                    overviewAddresses.currentItem.startEdit();
                })},
                {'text': qsTr("Show Transaction Details"), 'action': (function(){
                    transactionDescriptionDialog.trData = overviewAddresses.currentItem.descr
                    transactionDescriptionDialog.visible = true;
                })}
            ]
        }
        ToolTip {
            id: tooltip
            bottomMargin: 90

        }
        Rectangle {
            color: parent.color
            id: headerItem
            height: 44
            width: parent.width
            anchors.left: parent.left
            anchors.right: parent.right
            border.color: constants.earnTabPanelBorder
            anchors.top: parent.top
            RowLayout {
                anchors.fill: parent
                anchors.leftMargin: constants.uiBorders
//                Item{Layout.minimumWidth:  30}
                TextLabel {
                    Layout.minimumWidth:  80
                    Layout.maximumWidth: 80
                    anchors.verticalCenter: parent.verticalCenter
//                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: 16
                    font.bold: true
                    color:constants.textColor
                    text: qsTr("Confirmations")
                    id: col0
                    width: 80
                }
                TextLabel {
                    Layout.minimumWidth:  100
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: 16
                    font.bold: true
                    color:constants.textColor
                    text: qsTr("Date")
                    id: col1
                }
                TextLabel {
                    Layout.minimumWidth: 60
                    horizontalAlignment: Text.AlignHCenter
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 16
                    font.bold: true
                    color:constants.textColor
                    text: qsTr("Type")
                    id: col2
                }
                TextLabel {
                    Layout.minimumWidth: 240
                    horizontalAlignment: Text.AlignHCenter
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 16
                    font.bold: true
                    color:constants.textColor
                    text: qsTr("Address")
                    id: col3
                }
                TextLabel {
                    Layout.minimumWidth: 120
                    horizontalAlignment: Text.AlignHCenter
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 16
                    font.bold: true
                    color:constants.textColor
                    text: qsTr("Amount")
                    id: col4
                }
            }
        }
        Rectangle {
            id: footerItem
            color: parent.color
            height: 60
            width: parent.width
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            border.color: constants.earnTabPanelBorder
            Row {

                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: constants.uiBorders+3
                spacing: 3


                ButtonType1 {
                    radius: 2
                    width: 120
                    height: 28
                    baseColor: "#53a6a6"
                    text: qsTr("Export")
                    onClicked: exportDialog.open()

                    FileDialog {
                        id: exportDialog
                        title: "Please choose a file"
                        folder: shortcuts.home
                        selectMultiple: false
                        nameFilters: [ qsTr("CSV files (*.csv)"), qsTr("All files (*)") ]
                        selectExisting: false
                        onAccepted: {
                            if(! aModel.writeCSV(exportDialog.fileUrl, [['confirmed', qsTr('Confirmed')],
                                                                        ['date', qsTr('Date')],
                                                                        ['type', qsTr('Type')],
                                                                        ['label', qsTr('Label')],
                                                                        ['address', qsTr('Address')],
                                                                        ['formattedAmount', qsTr('Amount')],
                                                                        ['txID', qsTr('ID')]
                                                 ]))
                                console.log("Cannot export addresses to ",  exportDialog.fileUrl)
                        }
                    }
                }

            }
        }
    }

    FontMetrics {
        id: fontMetrics1
        font.pixelSize: 11
        font.family:  mainFont.name
    }

    function calcDateFilter() {
        var ret = {roles: ['date']};
        if(dateFilterInput.text != qsTr("ALL")) {
            if(dateFilterInput.text == qsTr("Range...")) {
                ret['min'] = fromPicker.selectedDate;
                ret['max'] = toPicker.selectedDate;
            } else {
                var d = new Date()
                //                d.setTime( d.getTime() - d.getTimezoneOffset()*60*1000 );
                console.log('d',d);
                switch(dateFilterInput.text) {
                case qsTr('Today'):
                    d.setHours(0,0,0,0);
                    ret['min'] = d;
                    break;
                case qsTr('This week'):
                    var weekstart = d.getDate() - (d.getDay() == 0 ? 7: d.getDay()) +1;
                    d.setDate(weekstart)
                    d.setHours(0,0,0,0);
                    ret['min'] = new Date(d); ;
                    break;
                case qsTr('This month'):
                    d.setHours(0,0,0,0);
                    d.setDate(1);
                    ret['min'] = d;
                    break;
                case qsTr('Last month'):
                    d.setMonth(d.getMonth() - 1);
                    ret['min'] = d;
                    break;
                case qsTr('This year'):
                    d.setHours(0,0,0,0);
                    d.setDate(1);
                    d.setMonth(0);
                    ret['min'] = d;
                    break;
                }
            }
        }
        return ret;
    }
}
