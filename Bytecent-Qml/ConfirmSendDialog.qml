import QtQuick 2.5

DialogBase {
    caption: qsTr("Confirm Send Bytecent")
    property alias text: lab.text
    property var sendInfo
    onSendInfoChanged: text = kernelAdapter.getSendConfirmationText(sendInfo);
    TextLabel {
        id: lab
        width: 500
        wrapMode: Text.WordWrap
        textFormat: Text.RichText
        color:constants.textColor

        font.pixelSize: 12
    }

    onAccepted: {
        visible = false;

    }
    onRejected: {
        visible = false;
    }
}
