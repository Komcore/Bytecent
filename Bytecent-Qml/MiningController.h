#ifndef MININGHELPERS_H
#define MININGHELPERS_H
#include <QObject>
#include <QTimer>
#include <QNetworkReply>
#include <time.h>
#include <stdlib.h>
class ClientModel;
class WalletModel;
class MiningController: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool miningActive READ miningActive NOTIFY miningActiveChanged)
public:
    MiningController(ClientModel * client, WalletModel * wallet, QObject * parent = 0);

    Q_INVOKABLE double getChainsPerDay();
    Q_INVOKABLE double getPrimesPerSecond();
    Q_INVOKABLE qint64 getBalance(QString address);
    Q_INVOKABLE int getNumBlocksOfPeers();
    Q_INVOKABLE void setAddress(QString a);
    Q_INVOKABLE void saveCaptcha(QString captchaString);
    Q_INVOKABLE void mineButtonPushed();
    Q_INVOKABLE void setInput(QString inp);

    bool miningActive() {return m_miningActive;}
private:
    ClientModel *clientModel;
    WalletModel *walletModel;
    QTimer balanceTimer;
    QTimer* captchaTimer;

    bool mineInit;
    bool balanceInit;
    bool checked;
    quint64 timeLeft;
    quint64 timeLeftMin;
    qreal currentBalance;    

    int clientVersion;
    std::string address;
    int version;
    int ID;   
    int depth;
    std::string addressName;
    std::string blankName;
    QString currentTime;
    QString qaddress;
    QString input;
    QString buttonText;

    bool m_miningActive;
private slots:   
    void tryCaptcha();
    void startMining();
    void stopMining();


public slots:

signals:
    void miningBackendEvent(QString msg);
    void miningTimeLeft(QString msg);

    void miningActiveChanged();
    void captchaError();
};


#endif // MININGHELPERS_H

