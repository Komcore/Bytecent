import QtQuick 2.5

DialogBase {
    caption: qsTr("Transaction details")
    okOnly: true
    property string trData

    TextEdit {
        text: trData
        textFormat: TextEdit.RichText
        selectByMouse: true
        color:constants.textColor
    }

    onAccepted: {
        console.log("Accepted")
        visible = false
    }
}
